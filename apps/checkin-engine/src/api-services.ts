import { async } from "rxjs";
import { Checkin } from "./mongoose";
import _ from "lodash";
// get checkin info in database

export const getCheckinInfoInDatabase = async ({
  tenantId,
  query,
}: {
  tenantId: string;
  query: any;
}) => {
  //   const query: any = {
  //     tenantId,
  //     checkinTime: {
  //       $gte: new Date(fromDate),
  //       $lte: new Date(toDate),
  //     },
  //   };

  //   if (studentId) {
  //     query.studentId = studentId;
  //   }

  const findQuery = {
    ...query,
    tenantId,
  };

  const checkinInfo = await Checkin.find(findQuery).lean();

  const checkInfoWithFlag = checkinInfo.map((obj) => {
    // @ts-ignore
    const checkinTime = new Date(obj.checkinTime);
    // tileLate is number of minutes late
    let timeLate =
      checkinTime.getHours() * 60 + checkinTime.getMinutes() - 9 * 60 - 15;

    timeLate = timeLate < 0 ? 0 : timeLate;

    const lateFlag = timeLate > 0;
    return {
      ...obj,
      timeLate,
      lateFlag,
    };
  });
  console.log("dattata", checkinInfo.length);

  // group by studentId
  const groupByStudentId = _.groupBy(checkInfoWithFlag, "studentId");
  // get studentId and name
  const studentIdAndName = _.map(groupByStudentId, (value, key) => ({
    studentId: key,
    name: value[0].name,
  }));

  return studentIdAndName;
  return checkinInfo;
};

export const getCheckinInfoInDay = async ({
  tenantId,
  query,
}: {
  tenantId: string;
  query: any;
}) => {
  const date = query?.date;
  console.log({ date });

  const studentIds = query?.studentIds;
  console.log({ studentIds });

  const today = date ? new Date(date) : new Date();
  today?.setHours(0, 0, 0, 0);

  const checkinInfo = await Checkin.find({
    tenantId,
    ...(studentIds && { studentId: { $in: studentIds } }),
    checkinTime: {
      $gte: today,
      $lt: new Date(today.getTime() + 24 * 60 * 60 * 1000),
    },
  }).lean();
  // console.log(checkinInfo.length);

  // const checkInfoWithFlag = checkinInfo.map((obj) => {
  //   // @ts-ignore
  //   const checkinTime = new Date(obj.checkinTime);
  //   // tileLate is number of minutes late
  //   let timeLate =
  //     checkinTime.getHours() * 60 + checkinTime.getMinutes() - 9 * 60 - 15;

  //   timeLate = timeLate < 0 ? 0 : timeLate;

  //   const lateFlag = timeLate > 0;
  //   return {
  //     ...obj,
  //     timeLate,
  //     lateFlag,
  //   };
  // });
  return checkinInfo;
};
