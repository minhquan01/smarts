import { Hik, InfoList } from "@hola/hik-sdk";
import { Checkin, Tenant } from "./mongoose";
import { v4 as uuidv4 } from "uuid";
import _, { get } from "lodash";
import { getDDMMYYYY } from "./utils";

export const getCheckinInfoByTenantAndTime = async (
  tenantId: string,
  start: Date,
  end: Date
) => {
  const dateId = getDDMMYYYY(end);

  // const tenant = await Tenant.findOne({ tenantId }).lean();
  // if (!tenant) {
  //   console.log("tenant not found");
  //   return;
  // }

  // const { checkinLatestTime, username, password } = tenant;

  const hik = new Hik({
    ip: "192.168.68.101",
    port: 80,
    user: "admin",
    password: "vvvVVV123",
  });

  // get info list
  const startTime = start.toISOString();
  const endTime = end.toISOString();

  const response = await hik.Access.AccessEvent({
    searchID: uuidv4(),
    searchResultPosition: 0,
    maxResults: 30,
    major: 5,
    minor: 75,
    startTime,
    endTime,
    // startTime: "2023-09-8T8:15:00+07:00",
    // endTime: "2023-09-8T12:59:59+07:00",
  });
  const infoList = response?.AcsEvent?.InfoList || [];

  // keep first checkin by studentId
  const infoListUnique = _.uniqBy(infoList, "employeeNoString");

  const data = _.map(infoListUnique, (obj) =>
    _.pick(obj, ["employeeNoString", "time", "name"])
  );
  // mapping and insert to database
  const checkinInfoList = _.map(data, (obj) => {
    // checkin id = tenantId_studentId_dateId
    // for keep studentId can checkin just one time per day
    const checkinId = `${tenantId}_${obj.employeeNoString}_${dateId}`;
    return {
      studentId: obj.employeeNoString,
      checkinTime: obj.time,
      tenantId,
      checkinId,
    };
  });

  // insert checkinInfoList to database and skip checkinId unique
  await Checkin.insertMany(checkinInfoList, { ordered: false });
  console.log("insert checkinInfoList to database successfully");
};

// remove checkin info duplicate by day (GMT+7), just keep the first document
export const removeCheckinInfoDuplicateByDay = async () => {
  await Checkin.aggregate([
    {
      $group: {
        _id: {
          studentId: "$studentId",
          tenantId: "$tenantId",
          day: { $dayOfMonth: "$checkinTime" },
          month: { $month: "$checkinTime" },
          year: { $year: "$checkinTime" },
        },
        count: { $sum: 1 },
        ids: { $push: "$_id" },
      },
    },
    {
      $match: {
        count: { $gt: 1 },
      },
    },
  ]).then((results) => {
    // loop over the results
    results.forEach(async (result) => {
      // Remove all except for the first document
      await Checkin.deleteMany({
        _id: { $in: result.ids.slice(1) },
      });
    });
  });
  console.log("remove checkinInfo duplicate by day successfully");
};

const getAccessEvent = async (data: any[], page: number): Promise<any[]> => {
  const maxResults = 30;
  const cloneData = _.cloneDeep(data);

  const hik = new Hik({
    ip: "192.168.68.101",
    port: 80,
    user: "admin",
    password: "vvvVVV123",
  });

  const response = await hik.Access.AccessEvent({
    searchID: "226a3a36-6e31-4ed8-9250-36a1e1a57472a4",
    searchResultPosition: page * maxResults,
    maxResults,
    major: 5,
    minor: 75,
    startTime: "2023-08-01T8:15:00+07:00",
    endTime: "2023-08-31T19:59:59+07:00",
  });
  const totalMatches = response?.AcsEvent?.totalMatches || 0;
  const totalPages = Math.ceil(totalMatches / maxResults);
  console.log({ totalMatches, totalPages });

  // the response have pagination, so we need to get all data. We will use recursion to get all data
  const infoList = response?.AcsEvent?.InfoList || [];
  const infoListPickDataNeeded = _.map(infoList, (obj) =>
    _.pick(obj, ["employeeNoString", "time", "name"])
  );
  // convert employeeNoString to studentId, time to checkinTime
  const checkinInfoList = _.map(infoListPickDataNeeded, (obj) => ({
    studentId: obj.employeeNoString,
    checkinTime: obj.time,
    name: obj.name,
    tenantId: "tenant1",
  }));

  cloneData.push(...checkinInfoList);
  if (page < totalPages - 1) {
    return getAccessEvent(cloneData, page + 1);
  }
  console.log(cloneData, totalPages);
  return cloneData;
};

// get checkin info from 2023/08/01 to 2023/08/31
// serve HOLA TECH
export const getCheckinInfoByMonth = async (tenantId: string) => {
  const infoList = await getAccessEvent([], 0);

  // insert checkinInfoList to database
  await Checkin.insertMany(infoList);
  console.log("insert checkinInfoList to database successfully");
};

export const getCheckinInfoInDatabase = async () => {
  const checkinInfo = await Checkin.find().lean();
  // add late flag = true if checkinTime > 9:15 and timelater = time late
  // add late flag = false if checkinTime <= 9:15 and timelater = 0
  const checkInfoWithFlag = checkinInfo.map((obj) => {
    // @ts-ignore
    const checkinTime = new Date(obj.checkinTime);
    // tileLate is number of minutes late
    let timeLate =
      checkinTime.getHours() * 60 + checkinTime.getMinutes() - 9 * 60 - 15;

    timeLate = timeLate < 0 ? 0 : timeLate;

    const lateFlag = timeLate > 0;
    return {
      ...obj,
      timeLate,
      lateFlag,
    };
  });

  // group by studentId
  const groupByStudentId = _.groupBy(checkInfoWithFlag, "name");
  // get studentId and name
  const studentIdAndName = _.map(groupByStudentId, (value, key) => ({
    studentId: key,
    name: value[0].name,
  }));

  return groupByStudentId;
};
