// src/mongoose.ts
import mongoose, { mongo } from "mongoose";

// Create a Mongoose Schema for Checkin
const checkinSchema = new mongoose.Schema(
  {
    checkinTime: Date,
    studentId: String,
    tenantId: String,
    name: String,
    checkinId: {
      unique: true,
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

const tenantSchema = new mongoose.Schema(
  {
    tenantId: String,
    checkinLatestTime: Date,
    username: String,
    password: String,
    ip: String,
  },
  { timestamps: true }
);

// typeof checkinSchema
type CheckinType = typeof checkinSchema;

// Create a Mongoose Model for Checkin
export const Checkin = mongoose.model("Checkin", checkinSchema);
export const Tenant = mongoose.model("Tenant", tenantSchema);
