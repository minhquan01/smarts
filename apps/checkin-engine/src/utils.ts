export const getDDMMYYYY = (date: Date) => {
  return date
    .toLocaleDateString("en-US", {
      timeZone: "Asia/Ho_Chi_Minh",
    })
    .replace(/\//g, "-");
};
