import express, { Application, Request, Response } from "express";
import { Hik } from "@hola/hik-sdk";
import { v4 as uuidv4 } from "uuid";
// src/mongoose.ts
import mongoose from "mongoose";
import {
  getCheckinInfoByMonth,
  getCheckinInfoByTenantAndTime,
  removeCheckinInfoDuplicateByDay,
} from "./hik-services";
import { Checkin, Tenant } from "./mongoose";
import { getCheckinInfoInDatabase, getCheckinInfoInDay } from "./api-services";

// Define the Mongoose connection URI (modify as needed)
const mongoURI =
  "mongodb+srv://duylinh196tb:1tiWyOOx4JIQA7bO@smartschool.kluuglx.mongodb.net/";

// Connect to MongoDB
mongoose
  .connect(mongoURI, {})
  .then(() => console.log("Connect to database successfully"));

const app: Application = express();

const PORT: number = 3001;
// test
const hik = new Hik({
  ip: "192.168.68.101",
  port: 80,
  user: "admin",
  password: "vvvVVV123",
});

app.get("/get-checkin-info", async (req: Request, res: Response) => {
  // @ts-ignore
  const { tenantId, query }: { tenantId: string; query: any } = req.query;
  const data = await getCheckinInfoInDatabase({ tenantId: "tenant1", query });
  res.json({ data });
});

app.get("/get-by-day", async (req: Request, res: Response) => {
  // @ts-ignore
  const { tenantId, ...query }: { tenantId: string; query: any } = req?.query;
  const data = await getCheckinInfoInDay({ tenantId: "tenant1", query });
  res.json({ data });
});

// app.get("/", async (req: Request, res: Response) => {
//   // await getCheckinInfoByMonth("tenant1");
//   // await removeCheckinInfoDuplicateByDay();
//   const data = await getCheckinInfoInDatabase();
//   res.json({ data });
//   // const data = await Checkin.find().lean();
//   // // convert checkinTime to GMT+7
//   // const newData = data.map((obj) => ({
//   //   ...obj,
//   //   // @ts-ignore
//   //   checkinTime: new Date(obj.checkinTime).toLocaleString("en-US", {
//   //     timeZone: "Asia/Ho_Chi_Minh",
//   //   }),
//   // }));
//   // return res.json({ data: newData });
// });

// app.get("/", async (req: Request, res: Response) => {
//   await getCheckinInfoByTenant("tenant1");
//   // const data = await hik.Access.AccessEvent({
//   //   searchID: uuidv4(),
//   //   searchResultPosition: 0,
//   //   maxResults: 30,
//   //   major: 5,
//   //   minor: 75,
//   //   startTime: "2023-09-8T8:15:00+07:00",
//   //   endTime: "2023-09-8T12:59:59+07:00",
//   // });
//   // console.log(data);
//   return res.json({ message: "OK" });
// });

// app.get("/create-tenant", async (req: Request, res: Response) => {
//   await Tenant.create({
//     tenantId: "tenant1",
//     checkinLatestTime: new Date(),
//     username: "admin",
//     password: "vvvVVV123",
//     ip: "192.168.68.101",
//   });
// });

// TODO: Cronjob scan checkin info by tenantId

// API get checkin info by tenantId and time

app.listen(PORT, (): void => {
  console.log("SERVER IS UP ON PORT:", PORT);
});
