import type { AppProps } from "next/app";
import { Provider } from "react-redux";
import store from "../src/store";
import "../src/styles/global.css";
import { CoreAdminContext } from "@hola/ui-core";
import jsonServerProvider from "ra-data-json-server";

function MyApp({ Component, pageProps }: AppProps) {
  return (
<CoreAdminContext dataProvider={jsonServerProvider("https://jsonplaceholder.typicode.com")}>
  
    <Provider store={store}>
      <Component {...pageProps} />;
    </Provider>
</CoreAdminContext>
  );
}

export default MyApp;
