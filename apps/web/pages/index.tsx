import { ButtonX } from "ui";
import { useHelloQuery } from "../src/store/services/api";
import { sum } from "helpers";
import { useGetList } from "@hola/ui-core";

export default function Web() {
  const { data } = useHelloQuery();
  const xxx = useGetList("users");

  return (
    <div>
      <h1>{data?.message}</h1>
      <ButtonX />
    </div>
  );
}
