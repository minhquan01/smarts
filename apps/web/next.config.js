const withTM = require("next-transpile-modules")(["ui", "helpers"]);

module.exports = withTM({
  reactStrictMode: true,
});
