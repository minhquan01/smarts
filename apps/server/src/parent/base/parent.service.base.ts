/*
------------------------------------------------------------------------------ 
This code was generated by Amplication. 
 
Changes to this file will be lost if the code is regenerated. 

There are other ways to to customize your code, see this doc to learn more
https://docs.amplication.com/how-to/custom-code

------------------------------------------------------------------------------
  */
import { PasswordService } from 'src/auth/password.service';
import { PrismaService } from '../../prisma/prisma.service';
import { Prisma, Parent, Student } from '@prisma/client';
export class ParentServiceBase {
  constructor(
    protected readonly prisma: PrismaService,
    protected readonly passwordService: PasswordService,
  ) {}

  async count<T extends Prisma.ParentCountArgs>(
    args: Prisma.SelectSubset<T, Prisma.ParentCountArgs>,
  ): Promise<number> {
    return this.prisma.parent.count(args);
  }

  async findMany<T extends Prisma.ParentFindManyArgs>(
    args: Prisma.SelectSubset<T, Prisma.ParentFindManyArgs>,
  ): Promise<Parent[]> {
    return this.prisma.parent.findMany(args);
  }
  async findOne<T extends Prisma.ParentFindUniqueArgs>(
    args: Prisma.SelectSubset<T, Prisma.ParentFindUniqueArgs>,
  ): Promise<Parent | null> {
    return this.prisma.parent.findUnique(args);
  }
  async create<T extends Prisma.ParentCreateArgs>(
    args: Prisma.SelectSubset<T, Prisma.ParentCreateArgs>,
  ): Promise<Parent> {
    const user = await this.prisma.user.create({
      select: { id: true, username: true, password: true },
      data: {
        username: args?.data?.phone || '',
        password: await this.passwordService.hash('123456'),
        roles: ['parent'],
      },
    });

    return this.prisma.parent.create<T>(args);
  }

  async upsert<T extends Prisma.ParentUpsertArgs>(
    args: Prisma.SelectSubset<T, Prisma.ParentUpsertArgs>,
  ): Promise<Parent> {
    const newParent = this.prisma.parent.upsert(args);

    const user = await this.prisma.user.create({
      select: { id: true, username: true, password: true },
      data: {
        username: args?.create?.phone || '',
        password: await this.passwordService.hash('123456'),
        roles: ['parent'],
      },
    });
    return newParent;
  }
  async update<T extends Prisma.ParentUpdateArgs>(
    args: Prisma.SelectSubset<T, Prisma.ParentUpdateArgs>,
  ): Promise<Parent> {
    return this.prisma.parent.update<T>(args);
  }
  async delete<T extends Prisma.ParentDeleteArgs>(
    args: Prisma.SelectSubset<T, Prisma.ParentDeleteArgs>,
  ): Promise<Parent> {
    return this.prisma.parent.delete(args);
  }

  async findStudent(
    parentId: string,
    args: Prisma.StudentFindManyArgs,
  ): Promise<Student[]> {
    return this.prisma.parent
      .findUniqueOrThrow({
        where: { id: parentId },
      })
      .student(args);
  }
}
