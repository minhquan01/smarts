import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { ParentServiceBase } from './base/parent.service.base';
import { PasswordService } from 'src/auth/password.service';

@Injectable()
export class ParentService extends ParentServiceBase {
  constructor(
    protected readonly prisma: PrismaService,
    protected readonly passwordService: PasswordService,
  ) {
    super(prisma, passwordService);
  }
}
