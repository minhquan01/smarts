import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { SchoolYearServiceBase } from "./base/schoolYear.service.base";

@Injectable()
export class SchoolYearService extends SchoolYearServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
