import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { SchoolYearModuleBase } from "./base/schoolYear.module.base";
import { SchoolYearService } from "./schoolYear.service";
import { SchoolYearController } from "./schoolYear.controller";

@Module({
  imports: [SchoolYearModuleBase, forwardRef(() => AuthModule)],
  controllers: [SchoolYearController],
  providers: [SchoolYearService],
  exports: [SchoolYearService],
})
export class SchoolYearModule {}
