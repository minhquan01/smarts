import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { SchoolYearService } from "./schoolYear.service";
import { SchoolYearControllerBase } from "./base/schoolYear.controller.base";

@swagger.ApiTags("schoolYears - Năm học")
@common.Controller("schoolYears")
export class SchoolYearController extends SchoolYearControllerBase {
  constructor(
    protected readonly service: SchoolYearService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
