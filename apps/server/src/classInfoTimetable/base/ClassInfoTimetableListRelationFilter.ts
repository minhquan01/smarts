/*
------------------------------------------------------------------------------ 
This code was generated by Amplication. 
 
Changes to this file will be lost if the code is regenerated. 

There are other ways to to customize your code, see this doc to learn more
https://docs.amplication.com/how-to/custom-code

------------------------------------------------------------------------------
  */
import { InputType, Field } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import { ClassInfoTimetableWhereInput } from "./ClassInfoTimetableWhereInput";
import { ValidateNested, IsOptional } from "class-validator";
import { Type } from "class-transformer";

@InputType()
class ClassInfoTimetableListRelationFilter {
  @ApiProperty({
    required: false,
    type: () => ClassInfoTimetableWhereInput,
  })
  @ValidateNested()
  @Type(() => ClassInfoTimetableWhereInput)
  @IsOptional()
  @Field(() => ClassInfoTimetableWhereInput, {
    nullable: true,
  })
  every?: ClassInfoTimetableWhereInput;

  @ApiProperty({
    required: false,
    type: () => ClassInfoTimetableWhereInput,
  })
  @ValidateNested()
  @Type(() => ClassInfoTimetableWhereInput)
  @IsOptional()
  @Field(() => ClassInfoTimetableWhereInput, {
    nullable: true,
  })
  some?: ClassInfoTimetableWhereInput;

  @ApiProperty({
    required: false,
    type: () => ClassInfoTimetableWhereInput,
  })
  @ValidateNested()
  @Type(() => ClassInfoTimetableWhereInput)
  @IsOptional()
  @Field(() => ClassInfoTimetableWhereInput, {
    nullable: true,
  })
  none?: ClassInfoTimetableWhereInput;
}
export { ClassInfoTimetableListRelationFilter as ClassInfoTimetableListRelationFilter };
