/*
------------------------------------------------------------------------------ 
This code was generated by Amplication. 
 
Changes to this file will be lost if the code is regenerated. 

There are other ways to to customize your code, see this doc to learn more
https://docs.amplication.com/how-to/custom-code

------------------------------------------------------------------------------
  */
import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import { isRecordNotFoundError } from "../../prisma.util";
import * as errors from "../../errors";
import { Request } from "express";
import { plainToClass } from "class-transformer";
import { ApiNestedQuery } from "../../decorators/api-nested-query.decorator";
import * as nestAccessControl from "nest-access-control";
import * as defaultAuthGuard from "../../auth/defaultAuth.guard";
import { ClassInfoTimetableService } from "../classInfoTimetable.service";
import { AclValidateRequestInterceptor } from "../../interceptors/aclValidateRequest.interceptor";
import { AclFilterResponseInterceptor } from "../../interceptors/aclFilterResponse.interceptor";
import { ClassInfoTimetableCreateInput } from "./ClassInfoTimetableCreateInput";
import { ClassInfoTimetableWhereInput } from "./ClassInfoTimetableWhereInput";
import { ClassInfoTimetableWhereUniqueInput } from "./ClassInfoTimetableWhereUniqueInput";
import { ClassInfoTimetableFindManyArgs } from "./ClassInfoTimetableFindManyArgs";
import { ClassInfoTimetableUpdateInput } from "./ClassInfoTimetableUpdateInput";
import { ClassInfoTimetable } from "./ClassInfoTimetable";

@swagger.ApiBearerAuth()
@common.UseGuards(defaultAuthGuard.DefaultAuthGuard, nestAccessControl.ACGuard)
export class ClassInfoTimetableControllerBase {
  constructor(
    protected readonly service: ClassInfoTimetableService,
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {}
  @common.UseInterceptors(AclValidateRequestInterceptor)
  @common.Post()
  @swagger.ApiCreatedResponse({ type: ClassInfoTimetable })
  @swagger.ApiBody({
    type: ClassInfoTimetableCreateInput,
  })
  @nestAccessControl.UseRoles({
    resource: "ClassInfoTimetable",
    action: "create",
    possession: "any",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async create(
    @common.Req() req: Request,
    @common.Body() data: ClassInfoTimetableCreateInput
  ): Promise<ClassInfoTimetable> {
    // @ts-ignore
    const tenant = req?.user?.tenant

    return await this.service.create({
      data: {
        tenant,
        ...data,

        classRoom: data.classRoom
          ? {
              connect: data.classRoom,
            }
          : undefined,

        subject: data.subject
          ? {
              connect: data.subject,
            }
          : undefined,

        teacher: data.teacher
          ? {
              connect: data.teacher,
            }
          : undefined,

        timeTable: data.timeTable
          ? {
              connect: data.timeTable,
            }
          : undefined,
      },
      select: {
        classRoom: {
          select: {
            id: true,
          },
        },

        createdAt: true,
        deletedAt: true,
        id: true,
        numberLesson: true,

        subject: {
          select: {
            id: true,
          },
        },

        teacher: {
          select: {
            id: true,
          },
        },

        tenant: true,

        timeTable: {
          select: {
            id: true,
          },
        },

        updatedAt: true,
      },
    });
  }

  @common.UseInterceptors(AclFilterResponseInterceptor)
  @common.Get()
  @swagger.ApiOkResponse({ type: [ClassInfoTimetable] })
  @ApiNestedQuery(ClassInfoTimetableFindManyArgs)
  @nestAccessControl.UseRoles({
    resource: "ClassInfoTimetable",
    action: "read",
    possession: "any",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async findMany(
    @common.Req() request: Request
  ): Promise<ClassInfoTimetable[]> {
    const args = plainToClass(ClassInfoTimetableFindManyArgs, request.query);
    return this.service.findMany({
      ...args,
      select: {
        classRoom: {
          select: {
            id: true,
          },
        },

        createdAt: true,
        deletedAt: true,
        id: true,
        numberLesson: true,

        subject: {
          select: {
            id: true,
          },
        },

        teacher: {
          select: {
            id: true,
          },
        },

        tenant: true,

        timeTable: {
          select: {
            id: true,
          },
        },

        updatedAt: true,
      },
    });
  }

  @common.UseInterceptors(AclFilterResponseInterceptor)
  @common.Get("/:id")
  @swagger.ApiOkResponse({ type: ClassInfoTimetable })
  @swagger.ApiNotFoundResponse({ type: errors.NotFoundException })
  @nestAccessControl.UseRoles({
    resource: "ClassInfoTimetable",
    action: "read",
    possession: "own",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async findOne(
    @common.Param() params: ClassInfoTimetableWhereUniqueInput
  ): Promise<ClassInfoTimetable | null> {
    const result = await this.service.findOne({
      where: params,
      select: {
        classRoom: {
          select: {
            id: true,
          },
        },

        createdAt: true,
        deletedAt: true,
        id: true,
        numberLesson: true,

        subject: {
          select: {
            id: true,
          },
        },

        teacher: {
          select: {
            id: true,
          },
        },

        tenant: true,

        timeTable: {
          select: {
            id: true,
          },
        },

        updatedAt: true,
      },
    });
    if (result === null) {
      throw new errors.NotFoundException(
        `No resource was found for ${JSON.stringify(params)}`
      );
    }
    return result;
  }

  @common.UseInterceptors(AclValidateRequestInterceptor)
  @common.Patch("/:id")
  @swagger.ApiOkResponse({ type: ClassInfoTimetable })
  @swagger.ApiNotFoundResponse({ type: errors.NotFoundException })
  @swagger.ApiBody({
    type: ClassInfoTimetableUpdateInput,
  })
  @nestAccessControl.UseRoles({
    resource: "ClassInfoTimetable",
    action: "update",
    possession: "any",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async update(
    @common.Param() params: ClassInfoTimetableWhereUniqueInput,
    @common.Body() data: ClassInfoTimetableUpdateInput
  ): Promise<ClassInfoTimetable | null> {
    try {
      return await this.service.update({
        where: params,
        data: {
          ...data,

          classRoom: data.classRoom
            ? {
                connect: data.classRoom,
              }
            : undefined,

          subject: data.subject
            ? {
                connect: data.subject,
              }
            : undefined,

          teacher: data.teacher
            ? {
                connect: data.teacher,
              }
            : undefined,

          timeTable: data.timeTable
            ? {
                connect: data.timeTable,
              }
            : undefined,
        },
        select: {
          classRoom: {
            select: {
              id: true,
            },
          },

          createdAt: true,
          deletedAt: true,
          id: true,
          numberLesson: true,

          subject: {
            select: {
              id: true,
            },
          },

          teacher: {
            select: {
              id: true,
            },
          },

          tenant: true,

          timeTable: {
            select: {
              id: true,
            },
          },

          updatedAt: true,
        },
      });
    } catch (error) {
      if (isRecordNotFoundError(error)) {
        throw new errors.NotFoundException(
          `No resource was found for ${JSON.stringify(params)}`
        );
      }
      throw error;
    }
  }

  @common.Delete("/:id")
  @swagger.ApiOkResponse({ type: ClassInfoTimetable })
  @swagger.ApiNotFoundResponse({ type: errors.NotFoundException })
  @nestAccessControl.UseRoles({
    resource: "ClassInfoTimetable",
    action: "delete",
    possession: "any",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async delete(
    @common.Param() params: ClassInfoTimetableWhereUniqueInput
  ): Promise<ClassInfoTimetable | null> {
    try {
      return await this.service.delete({
        where: params,
        select: {
          classRoom: {
            select: {
              id: true,
            },
          },

          createdAt: true,
          deletedAt: true,
          id: true,
          numberLesson: true,

          subject: {
            select: {
              id: true,
            },
          },

          teacher: {
            select: {
              id: true,
            },
          },

          tenant: true,

          timeTable: {
            select: {
              id: true,
            },
          },

          updatedAt: true,
        },
      });
    } catch (error) {
      if (isRecordNotFoundError(error)) {
        throw new errors.NotFoundException(
          `No resource was found for ${JSON.stringify(params)}`
        );
      }
      throw error;
    }
  }
}
