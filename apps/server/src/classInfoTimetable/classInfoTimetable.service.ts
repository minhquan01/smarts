import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { ClassInfoTimetableServiceBase } from "./base/classInfoTimetable.service.base";

@Injectable()
export class ClassInfoTimetableService extends ClassInfoTimetableServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
