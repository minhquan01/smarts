import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { ClassInfoTimetableModuleBase } from "./base/classInfoTimetable.module.base";
import { ClassInfoTimetableService } from "./classInfoTimetable.service";
import { ClassInfoTimetableController } from "./classInfoTimetable.controller";

@Module({
  imports: [ClassInfoTimetableModuleBase, forwardRef(() => AuthModule)],
  controllers: [ClassInfoTimetableController],
  providers: [ClassInfoTimetableService],
  exports: [ClassInfoTimetableService],
})
export class ClassInfoTimetableModule {}
