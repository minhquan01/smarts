import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { ClassInfoTimetableService } from "./classInfoTimetable.service";
import { ClassInfoTimetableControllerBase } from "./base/classInfoTimetable.controller.base";

@swagger.ApiTags("classInfoTimetables - Thông tin các môn của lớp học trong thời khoá biểu")
@common.Controller("classInfoTimetables")
export class ClassInfoTimetableController extends ClassInfoTimetableControllerBase {
  constructor(
    protected readonly service: ClassInfoTimetableService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
