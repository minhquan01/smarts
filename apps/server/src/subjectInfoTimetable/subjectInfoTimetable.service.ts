import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { SubjectInfoTimetableServiceBase } from "./base/subjectInfoTimetable.service.base";

@Injectable()
export class SubjectInfoTimetableService extends SubjectInfoTimetableServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
