import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { SubjectInfoTimetableModuleBase } from "./base/subjectInfoTimetable.module.base";
import { SubjectInfoTimetableService } from "./subjectInfoTimetable.service";
import { SubjectInfoTimetableController } from "./subjectInfoTimetable.controller";

@Module({
  imports: [SubjectInfoTimetableModuleBase, forwardRef(() => AuthModule)],
  controllers: [SubjectInfoTimetableController],
  providers: [SubjectInfoTimetableService],
  exports: [SubjectInfoTimetableService],
})
export class SubjectInfoTimetableModule {}
