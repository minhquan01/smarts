import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { SubjectInfoTimetableService } from "./subjectInfoTimetable.service";
import { SubjectInfoTimetableControllerBase } from "./base/subjectInfoTimetable.controller.base";

@swagger.ApiTags("subjectInfoTimetables - Thông tin môn học trong thời khoá biểu")
@common.Controller("subjectInfoTimetables")
export class SubjectInfoTimetableController extends SubjectInfoTimetableControllerBase {
  constructor(
    protected readonly service: SubjectInfoTimetableService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
