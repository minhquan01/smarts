import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { SemesterService } from "./semester.service";
import { SemesterControllerBase } from "./base/semester.controller.base";

@swagger.ApiTags("semesters - Học kỳ trong năm học")
@common.Controller("semesters")
export class SemesterController extends SemesterControllerBase {
  constructor(
    protected readonly service: SemesterService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
