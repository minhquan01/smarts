import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { SemesterModuleBase } from "./base/semester.module.base";
import { SemesterService } from "./semester.service";
import { SemesterController } from "./semester.controller";

@Module({
  imports: [SemesterModuleBase, forwardRef(() => AuthModule)],
  controllers: [SemesterController],
  providers: [SemesterService],
  exports: [SemesterService],
})
export class SemesterModule {}
