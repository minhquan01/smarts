import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { TeachingInfoModuleBase } from "./base/teachingInfo.module.base";
import { TeachingInfoService } from "./teachingInfo.service";
import { TeachingInfoController } from "./teachingInfo.controller";

@Module({
  imports: [TeachingInfoModuleBase, forwardRef(() => AuthModule)],
  controllers: [TeachingInfoController],
  providers: [TeachingInfoService],
  exports: [TeachingInfoService],
})
export class TeachingInfoModule {}
