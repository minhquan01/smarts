import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { TeachingInfoServiceBase } from "./base/teachingInfo.service.base";

@Injectable()
export class TeachingInfoService extends TeachingInfoServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
