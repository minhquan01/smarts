import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { TeachingInfoService } from "./teachingInfo.service";
import { TeachingInfoControllerBase } from "./base/teachingInfo.controller.base";

@swagger.ApiTags("teachingInfos - Thông tin giảng dạy các môn học của lớp")
@common.Controller("teachingInfos")
export class TeachingInfoController extends TeachingInfoControllerBase {
  constructor(
    protected readonly service: TeachingInfoService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
