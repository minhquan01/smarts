/*
------------------------------------------------------------------------------ 
This code was generated by Amplication. 
 
Changes to this file will be lost if the code is regenerated. 

There are other ways to to customize your code, see this doc to learn more
https://docs.amplication.com/how-to/custom-code

------------------------------------------------------------------------------
  */
import { InputType, Field } from "@nestjs/graphql";
import { SubjectInfoTimetableWhereUniqueInput } from "../../subjectInfoTimetable/base/SubjectInfoTimetableWhereUniqueInput";
import { ApiProperty } from "@nestjs/swagger";

@InputType()
class SubjectInfoTimetableUpdateManyWithoutSubjectsInput {
  @Field(() => [SubjectInfoTimetableWhereUniqueInput], {
    nullable: true,
  })
  @ApiProperty({
    required: false,
    type: () => [SubjectInfoTimetableWhereUniqueInput],
  })
  connect?: Array<SubjectInfoTimetableWhereUniqueInput>;

  @Field(() => [SubjectInfoTimetableWhereUniqueInput], {
    nullable: true,
  })
  @ApiProperty({
    required: false,
    type: () => [SubjectInfoTimetableWhereUniqueInput],
  })
  disconnect?: Array<SubjectInfoTimetableWhereUniqueInput>;

  @Field(() => [SubjectInfoTimetableWhereUniqueInput], {
    nullable: true,
  })
  @ApiProperty({
    required: false,
    type: () => [SubjectInfoTimetableWhereUniqueInput],
  })
  set?: Array<SubjectInfoTimetableWhereUniqueInput>;
}

export { SubjectInfoTimetableUpdateManyWithoutSubjectsInput as SubjectInfoTimetableUpdateManyWithoutSubjectsInput };
