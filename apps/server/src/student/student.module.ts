import { Module, forwardRef } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { StudentModuleBase } from './base/student.module.base';
import { StudentService } from './student.service';
import { StudentController } from './student.controller';
import { ParentService } from 'src/parent/parent.service';
import { UserService } from 'src/user/user.service';
import { PasswordService } from 'src/auth/password.service';

@Module({
  imports: [StudentModuleBase, forwardRef(() => AuthModule)],
  controllers: [StudentController],
  providers: [StudentService, ParentService, UserService],
  exports: [StudentService],
})
export class StudentModule {}
