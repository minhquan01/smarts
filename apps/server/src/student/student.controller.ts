import * as common from '@nestjs/common';
import * as swagger from '@nestjs/swagger';
import * as nestAccessControl from 'nest-access-control';
import { StudentService } from './student.service';
import { StudentControllerBase } from './base/student.controller.base';
import { ParentService } from 'src/parent/parent.service';
import { UserService } from 'src/user/user.service';
import { PasswordService } from 'src/auth/password.service';

@swagger.ApiTags('students - Học sinh')
@common.Controller('students')
export class StudentController extends StudentControllerBase {
  constructor(
    protected readonly service: StudentService,
    protected readonly parentService: ParentService,
    protected readonly userService: UserService,
    protected readonly passwordService: PasswordService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder,
  ) {
    super(service, parentService, rolesBuilder);
  }
}
