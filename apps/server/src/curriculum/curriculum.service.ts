import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { CurriculumServiceBase } from "./base/curriculum.service.base";

@Injectable()
export class CurriculumService extends CurriculumServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
