import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { CurriculumModuleBase } from "./base/curriculum.module.base";
import { CurriculumService } from "./curriculum.service";
import { CurriculumController } from "./curriculum.controller";

@Module({
  imports: [CurriculumModuleBase, forwardRef(() => AuthModule)],
  controllers: [CurriculumController],
  providers: [CurriculumService],
  exports: [CurriculumService],
})
export class CurriculumModule {}
