/*
------------------------------------------------------------------------------ 
This code was generated by Amplication. 
 
Changes to this file will be lost if the code is regenerated. 

There are other ways to to customize your code, see this doc to learn more
https://docs.amplication.com/how-to/custom-code

------------------------------------------------------------------------------
  */
import { InputType, Field } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import {
  IsDate,
  IsOptional,
  ValidateNested,
  IsInt,
  IsString,
} from "class-validator";
import { Type } from "class-transformer";
import { GradeWhereUniqueInput } from "../../grade/base/GradeWhereUniqueInput";
import { SubjectWhereUniqueInput } from "../../subject/base/SubjectWhereUniqueInput";

@InputType()
class CurriculumUpdateInput {
  @ApiProperty({
    required: false,
  })
  @IsDate()
  @Type(() => Date)
  @IsOptional()
  @Field(() => Date, {
    nullable: true,
  })
  deletedAt?: Date | null;

  @ApiProperty({
    required: false,
    type: () => GradeWhereUniqueInput,
  })
  @ValidateNested()
  @Type(() => GradeWhereUniqueInput)
  @IsOptional()
  @Field(() => GradeWhereUniqueInput, {
    nullable: true,
  })
  grade?: GradeWhereUniqueInput | null;

  @ApiProperty({
    required: false,
    type: Number,
  })
  @IsInt()
  @IsOptional()
  @Field(() => Number, {
    nullable: true,
  })
  numberAdjacentLesson?: number | null;

  @ApiProperty({
    required: false,
    type: Number,
  })
  @IsInt()
  @IsOptional()
  @Field(() => Number, {
    nullable: true,
  })
  numberLesson?: number | null;

  @ApiProperty({
    required: false,
    type: () => SubjectWhereUniqueInput,
  })
  @ValidateNested()
  @Type(() => SubjectWhereUniqueInput)
  @IsOptional()
  @Field(() => SubjectWhereUniqueInput, {
    nullable: true,
  })
  subject?: SubjectWhereUniqueInput | null;

  // @ApiProperty({
  //   required: false,
  //   type: String,
  // })
  // @IsString()
  // @IsOptional()
  // @Field(() => String, {
  //   nullable: true,
  // })
  // tenant?: string | null;
}

export { CurriculumUpdateInput as CurriculumUpdateInput };
