import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { CurriculumService } from "./curriculum.service";
import { CurriculumControllerBase } from "./base/curriculum.controller.base";

@swagger.ApiTags("curricula - Khung chương trình các môn học trong thời khoá biểu của khối")
@common.Controller("curricula")
export class CurriculumController extends CurriculumControllerBase {
  constructor(
    protected readonly service: CurriculumService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
