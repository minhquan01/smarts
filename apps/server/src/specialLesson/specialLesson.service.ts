import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { SpecialLessonServiceBase } from "./base/specialLesson.service.base";

@Injectable()
export class SpecialLessonService extends SpecialLessonServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
