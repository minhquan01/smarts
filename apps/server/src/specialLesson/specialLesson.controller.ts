import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { SpecialLessonService } from "./specialLesson.service";
import { SpecialLessonControllerBase } from "./base/specialLesson.controller.base";

@swagger.ApiTags("specialLessons - Tiết học cần tranh và tiết học cố đinh")
@common.Controller("specialLessons")
export class SpecialLessonController extends SpecialLessonControllerBase {
  constructor(
    protected readonly service: SpecialLessonService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
