import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { SpecialLessonModuleBase } from "./base/specialLesson.module.base";
import { SpecialLessonService } from "./specialLesson.service";
import { SpecialLessonController } from "./specialLesson.controller";

@Module({
  imports: [SpecialLessonModuleBase, forwardRef(() => AuthModule)],
  controllers: [SpecialLessonController],
  providers: [SpecialLessonService],
  exports: [SpecialLessonService],
})
export class SpecialLessonModule {}
