import { Module, Scope, CacheModule } from '@nestjs/common';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { MorganInterceptor, MorganModule } from 'nest-morgan';
import * as redisStore from 'cache-manager-redis-store';
import { UserModule } from './user/user.module';
import { SchoolModule } from './school/school.module';
import { SemesterModule } from './semester/semester.module';
import { StudentModule } from './student/student.module';
import { ClassroomModule } from './classroom/classroom.module';
import { SubjectModule } from './subject/subject.module';
import { TeacherModule } from './teacher/teacher.module';
import { ParentModule } from './parent/parent.module';
import { GradeModule } from './grade/grade.module';
import { SchoolYearModule } from './schoolYear/schoolYear.module';
import { NotifyModule } from './notify/notify.module';
import { TeachingInfoModule } from './teachingInfo/teachingInfo.module';
import { TimeTableModule } from './timeTable/timeTable.module';
import { LessonModule } from './lesson/lesson.module';
import { AttendanceModule } from './attendance/attendance.module';
import { StudyTimeModule } from './studyTime/studyTime.module';
import { DepartmentModule } from './department/department.module';
import { SpecialLessonModule } from './specialLesson/specialLesson.module';
import { SubjectInfoTimetableModule } from './subjectInfoTimetable/subjectInfoTimetable.module';
import { CurriculumModule } from './curriculum/curriculum.module';
import { ClassInfoTimetableModule } from './classInfoTimetable/classInfoTimetable.module';
import { HealthModule } from './health/health.module';
import { PrismaModule } from './prisma/prisma.module';
import { SecretsManagerModule } from './providers/secrets/secretsManager.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ServeStaticOptionsService } from './serveStaticOptions.service';
import { GraphQLModule } from '@nestjs/graphql';

import { ACLModule } from './auth/acl.module';
import { AuthModule } from './auth/auth.module';
import { TransformInterceptor } from './decorators/response-transform-interceptor.interceptor';

@Module({
  controllers: [],
  imports: [
    ACLModule,
    AuthModule,
    UserModule,
    SchoolModule,
    SemesterModule,
    StudentModule,
    ClassroomModule,
    SubjectModule,
    TeacherModule,
    ParentModule,
    GradeModule,
    SchoolYearModule,
    NotifyModule,
    TeachingInfoModule,
    TimeTableModule,
    LessonModule,
    AttendanceModule,
    StudyTimeModule,
    DepartmentModule,
    SpecialLessonModule,
    SubjectInfoTimetableModule,
    CurriculumModule,
    ClassInfoTimetableModule,
    HealthModule,
    PrismaModule,
    SecretsManagerModule,
    MorganModule,
    ConfigModule.forRoot({ isGlobal: true }),
    ServeStaticModule.forRootAsync({
      useClass: ServeStaticOptionsService,
    }),
    GraphQLModule.forRootAsync({
      useFactory: configService => {
        const playground = configService.get('GRAPHQL_PLAYGROUND');
        const introspection = configService.get('GRAPHQL_INTROSPECTION');
        return {
          autoSchemaFile: 'schema.graphql',
          sortSchema: true,
          playground,
          introspection: playground || introspection,
        };
      },
      inject: [ConfigService],
      imports: [ConfigModule],
    }),
    // CacheModule.register({
    //   isGlobal: true,
    //   store: redisStore,
    //   host: process.env.REDIS_HOST,
    //   port: process.env.REDIS_PORT,
    //   username: process.env.REDIS_USERNAME,
    //   password: process.env.REDIS_PASSWORD,
    //   ttl: parseInt(process.env.REDIS_TTL ? process.env.REDIS_TTL : '5'),

    //   max: parseInt(
    //     process.env.REDIS_MAX_REQUESTS_CACHED
    //       ? process.env.REDIS_MAX_REQUESTS_CACHED
    //       : '100',
    //   ),
    // }),
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      scope: Scope.REQUEST,
      useClass: MorganInterceptor('combined'),
    },
    { provide: APP_INTERCEPTOR, useClass: TransformInterceptor },
  ],
})
export class AppModule {}
