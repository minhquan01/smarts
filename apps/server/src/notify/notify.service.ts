import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { NotifyServiceBase } from "./base/notify.service.base";

@Injectable()
export class NotifyService extends NotifyServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
