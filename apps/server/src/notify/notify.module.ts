import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { NotifyModuleBase } from "./base/notify.module.base";
import { NotifyService } from "./notify.service";
import { NotifyController } from "./notify.controller";

@Module({
  imports: [NotifyModuleBase, forwardRef(() => AuthModule)],
  controllers: [NotifyController],
  providers: [NotifyService],
  exports: [NotifyService],
})
export class NotifyModule {}
