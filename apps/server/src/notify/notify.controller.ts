import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { NotifyService } from "./notify.service";
import { NotifyControllerBase } from "./base/notify.controller.base";

@swagger.ApiTags("notifies - Mẫu thông báo")
@common.Controller("notifies")
export class NotifyController extends NotifyControllerBase {
  constructor(
    protected readonly service: NotifyService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
