import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { StudyTimeServiceBase } from "./base/studyTime.service.base";

@Injectable()
export class StudyTimeService extends StudyTimeServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
