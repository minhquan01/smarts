import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { StudyTimeModuleBase } from "./base/studyTime.module.base";
import { StudyTimeService } from "./studyTime.service";
import { StudyTimeController } from "./studyTime.controller";

@Module({
  imports: [StudyTimeModuleBase, forwardRef(() => AuthModule)],
  controllers: [StudyTimeController],
  providers: [StudyTimeService],
  exports: [StudyTimeService],
})
export class StudyTimeModule {}
