/*
------------------------------------------------------------------------------ 
This code was generated by Amplication. 
 
Changes to this file will be lost if the code is regenerated. 

There are other ways to to customize your code, see this doc to learn more
https://docs.amplication.com/how-to/custom-code

------------------------------------------------------------------------------
  */
import { ArgsType, Field } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import { StudyTimeWhereInput } from "./StudyTimeWhereInput";
import { IsOptional, ValidateNested, IsInt } from "class-validator";
import { Type } from "class-transformer";
import { StudyTimeOrderByInput } from "./StudyTimeOrderByInput";

@ArgsType()
class StudyTimeFindManyArgs {
  @ApiProperty({
    required: false,
    type: () => StudyTimeWhereInput,
  })
  @IsOptional()
  @ValidateNested()
  @Field(() => StudyTimeWhereInput, { nullable: true })
  @Type(() => StudyTimeWhereInput)
  where?: StudyTimeWhereInput;

  @ApiProperty({
    required: false,
    type: [StudyTimeOrderByInput],
  })
  @IsOptional()
  @ValidateNested({ each: true })
  @Field(() => [StudyTimeOrderByInput], { nullable: true })
  @Type(() => StudyTimeOrderByInput)
  orderBy?: Array<StudyTimeOrderByInput>;

  @ApiProperty({
    required: false,
    type: Number,
  })
  @IsOptional()
  @IsInt()
  @Field(() => Number, { nullable: true })
  @Type(() => Number)
  skip?: number;

  @ApiProperty({
    required: false,
    type: Number,
  })
  @IsOptional()
  @IsInt()
  @Field(() => Number, { nullable: true })
  @Type(() => Number)
  take?: number;
}

export { StudyTimeFindManyArgs as StudyTimeFindManyArgs };
