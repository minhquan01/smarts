import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { StudyTimeService } from "./studyTime.service";
import { StudyTimeControllerBase } from "./base/studyTime.controller.base";

@swagger.ApiTags("studyTimes - Thời gian học mặc định")
@common.Controller("studyTimes")
export class StudyTimeController extends StudyTimeControllerBase {
  constructor(
    protected readonly service: StudyTimeService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
