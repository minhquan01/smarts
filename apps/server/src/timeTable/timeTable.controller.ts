import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import * as nestAccessControl from "nest-access-control";
import { TimeTableService } from "./timeTable.service";
import { TimeTableControllerBase } from "./base/timeTable.controller.base";

@swagger.ApiTags("timeTables - Thời khoá biểu")
@common.Controller("timeTables")
export class TimeTableController extends TimeTableControllerBase {
  constructor(
    protected readonly service: TimeTableService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {
    super(service, rolesBuilder);
  }
}
