import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma/prisma.service";
import { TimeTableServiceBase } from "./base/timeTable.service.base";

@Injectable()
export class TimeTableService extends TimeTableServiceBase {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma);
  }
}
