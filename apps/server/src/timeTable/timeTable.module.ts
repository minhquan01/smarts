import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { TimeTableModuleBase } from "./base/timeTable.module.base";
import { TimeTableService } from "./timeTable.service";
import { TimeTableController } from "./timeTable.controller";

@Module({
  imports: [TimeTableModuleBase, forwardRef(() => AuthModule)],
  controllers: [TimeTableController],
  providers: [TimeTableService],
  exports: [TimeTableService],
})
export class TimeTableModule {}
