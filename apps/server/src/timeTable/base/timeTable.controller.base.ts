/*
------------------------------------------------------------------------------ 
This code was generated by Amplication. 
 
Changes to this file will be lost if the code is regenerated. 

There are other ways to to customize your code, see this doc to learn more
https://docs.amplication.com/how-to/custom-code

------------------------------------------------------------------------------
  */
import * as common from "@nestjs/common";
import * as swagger from "@nestjs/swagger";
import { isRecordNotFoundError } from "../../prisma.util";
import * as errors from "../../errors";
import { Request } from "express";
import { plainToClass } from "class-transformer";
import { ApiNestedQuery } from "../../decorators/api-nested-query.decorator";
import * as nestAccessControl from "nest-access-control";
import * as defaultAuthGuard from "../../auth/defaultAuth.guard";
import { TimeTableService } from "../timeTable.service";
import { AclValidateRequestInterceptor } from "../../interceptors/aclValidateRequest.interceptor";
import { AclFilterResponseInterceptor } from "../../interceptors/aclFilterResponse.interceptor";
import { TimeTableCreateInput } from "./TimeTableCreateInput";
import { TimeTableWhereInput } from "./TimeTableWhereInput";
import { TimeTableWhereUniqueInput } from "./TimeTableWhereUniqueInput";
import { TimeTableFindManyArgs } from "./TimeTableFindManyArgs";
import { TimeTableUpdateInput } from "./TimeTableUpdateInput";
import { TimeTable } from "./TimeTable";
import { ClassInfoTimetableFindManyArgs } from "../../classInfoTimetable/base/ClassInfoTimetableFindManyArgs";
import { ClassInfoTimetable } from "../../classInfoTimetable/base/ClassInfoTimetable";
import { ClassInfoTimetableWhereUniqueInput } from "../../classInfoTimetable/base/ClassInfoTimetableWhereUniqueInput";
import { SubjectInfoTimetableFindManyArgs } from "../../subjectInfoTimetable/base/SubjectInfoTimetableFindManyArgs";
import { SubjectInfoTimetable } from "../../subjectInfoTimetable/base/SubjectInfoTimetable";
import { SubjectInfoTimetableWhereUniqueInput } from "../../subjectInfoTimetable/base/SubjectInfoTimetableWhereUniqueInput";
import { TeachingInfoFindManyArgs } from "../../teachingInfo/base/TeachingInfoFindManyArgs";
import { TeachingInfo } from "../../teachingInfo/base/TeachingInfo";
import { TeachingInfoWhereUniqueInput } from "../../teachingInfo/base/TeachingInfoWhereUniqueInput";

@swagger.ApiBearerAuth()
@common.UseGuards(defaultAuthGuard.DefaultAuthGuard, nestAccessControl.ACGuard)
export class TimeTableControllerBase {
  constructor(
    protected readonly service: TimeTableService,
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder
  ) {}
  @common.UseInterceptors(AclValidateRequestInterceptor)
  @common.Post()
  @swagger.ApiCreatedResponse({ type: TimeTable })
  @swagger.ApiBody({
    type: TimeTableCreateInput,
  })
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "create",
    possession: "any",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async create(@common.Req() req: Request,@common.Body() data: TimeTableCreateInput): Promise<TimeTable> {
   // @ts-ignore
   const tenant = req?.user?.tenant

    return await this.service.create({
      data: {
        tenant,
        ...data,

        classroom: data.classroom
          ? {
              connect: data.classroom,
            }
          : undefined,
      },
      select: {
        classroom: {
          select: {
            id: true,
          },
        },

        createdAt: true,
        deletedAt: true,
        expiresAt: true,
        id: true,
        name: true,
        note: true,
        startedAt: true,
        status: true,
        tenant: true,
        updatedAt: true,
      },
    });
  }

  @common.UseInterceptors(AclFilterResponseInterceptor)
  @common.Get()
  @swagger.ApiOkResponse({ type: [TimeTable] })
  @ApiNestedQuery(TimeTableFindManyArgs)
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "read",
    possession: "any",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async findMany(@common.Req() request: Request): Promise<TimeTable[]> {
    const args = plainToClass(TimeTableFindManyArgs, request.query);
    return this.service.findMany({
      ...args,
      select: {
        classroom: {
          select: {
            id: true,
          },
        },

        createdAt: true,
        deletedAt: true,
        expiresAt: true,
        id: true,
        name: true,
        note: true,
        startedAt: true,
        status: true,
        tenant: true,
        updatedAt: true,
      },
    });
  }

  @common.UseInterceptors(AclFilterResponseInterceptor)
  @common.Get("/:id")
  @swagger.ApiOkResponse({ type: TimeTable })
  @swagger.ApiNotFoundResponse({ type: errors.NotFoundException })
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "read",
    possession: "own",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async findOne(
    @common.Param() params: TimeTableWhereUniqueInput
  ): Promise<TimeTable | null> {
    const result = await this.service.findOne({
      where: params,
      select: {
        classroom: {
          select: {
            id: true,
          },
        },

        createdAt: true,
        deletedAt: true,
        expiresAt: true,
        id: true,
        name: true,
        note: true,
        startedAt: true,
        status: true,
        tenant: true,
        updatedAt: true,
      },
    });
    if (result === null) {
      throw new errors.NotFoundException(
        `No resource was found for ${JSON.stringify(params)}`
      );
    }
    return result;
  }

  @common.UseInterceptors(AclValidateRequestInterceptor)
  @common.Patch("/:id")
  @swagger.ApiOkResponse({ type: TimeTable })
  @swagger.ApiNotFoundResponse({ type: errors.NotFoundException })
  @swagger.ApiBody({
    type: TimeTableUpdateInput,
  })
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async update(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() data: TimeTableUpdateInput
  ): Promise<TimeTable | null> {
    try {
      return await this.service.update({
        where: params,
        data: {
          ...data,

          classroom: data.classroom
            ? {
                connect: data.classroom,
              }
            : undefined,
        },
        select: {
          classroom: {
            select: {
              id: true,
            },
          },

          createdAt: true,
          deletedAt: true,
          expiresAt: true,
          id: true,
          name: true,
          note: true,
          startedAt: true,
          status: true,
          tenant: true,
          updatedAt: true,
        },
      });
    } catch (error) {
      if (isRecordNotFoundError(error)) {
        throw new errors.NotFoundException(
          `No resource was found for ${JSON.stringify(params)}`
        );
      }
      throw error;
    }
  }

  @common.Delete("/:id")
  @swagger.ApiOkResponse({ type: TimeTable })
  @swagger.ApiNotFoundResponse({ type: errors.NotFoundException })
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "delete",
    possession: "any",
  })
  @swagger.ApiForbiddenResponse({
    type: errors.ForbiddenException,
  })
  async delete(
    @common.Param() params: TimeTableWhereUniqueInput
  ): Promise<TimeTable | null> {
    try {
      return await this.service.delete({
        where: params,
        select: {
          classroom: {
            select: {
              id: true,
            },
          },

          createdAt: true,
          deletedAt: true,
          expiresAt: true,
          id: true,
          name: true,
          note: true,
          startedAt: true,
          status: true,
          tenant: true,
          updatedAt: true,
        },
      });
    } catch (error) {
      if (isRecordNotFoundError(error)) {
        throw new errors.NotFoundException(
          `No resource was found for ${JSON.stringify(params)}`
        );
      }
      throw error;
    }
  }

  @common.UseInterceptors(AclFilterResponseInterceptor)
  @common.Get("/:id/classInfoTimetables")
  @ApiNestedQuery(ClassInfoTimetableFindManyArgs)
  @nestAccessControl.UseRoles({
    resource: "ClassInfoTimetable",
    action: "read",
    possession: "any",
  })
  async findManyClassInfoTimetables(
    @common.Req() request: Request,
    @common.Param() params: TimeTableWhereUniqueInput
  ): Promise<ClassInfoTimetable[]> {
    const query = plainToClass(ClassInfoTimetableFindManyArgs, request.query);
    const results = await this.service.findClassInfoTimetables(params.id, {
      ...query,
      select: {
        classRoom: {
          select: {
            id: true,
          },
        },

        createdAt: true,
        deletedAt: true,
        id: true,
        numberLesson: true,

        subject: {
          select: {
            id: true,
          },
        },

        teacher: {
          select: {
            id: true,
          },
        },

        tenant: true,

        timeTable: {
          select: {
            id: true,
          },
        },

        updatedAt: true,
      },
    });
    if (results === null) {
      throw new errors.NotFoundException(
        `No resource was found for ${JSON.stringify(params)}`
      );
    }
    return results;
  }

  @common.Post("/:id/classInfoTimetables")
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  async connectClassInfoTimetables(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() body: ClassInfoTimetableWhereUniqueInput[]
  ): Promise<void> {
    const data = {
      classInfoTimetables: {
        connect: body,
      },
    };
    await this.service.update({
      where: params,
      data,
      select: { id: true },
    });
  }

  @common.Patch("/:id/classInfoTimetables")
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  async updateClassInfoTimetables(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() body: ClassInfoTimetableWhereUniqueInput[]
  ): Promise<void> {
    const data = {
      classInfoTimetables: {
        set: body,
      },
    };
    await this.service.update({
      where: params,
      data,
      select: { id: true },
    });
  }

  @common.Delete("/:id/classInfoTimetables")
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  async disconnectClassInfoTimetables(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() body: ClassInfoTimetableWhereUniqueInput[]
  ): Promise<void> {
    const data = {
      classInfoTimetables: {
        disconnect: body,
      },
    };
    await this.service.update({
      where: params,
      data,
      select: { id: true },
    });
  }

  @common.UseInterceptors(AclFilterResponseInterceptor)
  @common.Get("/:id/subjectInfoTimetables")
  @ApiNestedQuery(SubjectInfoTimetableFindManyArgs)
  @nestAccessControl.UseRoles({
    resource: "SubjectInfoTimetable",
    action: "read",
    possession: "any",
  })
  async findManySubjectInfoTimetables(
    @common.Req() request: Request,
    @common.Param() params: TimeTableWhereUniqueInput
  ): Promise<SubjectInfoTimetable[]> {
    const query = plainToClass(SubjectInfoTimetableFindManyArgs, request.query);
    const results = await this.service.findSubjectInfoTimetables(params.id, {
      ...query,
      select: {
        createdAt: true,
        deletedAt: true,
        id: true,
        limit: true,
        numberAdjacentLesson: true,

        subject: {
          select: {
            id: true,
          },
        },

        tenant: true,

        timeTable: {
          select: {
            id: true,
          },
        },

        updatedAt: true,
      },
    });
    if (results === null) {
      throw new errors.NotFoundException(
        `No resource was found for ${JSON.stringify(params)}`
      );
    }
    return results;
  }

  @common.Post("/:id/subjectInfoTimetables")
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  async connectSubjectInfoTimetables(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() body: SubjectInfoTimetableWhereUniqueInput[]
  ): Promise<void> {
    const data = {
      subjectInfoTimetables: {
        connect: body,
      },
    };
    await this.service.update({
      where: params,
      data,
      select: { id: true },
    });
  }

  @common.Patch("/:id/subjectInfoTimetables")
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  async updateSubjectInfoTimetables(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() body: SubjectInfoTimetableWhereUniqueInput[]
  ): Promise<void> {
    const data = {
      subjectInfoTimetables: {
        set: body,
      },
    };
    await this.service.update({
      where: params,
      data,
      select: { id: true },
    });
  }

  @common.Delete("/:id/subjectInfoTimetables")
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  async disconnectSubjectInfoTimetables(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() body: SubjectInfoTimetableWhereUniqueInput[]
  ): Promise<void> {
    const data = {
      subjectInfoTimetables: {
        disconnect: body,
      },
    };
    await this.service.update({
      where: params,
      data,
      select: { id: true },
    });
  }

  @common.UseInterceptors(AclFilterResponseInterceptor)
  @common.Get("/:id/teachingInfos")
  @ApiNestedQuery(TeachingInfoFindManyArgs)
  @nestAccessControl.UseRoles({
    resource: "TeachingInfo",
    action: "read",
    possession: "any",
  })
  async findManyTeachingInfos(
    @common.Req() request: Request,
    @common.Param() params: TimeTableWhereUniqueInput
  ): Promise<TeachingInfo[]> {
    const query = plainToClass(TeachingInfoFindManyArgs, request.query);
    const results = await this.service.findTeachingInfos(params.id, {
      ...query,
      select: {
        classrooms: {
          select: {
            id: true,
          },
        },

        createdAt: true,
        deletedAt: true,
        id: true,

        lessons: {
          select: {
            id: true,
          },
        },

        note: true,
        status: true,

        subjects: {
          select: {
            id: true,
          },
        },

        teacher: {
          select: {
            id: true,
          },
        },

        timeTable: {
          select: {
            id: true,
          },
        },

        updatedAt: true,
      },
    });
    if (results === null) {
      throw new errors.NotFoundException(
        `No resource was found for ${JSON.stringify(params)}`
      );
    }
    return results;
  }

  @common.Post("/:id/teachingInfos")
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  async connectTeachingInfos(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() body: TeachingInfoWhereUniqueInput[]
  ): Promise<void> {
    const data = {
      teachingInfos: {
        connect: body,
      },
    };
    await this.service.update({
      where: params,
      data,
      select: { id: true },
    });
  }

  @common.Patch("/:id/teachingInfos")
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  async updateTeachingInfos(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() body: TeachingInfoWhereUniqueInput[]
  ): Promise<void> {
    const data = {
      teachingInfos: {
        set: body,
      },
    };
    await this.service.update({
      where: params,
      data,
      select: { id: true },
    });
  }

  @common.Delete("/:id/teachingInfos")
  @nestAccessControl.UseRoles({
    resource: "TimeTable",
    action: "update",
    possession: "any",
  })
  async disconnectTeachingInfos(
    @common.Param() params: TimeTableWhereUniqueInput,
    @common.Body() body: TeachingInfoWhereUniqueInput[]
  ): Promise<void> {
    const data = {
      teachingInfos: {
        disconnect: body,
      },
    };
    await this.service.update({
      where: params,
      data,
      select: { id: true },
    });
  }
}
