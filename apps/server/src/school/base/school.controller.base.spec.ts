import { Test } from "@nestjs/testing";
import {
  INestApplication,
  HttpStatus,
  ExecutionContext,
  CallHandler,
} from "@nestjs/common";
import request from "supertest";
import { MorganModule } from "nest-morgan";
import { ACGuard } from "nest-access-control";
import { DefaultAuthGuard } from "../../auth/defaultAuth.guard";
import { ACLModule } from "../../auth/acl.module";
import { AclFilterResponseInterceptor } from "../../interceptors/aclFilterResponse.interceptor";
import { AclValidateRequestInterceptor } from "../../interceptors/aclValidateRequest.interceptor";
import { map } from "rxjs";
import { SchoolController } from "../school.controller";
import { SchoolService } from "../school.service";

const nonExistingId = "nonExistingId";
const existingId = "existingId";
const CREATE_INPUT = {
  address: "exampleAddress",
  avatar: "exampleAvatar",
  city: "exampleCity",
  createdAt: new Date(),
  deletedAt: new Date(),
  deputy: "exampleDeputy",
  deputyEmail: "exampleDeputyEmail",
  deputyPhone: "exampleDeputyPhone",
  district: "exampleDistrict",
  email: "exampleEmail",
  id: "exampleId",
  name: "exampleName",
  phone: "examplePhone",
  standard: "exampleStandard",
  tenant: "exampleTenant",
  updatedAt: new Date(),
  ward: "exampleWard",
};
const CREATE_RESULT = {
  address: "exampleAddress",
  avatar: "exampleAvatar",
  city: "exampleCity",
  createdAt: new Date(),
  deletedAt: new Date(),
  deputy: "exampleDeputy",
  deputyEmail: "exampleDeputyEmail",
  deputyPhone: "exampleDeputyPhone",
  district: "exampleDistrict",
  email: "exampleEmail",
  id: "exampleId",
  name: "exampleName",
  phone: "examplePhone",
  standard: "exampleStandard",
  tenant: "exampleTenant",
  updatedAt: new Date(),
  ward: "exampleWard",
};
const FIND_MANY_RESULT = [
  {
    address: "exampleAddress",
    avatar: "exampleAvatar",
    city: "exampleCity",
    createdAt: new Date(),
    deletedAt: new Date(),
    deputy: "exampleDeputy",
    deputyEmail: "exampleDeputyEmail",
    deputyPhone: "exampleDeputyPhone",
    district: "exampleDistrict",
    email: "exampleEmail",
    id: "exampleId",
    name: "exampleName",
    phone: "examplePhone",
    standard: "exampleStandard",
    tenant: "exampleTenant",
    updatedAt: new Date(),
    ward: "exampleWard",
  },
];
const FIND_ONE_RESULT = {
  address: "exampleAddress",
  avatar: "exampleAvatar",
  city: "exampleCity",
  createdAt: new Date(),
  deletedAt: new Date(),
  deputy: "exampleDeputy",
  deputyEmail: "exampleDeputyEmail",
  deputyPhone: "exampleDeputyPhone",
  district: "exampleDistrict",
  email: "exampleEmail",
  id: "exampleId",
  name: "exampleName",
  phone: "examplePhone",
  standard: "exampleStandard",
  tenant: "exampleTenant",
  updatedAt: new Date(),
  ward: "exampleWard",
};

const service = {
  create() {
    return CREATE_RESULT;
  },
  findMany: () => FIND_MANY_RESULT,
  findOne: ({ where }: { where: { id: string } }) => {
    switch (where.id) {
      case existingId:
        return FIND_ONE_RESULT;
      case nonExistingId:
        return null;
    }
  },
};

const basicAuthGuard = {
  canActivate: (context: ExecutionContext) => {
    const argumentHost = context.switchToHttp();
    const request = argumentHost.getRequest();
    request.user = {
      roles: ["user"],
    };
    return true;
  },
};

const acGuard = {
  canActivate: () => {
    return true;
  },
};

const aclFilterResponseInterceptor = {
  intercept: (context: ExecutionContext, next: CallHandler) => {
    return next.handle().pipe(
      map((data) => {
        return data;
      })
    );
  },
};
const aclValidateRequestInterceptor = {
  intercept: (context: ExecutionContext, next: CallHandler) => {
    return next.handle();
  },
};

describe("School", () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        {
          provide: SchoolService,
          useValue: service,
        },
      ],
      controllers: [SchoolController],
      imports: [MorganModule.forRoot(), ACLModule],
    })
      .overrideGuard(DefaultAuthGuard)
      .useValue(basicAuthGuard)
      .overrideGuard(ACGuard)
      .useValue(acGuard)
      .overrideInterceptor(AclFilterResponseInterceptor)
      .useValue(aclFilterResponseInterceptor)
      .overrideInterceptor(AclValidateRequestInterceptor)
      .useValue(aclValidateRequestInterceptor)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  test("POST /schools", async () => {
    await request(app.getHttpServer())
      .post("/schools")
      .send(CREATE_INPUT)
      .expect(HttpStatus.CREATED)
      .expect({
        ...CREATE_RESULT,
        createdAt: CREATE_RESULT.createdAt.toISOString(),
        deletedAt: CREATE_RESULT.deletedAt.toISOString(),
        updatedAt: CREATE_RESULT.updatedAt.toISOString(),
      });
  });

  test("GET /schools", async () => {
    await request(app.getHttpServer())
      .get("/schools")
      .expect(HttpStatus.OK)
      .expect([
        {
          ...FIND_MANY_RESULT[0],
          createdAt: FIND_MANY_RESULT[0].createdAt.toISOString(),
          deletedAt: FIND_MANY_RESULT[0].deletedAt.toISOString(),
          updatedAt: FIND_MANY_RESULT[0].updatedAt.toISOString(),
        },
      ]);
  });

  test("GET /schools/:id non existing", async () => {
    await request(app.getHttpServer())
      .get(`${"/schools"}/${nonExistingId}`)
      .expect(HttpStatus.NOT_FOUND)
      .expect({
        statusCode: HttpStatus.NOT_FOUND,
        message: `No resource was found for {"${"id"}":"${nonExistingId}"}`,
        error: "Not Found",
      });
  });

  test("GET /schools/:id existing", async () => {
    await request(app.getHttpServer())
      .get(`${"/schools"}/${existingId}`)
      .expect(HttpStatus.OK)
      .expect({
        ...FIND_ONE_RESULT,
        createdAt: FIND_ONE_RESULT.createdAt.toISOString(),
        deletedAt: FIND_ONE_RESULT.deletedAt.toISOString(),
        updatedAt: FIND_ONE_RESULT.updatedAt.toISOString(),
      });
  });

  test("POST /schools existing resource", async () => {
    const agent = request(app.getHttpServer());
    await agent
      .post("/schools")
      .send(CREATE_INPUT)
      .expect(HttpStatus.CREATED)
      .expect({
        ...CREATE_RESULT,
        createdAt: CREATE_RESULT.createdAt.toISOString(),
        deletedAt: CREATE_RESULT.deletedAt.toISOString(),
        updatedAt: CREATE_RESULT.updatedAt.toISOString(),
      })
      .then(function () {
        agent
          .post("/schools")
          .send(CREATE_INPUT)
          .expect(HttpStatus.CONFLICT)
          .expect({
            statusCode: HttpStatus.CONFLICT,
          });
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
