import { Module, forwardRef } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { SchoolModuleBase } from "./base/school.module.base";
import { SchoolService } from "./school.service";
import { SchoolController } from "./school.controller";

@Module({
  imports: [SchoolModuleBase, forwardRef(() => AuthModule)],
  controllers: [SchoolController],
  providers: [SchoolService],
  exports: [SchoolService],
})
export class SchoolModule {}
