import { AuthModule } from '../auth/auth.module';
import { AttendanceModuleBase } from './base/attendance.module.base';
import { AttendanceService } from './attendance.service';
import { AttendanceController } from './attendance.controller';
import { HttpModule } from '@nestjs/axios';
import { Module, forwardRef } from '@nestjs/common';
import { StudentService } from 'src/student/student.service';

@Module({
  imports: [AttendanceModuleBase, forwardRef(() => AuthModule), HttpModule],
  controllers: [AttendanceController],
  providers: [AttendanceService, StudentService],
  exports: [AttendanceService],
})
export class AttendanceModule {}
