import * as common from '@nestjs/common';
import * as swagger from '@nestjs/swagger';
import * as nestAccessControl from 'nest-access-control';
import { AttendanceService } from './attendance.service';
import { AttendanceControllerBase } from './base/attendance.controller.base';
import { HttpService } from '@nestjs/axios';
import { StudentService } from 'src/student/student.service';

@swagger.ApiTags('attendances - Điểm danh')
@common.Controller('attendances')
export class AttendanceController extends AttendanceControllerBase {
  constructor(
    protected readonly service: AttendanceService,
    protected readonly studentService: StudentService,
    protected readonly httpService: HttpService,
    @nestAccessControl.InjectRolesBuilder()
    protected readonly rolesBuilder: nestAccessControl.RolesBuilder,
  ) {
    super(service, httpService, studentService, rolesBuilder);
  }
}
