export interface ITokenPayload {
  id: string;
  username: string;
  password: string;
  tenant?: string;
}

export interface ITokenService {
  createToken: ({ id, username, password, tenant }: ITokenPayload) => Promise<string>;
}
