-- CreateEnum
CREATE TYPE "EnumSchoolTypeSchool" AS ENUM ('Option');

-- CreateEnum
CREATE TYPE "EnumSemesterStatus" AS ENUM ('Option1', 'Option_2');

-- CreateEnum
CREATE TYPE "EnumStudentGender" AS ENUM ('Option_1');

-- CreateEnum
CREATE TYPE "EnumStudentStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumClassroomStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumSubjectStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumTeacherGender" AS ENUM ('Option_1');

-- CreateEnum
CREATE TYPE "EnumTeacherTypeWork" AS ENUM ('Option_1', 'Option_2');

-- CreateEnum
CREATE TYPE "EnumParentRelation" AS ENUM ('Option_1');

-- CreateEnum
CREATE TYPE "EnumGradeStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumSchoolYearStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumNotifyStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumTeachingInfoStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumTimeTableStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumLessonStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumAttendanceStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumDepartmentStatus" AS ENUM ('Option1');

-- CreateEnum
CREATE TYPE "EnumSpecialLessonStatus" AS ENUM ('Option1');

-- CreateTable
CREATE TABLE "User" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "firstName" TEXT,
    "id" TEXT NOT NULL,
    "lastName" TEXT,
    "password" TEXT NOT NULL,
    "roles" JSONB NOT NULL,
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "username" TEXT NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "School" (
    "address" TEXT,
    "avatar" TEXT,
    "city" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "deputy" TEXT,
    "deputyEmail" TEXT,
    "deputyPhone" TEXT,
    "district" TEXT,
    "email" TEXT,
    "id" TEXT NOT NULL,
    "name" TEXT,
    "phone" TEXT,
    "standard" TEXT,
    "tenant" TEXT,
    "typeSchool" "EnumSchoolTypeSchool",
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "ward" TEXT,

    CONSTRAINT "School_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Semester" (
    "code" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TEXT,
    "id" TEXT NOT NULL,
    "name" TEXT,
    "note" TEXT,
    "status" "EnumSemesterStatus",
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Semester_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Student" (
    "address" TEXT,
    "avatar" TEXT,
    "birthday" TIMESTAMP(3),
    "classroomsId" TEXT,
    "code" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TEXT,
    "ethnicty" TEXT,
    "gender" "EnumStudentGender",
    "id" TEXT NOT NULL,
    "name" TEXT,
    "note" TEXT,
    "religion" TEXT,
    "status" "EnumStudentStatus",
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Student_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Classroom" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "gradesId" TEXT,
    "id" TEXT NOT NULL,
    "name" TEXT,
    "order" INTEGER,
    "schoolYearsId" TEXT,
    "status" "EnumClassroomStatus",
    "studentIds" JSONB,
    "teacher" TEXT,
    "tenant" TEXT,
    "timeTablesId" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Classroom_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Subject" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "name" TEXT,
    "note" TEXT,
    "status" "EnumSubjectStatus",
    "teachers" TEXT,
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Subject_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Teacher" (
    "address" TEXT,
    "birthday" TEXT,
    "code" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TEXT,
    "email" TEXT,
    "gender" "EnumTeacherGender",
    "id" TEXT NOT NULL,
    "level" TEXT,
    "manage" TEXT,
    "name" TEXT,
    "note" TEXT,
    "phone" TEXT,
    "tenant" TEXT,
    "typeWork" "EnumTeacherTypeWork",
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Teacher_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Parent" (
    "address" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "email" TEXT,
    "id" TEXT NOT NULL,
    "name" TEXT,
    "note" TEXT,
    "phone" TEXT,
    "relation" "EnumParentRelation",
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Parent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Grade" (
    "code" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "name" TEXT,
    "note" TEXT,
    "status" "EnumGradeStatus",
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Grade_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "SchoolYear" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "description" TEXT,
    "id" TEXT NOT NULL,
    "name" TEXT,
    "status" "EnumSchoolYearStatus",
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "SchoolYear_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Notify" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "description" TEXT,
    "file" TEXT,
    "id" TEXT NOT NULL,
    "status" "EnumNotifyStatus",
    "tenant" TEXT,
    "title" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Notify_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TeachingInfo" (
    "classroomsId" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "lessonsId" TEXT,
    "note" TEXT,
    "status" "EnumTeachingInfoStatus",
    "subjectsId" TEXT,
    "teacherId" TEXT,
    "timeTableId" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "TeachingInfo_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TimeTable" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "expiresAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "name" TEXT,
    "note" TEXT,
    "startedAt" TIMESTAMP(3),
    "status" "EnumTimeTableStatus",
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "TimeTable_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Lesson" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "name" TEXT,
    "note" TEXT,
    "status" "EnumLessonStatus",
    "tenant" TEXT,
    "timeFrom" TIMESTAMP(3),
    "timeTo" TIMESTAMP(3),
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Lesson_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Attendance" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "lessonId" TEXT,
    "note" TEXT,
    "status" "EnumAttendanceStatus",
    "studentId" TEXT,
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Attendance_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "StudyTime" (
    "afternoon" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "morning" TEXT,
    "tenant" TEXT,
    "timeLesson" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "StudyTime_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Department" (
    "code" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "description" TEXT,
    "id" TEXT NOT NULL,
    "name" TEXT,
    "status" "EnumDepartmentStatus",
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Department_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "SpecialLesson" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "lessonsId" TEXT,
    "status" "EnumSpecialLessonStatus",
    "subjectId" TEXT,
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "SpecialLesson_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "SubjectInfoTimetable" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "limit" INTEGER,
    "numberAdjacentLesson" TEXT,
    "subjectId" TEXT,
    "tenant" TEXT,
    "timeTableId" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "SubjectInfoTimetable_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Curriculum" (
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "gradeId" TEXT,
    "id" TEXT NOT NULL,
    "numberAdjacentLesson" INTEGER,
    "numberLesson" INTEGER,
    "subjectId" TEXT,
    "tenant" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Curriculum_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ClassInfoTimetable" (
    "classRoomId" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),
    "id" TEXT NOT NULL,
    "numberLesson" INTEGER,
    "subjectId" TEXT,
    "teacherId" TEXT,
    "tenant" TEXT,
    "timeTableId" TEXT,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "ClassInfoTimetable_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_ParentToStudent" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_DepartmentToTeacher" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");

-- CreateIndex
CREATE UNIQUE INDEX "Semester_code_key" ON "Semester"("code");

-- CreateIndex
CREATE UNIQUE INDEX "Student_code_key" ON "Student"("code");

-- CreateIndex
CREATE UNIQUE INDEX "Classroom_timeTablesId_key" ON "Classroom"("timeTablesId");

-- CreateIndex
CREATE UNIQUE INDEX "Teacher_code_key" ON "Teacher"("code");

-- CreateIndex
CREATE UNIQUE INDEX "Parent_phone_key" ON "Parent"("phone");

-- CreateIndex
CREATE UNIQUE INDEX "Grade_code_key" ON "Grade"("code");

-- CreateIndex
CREATE UNIQUE INDEX "Department_code_key" ON "Department"("code");

-- CreateIndex
CREATE UNIQUE INDEX "_ParentToStudent_AB_unique" ON "_ParentToStudent"("A", "B");

-- CreateIndex
CREATE INDEX "_ParentToStudent_B_index" ON "_ParentToStudent"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_DepartmentToTeacher_AB_unique" ON "_DepartmentToTeacher"("A", "B");

-- CreateIndex
CREATE INDEX "_DepartmentToTeacher_B_index" ON "_DepartmentToTeacher"("B");

-- AddForeignKey
ALTER TABLE "Student" ADD CONSTRAINT "Student_classroomsId_fkey" FOREIGN KEY ("classroomsId") REFERENCES "Classroom"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Classroom" ADD CONSTRAINT "Classroom_gradesId_fkey" FOREIGN KEY ("gradesId") REFERENCES "Grade"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Classroom" ADD CONSTRAINT "Classroom_schoolYearsId_fkey" FOREIGN KEY ("schoolYearsId") REFERENCES "SchoolYear"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Classroom" ADD CONSTRAINT "Classroom_timeTablesId_fkey" FOREIGN KEY ("timeTablesId") REFERENCES "TimeTable"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TeachingInfo" ADD CONSTRAINT "TeachingInfo_classroomsId_fkey" FOREIGN KEY ("classroomsId") REFERENCES "Classroom"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TeachingInfo" ADD CONSTRAINT "TeachingInfo_lessonsId_fkey" FOREIGN KEY ("lessonsId") REFERENCES "Lesson"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TeachingInfo" ADD CONSTRAINT "TeachingInfo_subjectsId_fkey" FOREIGN KEY ("subjectsId") REFERENCES "Subject"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TeachingInfo" ADD CONSTRAINT "TeachingInfo_teacherId_fkey" FOREIGN KEY ("teacherId") REFERENCES "Teacher"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TeachingInfo" ADD CONSTRAINT "TeachingInfo_timeTableId_fkey" FOREIGN KEY ("timeTableId") REFERENCES "TimeTable"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Attendance" ADD CONSTRAINT "Attendance_lessonId_fkey" FOREIGN KEY ("lessonId") REFERENCES "Lesson"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Attendance" ADD CONSTRAINT "Attendance_studentId_fkey" FOREIGN KEY ("studentId") REFERENCES "Student"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SpecialLesson" ADD CONSTRAINT "SpecialLesson_lessonsId_fkey" FOREIGN KEY ("lessonsId") REFERENCES "Lesson"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SpecialLesson" ADD CONSTRAINT "SpecialLesson_subjectId_fkey" FOREIGN KEY ("subjectId") REFERENCES "Subject"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SubjectInfoTimetable" ADD CONSTRAINT "SubjectInfoTimetable_subjectId_fkey" FOREIGN KEY ("subjectId") REFERENCES "Subject"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SubjectInfoTimetable" ADD CONSTRAINT "SubjectInfoTimetable_timeTableId_fkey" FOREIGN KEY ("timeTableId") REFERENCES "TimeTable"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Curriculum" ADD CONSTRAINT "Curriculum_gradeId_fkey" FOREIGN KEY ("gradeId") REFERENCES "Grade"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Curriculum" ADD CONSTRAINT "Curriculum_subjectId_fkey" FOREIGN KEY ("subjectId") REFERENCES "Subject"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ClassInfoTimetable" ADD CONSTRAINT "ClassInfoTimetable_classRoomId_fkey" FOREIGN KEY ("classRoomId") REFERENCES "Classroom"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ClassInfoTimetable" ADD CONSTRAINT "ClassInfoTimetable_subjectId_fkey" FOREIGN KEY ("subjectId") REFERENCES "Subject"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ClassInfoTimetable" ADD CONSTRAINT "ClassInfoTimetable_teacherId_fkey" FOREIGN KEY ("teacherId") REFERENCES "Teacher"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ClassInfoTimetable" ADD CONSTRAINT "ClassInfoTimetable_timeTableId_fkey" FOREIGN KEY ("timeTableId") REFERENCES "TimeTable"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ParentToStudent" ADD CONSTRAINT "_ParentToStudent_A_fkey" FOREIGN KEY ("A") REFERENCES "Parent"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ParentToStudent" ADD CONSTRAINT "_ParentToStudent_B_fkey" FOREIGN KEY ("B") REFERENCES "Student"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_DepartmentToTeacher" ADD CONSTRAINT "_DepartmentToTeacher_A_fkey" FOREIGN KEY ("A") REFERENCES "Department"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_DepartmentToTeacher" ADD CONSTRAINT "_DepartmentToTeacher_B_fkey" FOREIGN KEY ("B") REFERENCES "Teacher"("id") ON DELETE CASCADE ON UPDATE CASCADE;
