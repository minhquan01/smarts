-- AlterTable
ALTER TABLE "School" ADD COLUMN     "coverImage" TEXT,
ADD COLUMN     "deputyPosition" TEXT,
ADD COLUMN     "level" TEXT,
ADD COLUMN     "type" TEXT,
ADD COLUMN     "yearReview" TEXT;
