/*
  Warnings:

  - The `status` column on the `Classroom` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - Made the column `name` on table `Teacher` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Classroom" ADD COLUMN     "specialize" TEXT,
ALTER COLUMN "name" DROP NOT NULL,
DROP COLUMN "status",
ADD COLUMN     "status" BOOLEAN;

-- AlterTable
ALTER TABLE "Student" ADD COLUMN     "studentId" INTEGER;

-- AlterTable
ALTER TABLE "Teacher" ALTER COLUMN "name" SET NOT NULL;
