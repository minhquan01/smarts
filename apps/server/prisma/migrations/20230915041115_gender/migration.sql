/*
  Warnings:

  - The values [Option_1] on the enum `EnumStudentGender` will be removed. If these variants are still used in the database, this will fail.
  - The values [Option_1] on the enum `EnumTeacherGender` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "EnumStudentGender_new" AS ENUM ('MALE', 'FEMALE', 'OTHER');
ALTER TABLE "Student" ALTER COLUMN "gender" TYPE "EnumStudentGender_new" USING ("gender"::text::"EnumStudentGender_new");
ALTER TYPE "EnumStudentGender" RENAME TO "EnumStudentGender_old";
ALTER TYPE "EnumStudentGender_new" RENAME TO "EnumStudentGender";
DROP TYPE "EnumStudentGender_old";
COMMIT;

-- AlterEnum
BEGIN;
CREATE TYPE "EnumTeacherGender_new" AS ENUM ('MALE', 'FEMALE', 'OTHER');
ALTER TABLE "Teacher" ALTER COLUMN "gender" TYPE "EnumTeacherGender_new" USING ("gender"::text::"EnumTeacherGender_new");
ALTER TYPE "EnumTeacherGender" RENAME TO "EnumTeacherGender_old";
ALTER TYPE "EnumTeacherGender_new" RENAME TO "EnumTeacherGender";
DROP TYPE "EnumTeacherGender_old";
COMMIT;
