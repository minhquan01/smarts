/*
  Warnings:

  - Made the column `phone` on table `Parent` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Parent" ALTER COLUMN "phone" SET NOT NULL;
