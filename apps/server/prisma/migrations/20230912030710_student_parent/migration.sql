/*
  Warnings:

  - You are about to drop the column `ethnicty` on the `Student` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Student" DROP COLUMN "ethnicty",
ADD COLUMN     "birthplace" TEXT,
ADD COLUMN     "ethnicity" TEXT;
