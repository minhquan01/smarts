/*
  Warnings:

  - The `birthday` column on the `Teacher` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The `deletedAt` column on the `Teacher` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - Made the column `name` on table `Classroom` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Classroom" ALTER COLUMN "name" SET NOT NULL;

-- AlterTable
ALTER TABLE "Teacher" DROP COLUMN "birthday",
ADD COLUMN     "birthday" TIMESTAMP(3),
DROP COLUMN "deletedAt",
ADD COLUMN     "deletedAt" TIMESTAMP(3);
