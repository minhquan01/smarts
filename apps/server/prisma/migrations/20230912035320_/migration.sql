/*
  Warnings:

  - The values [Option_1] on the enum `EnumParentRelation` will be removed. If these variants are still used in the database, this will fail.
  - The values [Option1] on the enum `EnumStudentStatus` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "EnumParentRelation_new" AS ENUM ('FATHER', 'MOTHER', 'GRANDFATHER', 'GRANDMOTHER', 'BROTHERS', 'SISTERS', 'OTHER');
ALTER TABLE "Parent" ALTER COLUMN "relation" TYPE "EnumParentRelation_new" USING ("relation"::text::"EnumParentRelation_new");
ALTER TYPE "EnumParentRelation" RENAME TO "EnumParentRelation_old";
ALTER TYPE "EnumParentRelation_new" RENAME TO "EnumParentRelation";
DROP TYPE "EnumParentRelation_old";
COMMIT;

-- AlterEnum
BEGIN;
CREATE TYPE "EnumStudentStatus_new" AS ENUM ('STUDY', 'STOP_STUDY', 'PRESERVE');
ALTER TABLE "Student" ALTER COLUMN "status" TYPE "EnumStudentStatus_new" USING ("status"::text::"EnumStudentStatus_new");
ALTER TYPE "EnumStudentStatus" RENAME TO "EnumStudentStatus_old";
ALTER TYPE "EnumStudentStatus_new" RENAME TO "EnumStudentStatus";
DROP TYPE "EnumStudentStatus_old";
COMMIT;
