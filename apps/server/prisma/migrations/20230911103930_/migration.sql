/*
  Warnings:

  - You are about to drop the column `teacher` on the `Classroom` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Classroom" DROP COLUMN "teacher",
ADD COLUMN     "teacherId" TEXT;

-- AddForeignKey
ALTER TABLE "Classroom" ADD CONSTRAINT "Classroom_teacherId_fkey" FOREIGN KEY ("teacherId") REFERENCES "Teacher"("id") ON DELETE SET NULL ON UPDATE CASCADE;
