// Images
export { default as Avatar } from "./images/avatar.png";
export { default as LogoSmart } from "./images/logo-smart.png";
export { default as Logo } from "./images/logo.png";
export { default as LogoSmartBlack } from "./images/logo_black.png";
export { default as SmartsLogoAuth } from "./images/smarts.png";
export { default as User } from "./images/user.png";

// Icons
export { default as IconArrowDown } from "./icons/arrow.down";
export { default as IconArrowUp } from "./icons/arrow.up";
export { default as IconBell } from "./icons/bell";
export { default as IconBook } from "./icons/book";
export { default as IconCamera } from "./icons/camera";
export { default as IconDirectboxReceive } from "./icons/directbox.receive";
export { default as IconDirectboxSend } from "./icons/directbox.send";
export { default as IconDocumentDownload } from "./icons/document.download";
export { default as IconExtend } from "./icons/extend";
export { default as IconGallery } from "./icons/gallery";
export { default as IconGenerality } from "./icons/generality";
export { default as IconList } from "./icons/list";
export { default as IconMenu } from "./icons/menu";
export { default as IconMenuBoard } from "./icons/menu.board";
export { default as IconMortarBoard } from "./icons/mortar.board";
export { default as IconReport } from "./icons/report";
export { default as IconRoundPlus } from "./icons/round.plus";
export { default as IconSetting } from "./icons/setting";
export { default as IconStickyNote } from "./icons/sticky.note";
export { default as IconSwitchOff } from "./icons/switch_off.svg";
export { default as IconSwitchOn } from "./icons/switch_on.svg";
export { default as IconUserCheck } from "./icons/user.check";
