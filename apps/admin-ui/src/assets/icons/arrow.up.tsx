import React from 'react';

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        fill="#292D32"
        d="M16.227 15H7.771c-.686 0-1.029-.91-.543-1.445l3.7-4.066a1.427 1.427 0 012.15 0l1.407 1.546 2.292 2.52c.479.534.136 1.445-.55 1.445z"
      ></path>
    </svg>
  );
}

export default Icon;
