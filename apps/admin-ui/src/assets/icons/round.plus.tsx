import React from "react";

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <circle cx="12" cy="12" r="9" fill="#00B4EC"></circle>
      <path
        stroke="#fff"
        strokeLinecap="round"
        strokeWidth="1.6"
        d="M12 8v8M16 12H8"
      ></path>
    </svg>
  );
}

export default Icon;
