import React from 'react';

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="none"
      viewBox="0 0 32 32"
    >
      <path
        stroke="#292D32"
        strokeLinecap="round"
        strokeWidth="3"
        d="M4 8h24M4 16h24M4 24h24"
      ></path>
    </svg>
  );
}

export default Icon;
