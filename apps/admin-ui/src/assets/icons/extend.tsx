import React from 'react';

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <rect
        width="24"
        height="24"
        fill="url(#paint0_linear_82_3578)"
        rx="12"
        transform="matrix(-1 0 0 1 24 0)"
      ></rect>
      <path
        stroke="#fff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M14 7l-5 5 5 5"
      ></path>
      <defs>
        <linearGradient
          id="paint0_linear_82_3578"
          x1="0"
          x2="24"
          y1="0"
          y2="0"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00B4EC"></stop>
          <stop offset="1" stopColor="#FEF834"></stop>
        </linearGradient>
      </defs>
    </svg>
  );
}

export default Icon;
