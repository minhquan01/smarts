import React from 'react';

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        fill="#292D32"
        d="M7.773 9h8.457c.685 0 1.028.91.542 1.445l-3.7 4.066a1.427 1.427 0 01-2.15 0l-1.406-1.546-2.293-2.52C6.744 9.91 7.087 9 7.773 9z"
      ></path>
    </svg>
  );
}

export default Icon;
