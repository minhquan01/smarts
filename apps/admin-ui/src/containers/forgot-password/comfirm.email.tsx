import ButtonCustom from "@admin-ui/components/button/button.custom";
import RenderInput from "@admin-ui/components/render-input";
import TitleAuth from "@admin-ui/components/ui/text/title.auth";
import { Button, Checkbox, Grid, Group } from "@mantine/core";
import { useRouter } from "next/router";
import React from "react";
const fieldsLogin = [
  {
    type: "text",
    span: 12,
    placeholder: "Email",
    name: "email",
  },
];
const ComfirmEmail = () => {
  return (
    <>
      <TitleAuth
        title="Quên mật khẩu"
        desc="Vui lòng nhập đúng email mà bạn đã đăng ký tài khoản, chúng tôi sẽ gửi mã xác nhận đến email của bạn."
      />
      <Grid>
        {fieldsLogin.map((item, idx) => (
          <RenderInput
            key={idx}
            type={item?.type}
            span={item?.span}
            name={item?.name}
            placeholder={item?.placeholder}
          />
        ))}
      </Grid>
      <Group position="left" mt="md">
        <ButtonCustom type="submit">Xác nhận</ButtonCustom>
      </Group>
    </>
  );
};

export default ComfirmEmail;
