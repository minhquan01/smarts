import ButtonCustom from "@admin-ui/components/button/button.custom";
import RenderInput from "@admin-ui/components/render-input";
import TitleAuth from "@admin-ui/components/ui/text/title.auth";
import { Button, Checkbox, Grid, Group } from "@mantine/core";
import { useRouter } from "next/router";
import React from "react";
const fieldsCreatePassword = [
  {
    type: "password",
    span: 12,
    placeholder: "Nhập mật khẩu mới",
    name: "newPass",
  },
  {
    type: "password",
    span: 12,
    placeholder: "Xác mật khẩu mới",
    name: "repeatNewPass",
  },
];
const CreatePassword = () => {
  return (
    <>
      <TitleAuth
        title="Tạo mật khẩu mới"
        desc="Bạn vui lòng nhập mật khẩu mới"
      />
      <i className="mb-2 flex items-center gap-x-1 text-[#292D32]/50">
        <p className="text-red-500">*</p> Mật khẩu từ 8 ký tự bao gồm chữ viết
        thường, chữ viết hoa và số
      </i>
      <Grid>
        {fieldsCreatePassword.map((item, idx) => (
          <RenderInput
            key={idx}
            type={item?.type}
            span={item?.span}
            name={item?.name}
            placeholder={item?.placeholder}
          />
        ))}
      </Grid>
      <Group position="left" mt="md">
        <ButtonCustom type="submit">Xác nhận</ButtonCustom>
      </Group>
    </>
  );
};

export default CreatePassword;
