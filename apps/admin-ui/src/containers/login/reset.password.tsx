import ButtonCustom from "@admin-ui/components/button/button.custom";
import RenderInput from "@admin-ui/components/render-input";
import TitleAuth from "@admin-ui/components/ui/text/title.auth";
import { Button, Checkbox, Grid, Group } from "@mantine/core";
import { useRouter } from "next/router";
import React from "react";
const fieldsReset = [
  {
    type: "text",
    span: 12,
    placeholder: "Mật khẩu hiện tại",
    name: "currentPass",
  },
  {
    type: "password",
    span: 12,
    placeholder: "Mật khẩu mới",
    name: "newPass",
  },
  {
    type: "password",
    span: 12,
    placeholder: "Nhập lại mật khẩu mới",
    name: "repeatPassword",
  },
];
const ResetPassword = () => {
  const router = useRouter();
  return (
    <>
      <TitleAuth
        title="Chào mừng bạn đến với Smart-S"
        desc="Vui lòng thay đổi mật khẩu trong lần đăng nhập đầu tiên"
      />
      <Grid>
        {fieldsReset.map((item, idx) => (
          <RenderInput
            key={idx}
            type={item?.type}
            span={item?.span}
            name={item?.name}
            placeholder={item?.placeholder}
          />
        ))}
      </Grid>

      <Group position="left" mt="md">
        <ButtonCustom type="submit">Cập nhật</ButtonCustom>
      </Group>

      <p
        onClick={() => router.push("/forgot-password")}
        style={{ color: " var(--00-b-4-ec, #00B4EC)" }}
        className="  text-center   sm:hidden  text-base font-semibold mt-6 cursor-pointer "
      >
        Quên mật khẩu
      </p>
    </>
  );
};

export default ResetPassword;
