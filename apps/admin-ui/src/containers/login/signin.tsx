import ButtonCustom from "@admin-ui/components/button/button.custom";
import RenderInput from "@admin-ui/components/render-input";
import TitleAuth from "@admin-ui/components/ui/text/title.auth";
import { Button, Checkbox, Grid, Group } from "@mantine/core";
import { useRouter } from "next/router";
import React from "react";
const fieldsLogin = [
  {
    type: "text",
    span: 12,
    label: "Mã trường học",
    name: "code",
    require: true,
  },
  {
    type: "text",
    span: 12,
    label: "Email",
    name: "email",
    require: true,
  },
  {
    type: "password",
    span: 12,
    label: "Mật khẩu",
    name: "password",
    require: true,
  },
];
const SignIn = () => {
  const router = useRouter();
  return (
    <>
      <TitleAuth
        title="Chào mừng bạn đến với Smart-S"
        desc="Vui lòng điền thông tin tên tài khoản và mật khẩu để đăng nhập"
      />
      <Grid>
        {fieldsLogin.map((item, idx) => (
          <RenderInput
            key={idx}
            type={item?.type}
            label={item?.label}
            required={item?.require}
            span={item?.span}
            name={item?.name}
          />
        ))}
      </Grid>

      <div className="flex items-center justify-between">
        <Checkbox
          className="font-medium"
          label="Lưu thông tin đăng nhập"
          mt="24px"
          styles={{
            label: {
              fontSize: "16px",
              color: "#292D32",
            },
          }}
        />

        <p
          onClick={() => router.push("/forgot-password")}
          style={{ color: " var(--00-b-4-ec, #00B4EC)" }}
          className="hidden sm:block text-base font-semibold mt-6 cursor-pointer "
        >
          Quên mật khẩu
        </p>
      </div>

      <Group position="left" mt="md">
        <ButtonCustom type="submit">Đăng nhập</ButtonCustom>
      </Group>

      <p
        onClick={() => router.push("/forgot-password")}
        style={{ color: " var(--00-b-4-ec, #00B4EC)" }}
        className="  text-center   sm:hidden  text-base font-semibold mt-6 cursor-pointer "
      >
        Quên mật khẩu
      </p>
    </>
  );
};

export default SignIn;
