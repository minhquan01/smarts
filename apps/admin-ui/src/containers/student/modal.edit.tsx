import { ConstUseForm } from "@admin-ui/components/core-form/form.context";
import ModalCustom from "@admin-ui/components/modal/modal.custom";
import RenderInput from "@admin-ui/components/render-input";
import { IModalProps } from "@admin-ui/types";
import { Box, Grid, Title } from "@mantine/core";
import React, { useEffect } from "react";
import InfoStudent from "./info.student";
import { useGetOne, useUpdate } from "@hola/ui-core";
import { convertDateToString } from "@admin-ui/constants/convert";
import dayjs from "dayjs";

interface IModalEditStudentProps extends IModalProps {
  idEdit: string;
}

const ModalEditStudent = (props: IModalEditStudentProps) => {
  const { close, opened, idEdit, refetch } = props;
  const form = ConstUseForm({
    initialValues: {},
  });
  const { data: dataDetailStudent, refetch: refetchEdit } = useGetOne(
    "students",
    {
      id: idEdit,
    },
    {
      onSuccess() {
        // refetch();
      },
      enabled: false,
    }
  );

  useEffect(() => {
    console.log("first", dataDetailStudent);
  }, [dataDetailStudent]);
  const [update] = useUpdate(
    "students",
    {
      id: idEdit,
    },
    {}
  );
  const handleClose = () => {
    close();
    form.reset();
  };

  console.log({ dataDetailStudent });
  const handleSubmit = (values: any) => {
    console.log({
      values: new Date(
        String(values.monthBirth) +
          "/" +
          String(values.dayBirth) +
          "/" +
          String(values.yearBirth)
      ),
    });
    console.log({ values });
    const dataEdit = {
      code: values.code,
      classrooms: {
        id: values.class,
      },
      birthplace: values.birthplace,
      address: values.address,
      birthday: new Date(
        String(values.monthBirth) +
          "/" +
          String(values.dayBirth) +
          "/" +
          String(values.yearBirth)
      )?.toISOString(),
      gender: values.gender,
      name: values.name,
      ethnicity: values.ethnicity,
      note: values.note,
      religion: values.religion,
      status: values.status,
      parents: [
        {
          address: values.addressParents,
          email: values.emailParents,
          name: values.nameParents,
          note: values.noteParents,
          phone: values.phoneNumberParents,
          relation: values.relationship,
        },
      ],
    };
    console.log({ dataEdit });
    const res = update("students", { data: dataEdit });
    handleClose();
  };

  useEffect(() => {
    refetch;
  }, [idEdit, opened]);

  useEffect(() => {
    const parent = dataDetailStudent?.parents?.[0];
    form.setValues({
      ...dataDetailStudent,
      addressParents: parent?.address,
      nameParents: parent?.name,
      relationship: parent?.relation,
      phoneNumberParents: parent?.phone,
      emailParents: parent?.email,
      noteParents: parent?.note,
      class: dataDetailStudent?.classrooms?.id,
      dayBirth: dayjs(dataDetailStudent?.birthday).get("date"),
      monthBirth: dayjs(dataDetailStudent?.birthday).get("month") + 1,
      yearBirth: dayjs(dataDetailStudent?.birthday).get("year"),
    });
  }, [dataDetailStudent, opened]);

  return (
    <ModalCustom
      title="Chỉnh sửa thông tin học sinh"
      handleSubmit={handleSubmit}
      opened={opened}
      onClose={handleClose}
      actionText="Lưu"
      form={form}
      size="1128px"
    >
      <InfoStudent form={form} />
    </ModalCustom>
  );
};

export default ModalEditStudent;
