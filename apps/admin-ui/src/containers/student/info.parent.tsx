import RenderInput from "@admin-ui/components/render-input";
import { defaultSelectRelation } from "@admin-ui/constants";
import { Box, Grid } from "@mantine/core";
import React from "react";

const fieldsInfoParents = [
  {
    type: "text",
    span: 3,
    label: "Họ và tên",
    name: "nameParents",
    require: true,
  },
  {
    type: "select",
    span: 3,
    label: "Mối quan hệ",
    name: "relationship",
    require: true,
    data: defaultSelectRelation,
  },
  {
    type: "text",
    span: 3,
    label: "Số điện thoại",
    name: "phoneNumberParents",
    require: true,
  },
  {
    type: "text",
    span: 3,
    label: "Email",
    name: "emailParents",
  },
  {
    type: "text",
    span: 6,
    label: "Địa chỉ",
    name: "addressParents",
  },
  {
    type: "text",
    span: 6,
    label: "Ghi chú",
    name: "noteParents",
  },
];
const InfoParent = () => {
  return (
    <Box className="bg-[rgba(0,180,236,0.10)] rounded-2xl px-7 py-8">
      <Grid>
        {fieldsInfoParents.map((_data, index) => (
          <RenderInput
            key={index}
            type={_data?.type}
            label={_data?.label}
            required={_data?.require}
            span={_data?.span}
            data={_data?.data}
            name={_data?.name}
          />
        ))}
      </Grid>
    </Box>
  );
};

export default InfoParent;
