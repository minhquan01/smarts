import { InputDateForm } from "@admin-ui/components/core-form/input-core";
import InputDateBirthForm from "@admin-ui/components/core-form/input-core/input.date.field";
import RenderInput from "@admin-ui/components/render-input";
import { IParentProps, IStudentProps } from "@admin-ui/types";
import { Box, Col, Grid, Input, Title } from "@mantine/core";
import React, { useEffect, useState } from "react";
import InfoParent from "./info.parent";
import { useGetList } from "@hola/ui-core";
import { defaultGender, defaultSelectStatusStudent } from "@admin-ui/constants";
import SelectSearchForm from "@admin-ui/components/core-form/select.search.form";
import { IconLoader } from "@tabler/icons-react";
import { ethnicity } from "@admin-ui/constants/ethnicity";
import { religions } from "@admin-ui/constants/religion";
import { UseFormReturnType } from "@mantine/form";
import dayjs from "dayjs";

const fieldsInfoStudent = [
  {
    type: "text",
    span: 3,
    label: "Mã học sinh",
    name: "code",
    require: true,
    disabled: true,
  },
  {
    type: "text",
    span: 3,
    label: "Họ và tên",
    name: "name",
    require: true,
  },
  {
    type: "select",
    span: 2,
    label: "Giới tính",
    name: "gender",
    require: true,
    data: defaultGender,
  },
  {
    type: "date-birth",
    span: 4,
    label: "Ngày sinh",
    nameDay: "dayBirth",
    nameMonth: "monthBirth",
    nameYear: "yearBirth",
    require: true,
  },
  {
    type: "select",
    span: 3,
    label: "Lớp",
    name: "class",
    require: true,
    data: [],
  },
  {
    type: "select",
    span: 3,
    label: "Dân tộc",
    name: "ethnicity",
    require: false,
    data: ethnicity,
  },
  {
    type: "select",
    span: 3,
    label: "Tôn giáo",
    name: "religion",
    require: false,
    data: religions,
  },
  {
    type: "select",
    span: 3,
    label: "Tình trạng",
    name: "status",
    require: false,
    data: defaultSelectStatusStudent,
  },
  {
    type: "text",
    span: 6,
    label: "Nơi sinh",
    name: "birthplace",
    require: false,
  },
  {
    type: "text",
    span: 6,
    label: "Địa chỉ hiện tại",
    name: "address",
    require: false,
  },
  {
    type: "text",
    span: 12,
    label: "Ghi chú",
    name: "note",
    require: false,
  },
];

interface InfoStudentProps {
  dataEdit?: IStudentProps[];
  form: UseFormReturnType<unknown, (values: unknown) => unknown>;
}

const InfoStudent = ({ dataEdit, form }: InfoStudentProps) => {
  const { data } = useGetList("classrooms");
  const [dataSelectClass, setDataSelectClass] = useState<any>();
  const [ethnicitySearch, setEthnicitySearch] = useState("");
  const [selectEthnicity, setSelectEthnicity] = useState<any>();
  const [religionSearch, setReligionSearch] = useState("");
  const [selectReligion, setSelectReligion] = useState<any>();
  useEffect(() => {
    if (!data) {
      return;
    } else {
      const dataMap = data?.map((item) => ({
        value: item.id,
        label: item.name,
      }));
      setDataSelectClass(dataMap);
    }
  }, [data]);

  useEffect(() => {
    form.setFieldValue("ethnicity", selectEthnicity);
    form.setFieldValue("religion", selectReligion);
  }, [selectEthnicity, selectReligion]);

  return (
    <>
      <Box>
        <Title className="text-lg font-semibold">Thông tin học sinh</Title>
        <Box className="mt-10">
          <Grid gutter={"50px"}>
            <RenderInput span={2} type="upload-image" name="upload" />
            <Grid.Col span={10}>
              <Grid>
                {fieldsInfoStudent?.map((_data, index) => (
                  <>
                    {_data.name === "ethnicity" || _data.name === "religion" ? (
                      <Grid.Col span={_data.span}>
                        <SelectSearchForm
                          label={_data.label}
                          withAsterisk
                          name={_data.name}
                          // nothingFound={isFetching ? 'Loading...' : 'Không tồn tại'}
                          // rightSection={
                          //   isFetching ? (
                          //     <IconLoader
                          //       size="1rem"
                          //       className="animate-spin text-btn-blue"
                          //     />
                          //   ) : null
                          // }
                          data={_data.data}
                          onChange={
                            _data.name === "ethnicity"
                              ? setSelectEthnicity
                              : setSelectReligion
                          }
                          onKeyUp={(word) => {
                            // @ts-ignore
                            _data.name === "ethnicity"
                              ? setEthnicitySearch(word.target?.value)
                              : setReligionSearch(word.target?.value);
                          }}
                        />
                      </Grid.Col>
                    ) : (
                      <RenderInput
                        key={index}
                        type={_data?.type}
                        label={_data?.label}
                        required={_data?.require}
                        span={_data?.span}
                        data={
                          _data.name === "class"
                            ? dataSelectClass ?? []
                            : _data?.data
                        }
                        // data={_data?.data}
                        name={_data?.name}
                        nameDay={_data?.nameDay}
                        nameMonth={_data?.nameMonth}
                        nameYear={_data?.nameYear}
                        disabled={_data.disabled}
                      />
                    )}
                  </>
                ))}
              </Grid>
            </Grid.Col>
          </Grid>
        </Box>
      </Box>

      <Box className="mt-10">
        <Title className="text-base font-semibold mb-6">
          Thông tin phụ huynh
        </Title>
        {/* {dataEdit ? (
          <Box className="space-y-6">
            {["1", "2"].map((item, idx) => (
              <InfoParent index={idx} key={idx} />
            ))}
          </Box>
        ) : (
          <InfoParent />
        )} */}
        <InfoParent />
      </Box>
    </>
  );
};

export default InfoStudent;
