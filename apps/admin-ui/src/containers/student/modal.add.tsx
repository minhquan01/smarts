import { ConstUseForm } from "@admin-ui/components/core-form/form.context";
import ModalCustom from "@admin-ui/components/modal/modal.custom";
import RenderInput from "@admin-ui/components/render-input";
import { IModalProps } from "@admin-ui/types";
import { Box, Grid, Title } from "@mantine/core";
import React, { useEffect } from "react";
import InfoStudent from "./info.student";
import { useCreate } from "@hola/ui-core";

interface IModalAddStudentProps extends IModalProps {}

const ModalAddStudent = (props: IModalAddStudentProps) => {
  const { close, opened, refetch } = props;
  const form = ConstUseForm({
    initialValues: {},
  });

  const [create, { isLoading }] = useCreate(
    "students",
    {},
    {
      onSuccess(data) {
        refetch();
      },
      onError(err) {
        console.log({ err });
      },
    }
  );

  const handleClose = () => {
    close();
    form.reset();
  };
  const handleSubmit = async (values: any) => {
    console.log({ values });
    const dataAdd = {
      code: values.code,
      classrooms: {
        id: values.class,
      },
      birthplace: values.birthplace,
      address: values.address,
      birthday: new Date(
        String(values.monthBirth) +
          "/" +
          String(values.dayBirth) +
          "/" +
          String(values.yearBirth)
      )?.toISOString(),
      gender: values.gender,
      name: values.name,
      ethnicity: values.ethnicity,
      note: values.note,
      religion: values.religion,
      status: values.status,
      parents: {
        address: values.addressParents,
        email: values.emailParents,
        name: values.nameParents,
        note: values.notePatents,
        phone: values.phoneNumberParents,
        relation: values.relationship,
      },
    };
    create("students", {
      data: dataAdd,
    });
    handleClose();
    refetch();
  };
  useEffect(() => {
    form.setFieldValue("code", String(new Date().getTime()));
  }, [opened]);

  return (
    <ModalCustom
      title="Thêm mới học sinh"
      handleSubmit={handleSubmit}
      opened={opened}
      onClose={handleClose}
      form={form}
      size="1128px"
    >
      <InfoStudent form={form} />
    </ModalCustom>
  );
};

export default ModalAddStudent;
