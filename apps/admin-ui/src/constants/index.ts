import { SelectItem } from "@mantine/core";

export const defaultSelectClass: SelectItem[] = [
  { value: "10", label: "Lớp 10" },
  { value: "11", label: "Lớp 11" },
  { value: "12", label: "Lớp 12" },
];

export const defaultSelectSchoolLevel: SelectItem[] = [
  { value: "1", label: "Cấp 1" },
  { value: "2", label: "Cấp 2" },
  { value: "3", label: "Cấp 3" },
];

export const defaultSelectStatusStudent: SelectItem[] = [
  { value: "STUDY", label: "Đang học" },
  { value: "STOP_STUDY", label: "Nghỉ học" },
  { value: "PRESERVE", label: "Bảo lưu" },
];

export const defaultGender: SelectItem[] = [
  { value: "MALE", label: "Nam" },
  { value: "FEMALE", label: "Nữ" },
  { value: "OTHER", label: "Khác" },
];

export const defaultSelectRelation: SelectItem[] = [
  { value: "FATHER", label: "Bố" },
  { value: "MOTHER", label: "Mẹ" },
  { value: "GRANDFATHER", label: "Ông" },
  { value: "GRANDMOTHER", label: "Bà" },
  { value: "BROTHERS", label: "Anh" },
  { value: "SISTERS", label: "Chị" },
  { value: "OTHER", label: "Khác" },
];
