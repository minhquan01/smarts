import { defaultGender, defaultSelectStatusStudent } from "@admin-ui/constants";
import { ethnicity } from "@admin-ui/constants/ethnicity";
import { religions } from "@admin-ui/constants/religion";

export const utils = {};

export const renderEthnicity = (id: string) => {
  return ethnicity.find((item) => item.value === id)?.label;
};

export const renderReligion = (id: string) => {
  return religions.find((item) => item.value === id)?.label;
};

export const renderGender = (value: string) => {
  return defaultGender.find((item) => item.value === value)?.label;
};

export const renderStatusStudy = (value: string) => {
  return defaultSelectStatusStudent.find((item) => item.value === value)?.label;
};
