export const type = {};

export type IHeadCell<T> = {
  [key in keyof T]?: string;
} & {
  id?: string;
  actions?: string;
  select?: string;
  [key: string]: unknown;
};

export interface IModalProps {
  opened: boolean;
  close: () => void;
  refetch?: any;
}

// Enum Types
enum EnumSchoolTypeSchool {
  Option,
}

enum EnumSemesterStatus {
  Option1,
  Option_2,
}

enum EnumStudentGender {
  Option_1,
}

export enum EnumStudentStatus {
  STUDY = "STUDY",
  STOP_STUDY = "STOP_STUDY",
  PRESERVE = "PRESERVE",
}

enum EnumClassroomStatus {
  Option1,
}

enum EnumSubjectStatus {
  Option1,
}

enum EnumTeacherGender {
  Option_1,
}

enum EnumTeacherTypeWork {
  Option_1,
  Option_2,
}

enum EnumParentRelation {
  FATHER,
  MOTHER,
  GRANDFATHER,
  GRANDMOTHER,
  BROTHERS,
  SISTERS,
  OTHER,
}

enum EnumGradeStatus {
  Option1,
}

enum EnumSchoolYearStatus {
  Option1,
}

enum EnumNotifyStatus {
  Option1,
}

enum EnumTeachingInfoStatus {
  Option1,
}

enum EnumTimeTableStatus {
  Option1,
}

enum EnumLessonStatus {
  Option1,
}

enum EnumAttendanceStatus {
  Option1,
}

enum EnumDepartmentStatus {
  Option1,
}

enum EnumSpecialLessonStatus {
  Option1,
}

// export Interface ITypPropses
export interface IUserProps {
  createdAt: Date;
  deletedAt?: Date | null;
  firstName?: string | null;
  id: string;
  lastName?: string | null;
  password: string;
  roles: string[];
  tenant?: string | null;
  updatedAt: Date;
  username: string;
}

export interface ISchoolProps {
  address?: string | null;
  avatar?: string | null;
  coverImage?: string | null;
  level?: string | null;
  type?: string | null;
  typeSchool?: EnumSchoolTypeSchool | null;
  email?: string | null;
  phone?: string | null;
  standard?: string | null;
  yearReview?: string | null;
  deputy?: string | null;
  deputyEmail?: string | null;
  deputyPhone?: string | null;
  deputyPosition?: string | null;
  city?: string | null;
  district?: string | null;
  ward?: string | null;
  createdAt: Date;
  deletedAt?: Date | null;
  id: string;
  tenant?: string | null;
  updatedAt: Date;
}

export interface ISemesterProps {
  code?: string | null;
  createdAt: Date;
  deletedAt?: string | null;
  id: string;
  name?: string | null;
  note?: string | null;
  status?: EnumSemesterStatus | null;
  tenant?: string | null;
  updatedAt: Date;
}

export interface IStudentProps {
  address: string;
  avatar: string;
  birthday: Date;
  classrooms: IClassroomProps;
  classroomsId: string;
  code: string;
  createdAt: Date;
  deletedAt: string;
  birthplace: string;
  ethnicity: string;
  gender: EnumStudentGender;
  id: string;
  name: string;
  note: string;
  studentId: number;
  parents: IParentProps[];
  religion: string;
  status: EnumStudentStatus;
  tenant: string;
  updatedAt: Date;
}

export interface IClassroomProps {
  classInfoTimetables: IClassInfoTimetableProps[];
  createdAt: Date;
  deletedAt?: Date | null;
  grades?: IGradeProps | null;
  gradesId?: string | null;
  id: string;
  name?: string | null;
  order?: number | null;
  schoolYears?: ISchoolYearProps | null;
  schoolYearsId?: string | null;
  status?: boolean | null;
  student: IStudentProps[];
  studentIds?: string | null;
  teacher?: ITeacherProps | null;
  teacherId?: string | null;
  teachingInfo: ITeachingInfoProps[];
  tenant?: string | null;
  timeTables?: ITimeTableProps | null;
  timeTablesId?: string | null;
  updatedAt: Date;
  specialize?: string | null;
}

export interface ISubjectProps {
  classInfoTimetables: IClassInfoTimetableProps[];
  createdAt: Date;
  curricula: ICurriculumProps[];
  deletedAt?: Date | null;
  id: string;
  name?: string | null;
  note?: string | null;
  specialLessons: ISpecialLessonProps[];
  status?: EnumSubjectStatus | null;
  subjectInfoTimetables: ISubjectInfoTimetableProps[];
  teachers?: string | null;
  teachingInfo: ITeachingInfoProps[];
  tenant?: string | null;
  updatedAt: Date;
}

export interface ITeacherProps {
  address?: string | null;
  birthday?: Date | null;
  classInfoTimetables: IClassInfoTimetableProps[];
  code?: string | null;
  createdAt: Date;
  deletedAt?: Date | null;
  departments: IDepartmentProps[];
  email?: string | null;
  gender?: EnumTeacherGender | null;
  id: string;
  level?: string | null;
  manage?: string | null;
  name: string;
  note?: string | null;
  phone?: string | null;
  teachingInfos: ITeachingInfoProps[];
  tenant?: string | null;
  typeWork?: EnumTeacherTypeWork | null;
  updatedAt: Date;
  classroom: IClassroomProps[];
}

export interface IParentProps {
  address?: string | null;
  createdAt: Date;
  deletedAt?: Date | null;
  email?: string | null;
  id: string;
  name?: string | null;
  note?: string | null;
  phone?: string | null;
  relation?: EnumParentRelation | null;
  student: IStudentProps[];
  updatedAt: Date;
}

export interface IGradeProps {
  classroom: IClassroomProps[];
  code?: string | null;
  createdAt: Date;
  curriculum: ICurriculumProps[];
  deletedAt?: Date | null;
  id: string;
  name?: string | null;
  note?: string | null;
  status?: EnumGradeStatus | null;
  tenant?: string | null;
  updatedAt: Date;
}

export interface ISchoolYearProps {
  classroom: IClassroomProps[];
  createdAt: Date;
  deletedAt?: Date | null;
  description?: string | null;
  id: string;
  name?: string | null;
  status?: EnumSchoolYearStatus | null;
  tenant?: string | null;
  updatedAt: Date;
}

export interface INotifyProps {
  createdAt: Date;
  deletedAt?: Date | null;
  description?: string | null;
  file?: string | null;
  id: string;
  status?: EnumNotifyStatus | null;
  tenant?: string | null;
  title?: string | null;
  updatedAt: Date;
}

export interface ITeachingInfoProps {
  classrooms?: IClassroomProps | null;
  classroomsId?: string | null;
  createdAt: Date;
  deletedAt?: Date | null;
  id: string;
  lessons?: ILessonProps | null;
  lessonsId?: string | null;
  note?: string | null;
  status?: EnumTeachingInfoStatus | null;
  subjects?: ISubjectProps | null;
  subjectsId?: string | null;
  teacher?: ITeacherProps | null;
  teacherId?: string | null;
  timeTable?: ITimeTableProps | null;
  timeTableId?: string | null;
  updatedAt: Date;
}

export interface ITimeTableProps {
  classInfoTimetables: IClassInfoTimetableProps[];
  classroom?: IClassroomProps | null;
  createdAt: Date;
  deletedAt?: Date | null;
  expiresAt?: Date | null;
  id: string;
  name?: string | null;
  note?: string | null;
  startedAt?: Date | null;
  status?: EnumTimeTableStatus | null;
  subjectInfoTimetables: ISubjectInfoTimetableProps[];
  teachingInfos: ITeachingInfoProps[];
  tenant?: string | null;
  updatedAt: Date;
}

export interface ILessonProps {
  attendances: IAttendanceProps[];
  createdAt: Date;
  deletedAt?: Date | null;
  id: string;
  name?: string | null;
  note?: string | null;
  specialLesson: ISpecialLessonProps[];
  status?: EnumLessonStatus | null;
  teachingInfo: ITeachingInfoProps[];
  tenant?: string | null;
  timeFrom?: Date | null;
  timeTo?: Date | null;
  updatedAt: Date;
}

export interface IAttendanceProps {
  createdAt: Date;
  deletedAt?: Date | null;
  id: string;
  lesson?: ILessonProps | null;
  lessonId?: string | null;
  note?: string | null;
  status?: EnumAttendanceStatus | null;
  student?: IStudentProps | null;
  studentId?: string | null;
  tenant?: string | null;
}

export interface IStudyTimeProps {
  afternoon?: string | null;
  createdAt: Date;
  deletedAt?: Date | null;
  id: string;
  morning?: string | null;
  tenant?: string | null;
  timeLesson?: string | null;
  updatedAt: Date;
}

export interface IDepartmentProps {
  code?: string | null;
  createdAt: Date;
  deletedAt?: Date | null;
  description?: string | null;
  id: string;
  name?: string | null;
  status?: EnumDepartmentStatus | null;
  teacher: ITeacherProps[];
  tenant?: string | null;
  updatedAt: Date;
}

export interface ISpecialLessonProps {
  createdAt: Date;
  deletedAt?: Date | null;
  id: string;
  lessons?: ILessonProps | null;
  lessonsId?: string | null;
  status?: EnumSpecialLessonStatus | null;
  subject?: ISubjectProps | null;
  subjectId?: string | null;
  tenant?: string | null;
  updatedAt: Date;
}

export interface ISubjectInfoTimetableProps {
  createdAt: Date;
  deletedAt?: Date | null;
  id: string;
  limit?: number | null;
  numberAdjacentLesson?: string | null;
  subject?: ISubjectProps | null;
  subjectId?: string | null;
  tenant?: string | null;
  timeTable?: ITimeTableProps | null;
  timeTableId?: string | null;
  updatedAt: Date;
}

export interface ICurriculumProps {
  createdAt: Date;
  deletedAt?: Date | null;
  grade?: IGradeProps | null;
  gradeId?: string | null;
  id: string;
  numberAdjacentLesson?: number | null;
  numberLesson?: number | null;
  subject?: ISubjectProps | null;
  subjectId?: string | null;
  tenant?: string | null;
  updatedAt: Date;
}

export interface IClassInfoTimetableProps {
  classRoom?: IClassroomProps | null;
  classRoomId?: string | null;
  createdAt: Date;
  deletedAt?: Date | null;
  id: string;
  numberLesson?: number | null;
  subject?: ISubjectProps | null;
  subjectId?: string | null;
  teacher?: ITeacherProps | null;
  teacherId?: string | null;
  tenant?: string | null;
  timeTable?: ITimeTableProps | null;
  timeTableId?: string | null;
  updatedAt: Date;
}
