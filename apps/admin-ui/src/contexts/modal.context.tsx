import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useState,
} from 'react';

interface IModalProps {
  children: ReactNode;
}
interface IModalConfigs {
  modal: 'confirm' | 'actions';
  modalBody: unknown;
}

type ModalsContextValue = {
  open: (config: IModalConfigs) => void;
  close: () => void;
};
const ModalsContext = createContext<ModalsContextValue | null>(null);
if (process.env.NODE_ENV === 'development') {
  ModalsContext.displayName = 'ModalsContext';
}

const ModalsProvider = ({ children }: IModalProps) => {
  const [modalConfig, setModalConfig] = useState<IModalConfigs | null>(null);
  const handleOpenModal = useCallback((config: IModalConfigs) => {
    setModalConfig(config);
  }, []);
  const handleCloseModal = useCallback(() => {
    setModalConfig(null);
  }, []);

  return (
    <ModalsContext.Provider
      value={{ open: handleOpenModal, close: handleCloseModal }}
    >
      {children}
    </ModalsContext.Provider>
  );
};

const useModals = () => {
  const modalsContext = useContext(ModalsContext);
  if (!modalsContext) {
    throw new Error('Forgot to warp component in modals context.');
  }

  return useModals;
};
export { ModalsProvider as default, useModals };
