import { Box, Text } from "@mantine/core";

import { useGetList } from "@hola/ui-core";

export default function Home() {
  const data = useGetList("users");

  return (
    <>
      <Box>
        <Text>Ahi hi test</Text>

        {/* <Resource name="post" list={ListPost} />
        <Resource name="comment" list={ListComment} /> */}
      </Box>
    </>
  );
}

// const ListPost = () => {
//   const { name } = useResource();
//   const { columns } = useTablePost();
//   // const [columnPinning, setColumnPinning] = useState<Record<string, boolean>>({
//   //   code: true,
//   // });

//   return (
//     <Box>
//       <Text>List {name} component</Text>
//       {/* <CoreTable
//         // @ts-ignore
//         columns={columns}
//         data={defaultData}
//         loading={false}
//         bodyHeight={500}
//         // columnPinning={columnPinning}
//         // setColumnPinning={setColumnPinning}
//       /> */}
//     </Box>
//   );
// };
// const ListComment = () => {
//   const { name } = useResource();
//   return <Box>List {name} component</Box>;
// };

// interface IResourceProps {
//   name: string;
//   list?: ComponentType | ReactElement;
//   create?: ReactElement;
//   edit?: ReactElement;
//   children?: ReactNode;
// }
// const Resource = (props: IResourceProps) => {
//   const { name, list, create, edit, children } = props;

//   return (
//     <ResourceProvider value={name}>
//       resource context provider to query
//       {list && getElement(list)}
//     </ResourceProvider>
//   );
// };
// const ResourceContext = createContext<{ name: string } | null>(null);
// const ResourceProvider = ({
//   children,
//   value,
// }: {
//   children: ReactNode;
//   value: string;
// }) => {
//   return (
//     <ResourceContext.Provider value={{ name: value }}>
//       {children}
//     </ResourceContext.Provider>
//   );
// };
// const useResource = () => {
//   const resource = useContext(ResourceContext);
//   if (!resource)
//     throw new Error("Forgot to warp component in resource context");
//   return resource;
// };

// type ComponentType<P = {}> = ComponentClass<P> | FunctionComponent<P>;
// const getElement = (Element: ReactElement | ComponentType) => {
//   if (isValidElement(Element)) return Element;
//   if (isValidElementType(Element)) return <Element />;
//   return null;
// };

// interface IPost {
//   code: string;
//   name: string;
//   views: number;
// }
// const HEAD_CELLS: IHeadCell<IPost> = {
//   code: "STT",
//   name: "Name",
//   views: "Views",
// };
// const columHelper = createColumnHelper<IPost>();
// const useTablePost = () => {
//   const columns = useMemo(
//     () => [
//       columHelper.accessor("code", {
//         cell: (context) => <Text>{context.getValue()}</Text>,
//         header: () => HEAD_CELLS.code,
//       }),
//       columHelper.accessor("name", {
//         cell: (context) => <Text>{context.getValue()}</Text>,
//         header: () => HEAD_CELLS.name,
//       }),
//       columHelper.accessor("views", {
//         cell: (context) => <Text>{context.getValue()}</Text>,
//         header: () => HEAD_CELLS.views,
//       }),
//     ],
//     []
//   );

//   return { columns };
// };
// const defaultData: IPost[] = [
//   {
//     code: "hola0001",
//     name: "post one",
//     views: 12,
//   },
// ];
