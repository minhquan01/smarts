import ButtonCustom from "@admin-ui/components/button/button.custom";
import {
  ConstFormProvider,
  ConstUseForm,
} from "@admin-ui/components/core-form/form.context";
import RenderInput from "@admin-ui/components/render-input";
import { defaultSelectSchoolLevel } from "@admin-ui/constants";
import { useCreate, useGetList, useUpdate } from "@hola/ui-core";
import { Box, Button, Grid } from "@mantine/core";
import { isEmpty } from "lodash";
import React, { useEffect } from "react";

const fieldsSchools = [
  {
    type: "text",
    span: 3,
    label: "Tên trường học",
    name: "nameSchool",
    require: true,
  },
  {
    type: "select",
    span: 3,
    label: "Cấp học",
    name: "schoolLevel",
    require: true,
    data: defaultSelectSchoolLevel,
  },
  {
    type: "select",
    span: 3,
    label: "Loại hình",
    name: "type",
    require: true,
    data: defaultSelectSchoolLevel,
  },
  {
    type: "select",
    span: 3,
    label: "Loại trường",
    name: "typeSchool",
    require: true,
    data: defaultSelectSchoolLevel,
  },
  {
    type: "select",
    span: 3,
    label: "Tỉnh/ thành phố",
    name: "province",
    data: defaultSelectSchoolLevel,
  },
  {
    type: "select",
    span: 3,
    label: "Quận/ huyện",
    name: "district",
    data: defaultSelectSchoolLevel,
  },
  {
    type: "select",
    span: 3,
    label: "Phường/ xã",
    name: "ward",
    data: defaultSelectSchoolLevel,
  },
  {
    type: "text",
    span: 3,
    label: "Địa chỉ chi tiết",
    name: "address",
  },
  {
    type: "text",
    span: 3,
    label: "Số điện thoại",
    name: "phoneNumberSchool",
  },
  {
    type: "text",
    span: 3,
    label: "Email",
    name: "email",
  },
  {
    type: "select",
    span: 3,
    label: "Trường chuẩn",
    name: "standard",
    data: defaultSelectSchoolLevel,
  },
  {
    type: "select",
    span: 3,
    label: "Năm xét",
    name: "yearReview",
    data: defaultSelectSchoolLevel,
  },
  {
    type: "text",
    span: 3,
    label: "Người đại diện",
    name: "deputy",
  },
  {
    type: "select",
    span: 3,
    label: "Chức vụ",
    name: "positionDeputy",
    data: defaultSelectSchoolLevel,
  },
  {
    type: "text",
    span: 3,
    label: "Số điện thoại",
    name: "phoneDeputy",
  },
  {
    type: "text",
    span: 3,
    label: "Email",
    name: "emailDeputy",
  },
];

const InfoSchool = () => {
  const form = ConstUseForm({
    initialValues: {},
  });
  const { data, refetch } = useGetList("schools");

  console.log({ data });

  useEffect(() => {
    if (!data) {
      return;
    }
    const dataFind = data[0];
    console.log({ dataFind });
    form.setValues({
      address: dataFind?.address,
      avatar: dataFind?.avatar,
      province: dataFind?.city,
      deputy: dataFind?.deputy,
      emailDeputy: dataFind?.deputyEmail,
      phoneDeputy: dataFind?.deputyPhone,
      district: dataFind?.district,
      email: dataFind?.email,
      phoneNumberSchool: dataFind?.phone,
      standard: dataFind?.standard,
      typeSchool: dataFind?.typeSchool,
      ward: dataFind?.ward,
      positionDeputy: dataFind?.deputyPosition,
      type: dataFind?.type,
      cover: dataFind?.coverImage,
      schoolLevel: dataFind?.level,
      yearReview: dataFind?.yearReview,
      nameSchool: dataFind?.name,
    });
  }, [data]);

  console.log({ dataFind2: form.values });

  const [create, { isLoading }] = useCreate(
    "schools",
    {},
    {
      onSuccess(data) {
        console.log(data, "dataStu");
      },
      onError(err) {
        console.log({ err });
      },
    }
  );

  const [update] = useUpdate(
    "schools",
    {},
    {
      onSuccess(data) {
        console.log(data, "dataStu");
      },
      onError(err) {
        console.log({ err });
      },
    }
  );

  const handleClose = () => {
    close();
  };
  const handleSubmit = (values: any) => {
    const dataSchool = {
      address: values.address,
      avatar: values.avatar ?? "",
      city: values.province,
      deputy: values.deputy,
      deputyEmail: values.emailDeputy,
      deputyPhone: values.phoneDeputy,
      district: values.district,
      email: values.email,
      phone: values.phoneNumberSchool,
      standard: values.standard,
      typeSchool: "Option",
      ward: values.ward,
      deputyPosition: values.positionDeputy,
      type: values.type,
      coverImage: values.cover ?? "",
      level: values.schoolLevel,
      yearReview: values.yearReview,
      name: values.nameSchool,
    };

    console.log({ dataSchool });

    if (!isEmpty(data[0])) {
      console.log("11");
      update("schools", {
        id: data?.[0].id,
        data: dataSchool,
      });
    } else {
      create("schools", {
        data: dataSchool,
      });
    }
    refetch;
    handleClose();
  };

  return (
    <ConstFormProvider form={form}>
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Box>
          <Grid>
            <RenderInput span={2} type="upload-image" name="avatar" />
            <RenderInput
              span={10}
              typeFor="cover"
              type="upload-image"
              name="cover"
            />
          </Grid>
        </Box>
        <Box className="mt-14">
          <Grid className="gap-y-4">
            {fieldsSchools.map((_data, index) => (
              <RenderInput
                key={index}
                type={_data?.type}
                label={_data?.label}
                required={_data?.require}
                span={_data?.span}
                data={_data?.data}
                name={_data?.name}
              />
            ))}
          </Grid>
        </Box>
        <ButtonCustom
          type="submit"
          path="primary"
          className="mt-10 float-right"
        >
          {isEmpty(data?.[0]) ? "Lưu" : "Cập nhật"}
        </ButtonCustom>
      </form>
    </ConstFormProvider>
  );
};

export default InfoSchool;
