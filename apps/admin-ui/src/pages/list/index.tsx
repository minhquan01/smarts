import { IconBook, IconRoundPlus, IconStickyNote } from "@admin-ui/assets";
import { useDelete, useGetList } from "@hola/ui-core";
import { Box, Text } from "@mantine/core";
import {
  IconPencil,
  IconToggleLeft,
  IconToggleRight,
  IconTrash,
} from "@tabler/icons-react";
import { useEffect, useMemo, useState } from "react";

import ButtonCustom from "@admin-ui/components/button/button.custom";
import SelectSearch from "@admin-ui/components/core-form/select.search";
import CoreTable from "@admin-ui/components/core-table";
import ModalMessage from "@admin-ui/components/modal/ModalMessage";
import { defaultSelectClass } from "@admin-ui/constants";
import { IHeadCell } from "@admin-ui/types";
import { createColumnHelper } from "@tanstack/react-table";
import AddClass from "./AddClass";
import EditClass from "./EditClass";
import ListStudent from "./ListStudent";

function ClassList() {
  const [openModal, setOpenModal] = useState(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [openModalRemove, setOpenModalRemove] = useState(false);
  const [openModalShowSt, setOpenModalShowSt] = useState(false);
  const [deleteOne, { isLoading: isLoadingDel }] = useDelete("classrooms");
  const { data, total, isLoading, error, pageInfo, refetch } = useGetList(
    "classrooms",
    {},
    {
      onSuccess: (res) => {
        // console.log({ res });
      },
      onError: (error) => {
        // console.log({ error });
      },
    }
  );

  useEffect(() => {
    refetch();
  }, [openModal]);

  const handleSubmitRemove = (val) => {
    deleteOne(
      "classrooms",
      { id: val },
      {
        onSuccess: (res) => {
          setOnRemove(false);
          setOpenModalRemove(false);
          refetch;
        },
        onError: (err) => {},
      }
    );
  };

  const {
    columns,
    type,
    setType,
    id,
    onEdit,
    setOnEdit,
    onSwitch,
    setOnSwitch,
    onRemove,
    setOnRemove,
    dataEd,
    showStudent,
    setShowStudent,
    nameOfClass,
  } = useTablePost();

  useEffect(() => {
    if (onEdit) {
      setOpenModalEdit(true);
    }
    if (onRemove) {
      setOpenModalRemove(true);
    }
    if (showStudent) {
      setOpenModalShowSt(true);
    }
  }, [onEdit, onSwitch, onRemove, showStudent]);

  const [rowSelection, setRowSelection] = useState<Record<string, boolean>>({
    0: true,
  });

  return (
    <>
      <Box className="flex justify-between items-center w-full pb-10 gap-4">
        <Box className="flex gap-6 items-center">
          <SelectSearch
            data={defaultSelectClass}
            className="flex items-center gap-2 font-medium"
            placeholder="Khối"
            clearable
          />
          <SelectSearch
            data={defaultSelectClass}
            className="flex items-center gap-2 font-medium"
            placeholder="Giáo viên chủ nhiệm"
            clearable
          />
        </Box>
        <Box className="flex items-center gap-5">
          <ButtonCustom
            onClick={() => setOpenModal(true)}
            leftIcon={<IconRoundPlus />}
          >
            Thêm mới lớp học
          </ButtonCustom>
        </Box>
      </Box>

      <CoreTable
        columns={columns}
        data={data}
        loading={false}
        bodyHeight={500}
        rowSelection={rowSelection}
        setRowSelection={setRowSelection}
        // sorting={sorting}
        // setSorting={setSorting}
        // columnPinning={columnPinning}
        // setColumnPinning={setColumnPinning}
      />
      {openModal && (
        <AddClass
          openModal={openModal}
          closeModal={() => {
            setOpenModal(false);
            refetch();
            setType("");
          }}
          title={"Thêm mới lớp học"}
          type={type}
          id={id}
        />
      )}
      {openModalEdit && (
        <EditClass
          openModal={openModal}
          closeModal={() => {
            setOpenModalEdit(false);
            setOnEdit(false);
            refetch();
            setType("");
          }}
          title={"Chỉnh sửa lớp học"}
          type={type}
          id={id}
          refetch={refetch}
          dataEd={dataEd}
        />
      )}
      {openModalRemove && (
        <ModalMessage
          onOpen={openModalRemove}
          onCancel={() => {
            setOnRemove(false);
            setOpenModalRemove(false);
          }}
          message="Bạn có chắc chắn muốn xóa lớp học?"
          onSubmit={() => handleSubmitRemove(id)}
          loading={isLoadingDel}
        />
      )}
      {openModalShowSt && (
        <ListStudent
          onOpen={openModalShowSt}
          closeModal={() => {
            refetch();
            setShowStudent(false);
            setOpenModalShowSt(false);
          }}
          title={`Danh sách lớp học ${nameOfClass}`}
          onSubmit={() => handleSubmitRemove(id)}
          id={id}
        />
      )}
    </>
  );
}

export default ClassList;

interface IPost {
  id: string;
  index: string;
  grades: string;
  name: string;
  teacher: string;
  specialize: string;
  listStudent: string;
  time_table: string;
  action?: string;
}
const HEAD_CELLS: IHeadCell<IPost> = {
  index: "TT",
  grades: "Tên khối",
  name: "Tên lớp",
  teacher: "GVCN",
  specialize: "Môn chuyên",
  listStudent: "Học sinh",
  time_table: "Thời khoá biểu",
  action: "",
};
const columHelper = createColumnHelper<IPost>();

const useTablePost = () => {
  const [onSwitch, setOnSwitch] = useState<boolean>(false);
  const [onEdit, setOnEdit] = useState<boolean>(false);
  const [onRemove, setOnRemove] = useState<boolean>(false);
  const [type, setType] = useState<string>("");
  const [id, setId] = useState<string>("");
  const [dataEd, setDataEd] = useState({});
  const [showStudent, setShowStudent] = useState(false);
  const [nameOfClass, setNameOfClass] = useState("");

  const handleEdit = (id, data, type) => {
    console.log(dataEd, "dataEd");
    setOnEdit(!onEdit);
    setDataEd(data);
    setType(type);
    setId(id);
  };
  const handleRemove = (id, type) => {
    setOnRemove(!onRemove);
    setType(type);
    setId(id);
  };
  const handleSwitch = (id, type) => {
    setOnSwitch(!onSwitch);
    setType(type);
    setId(id);
  };
  const handleShowStudent = (id, data, type) => {
    setNameOfClass(data.name);
    setId(id);
    setType(type);
    setShowStudent(true);
  };

  const columns = useMemo(
    () => [
      columHelper.accessor("index", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.index,
        maxSize: 100,
      }),
      columHelper.accessor("grades", {
        cell: (context) => <Text>{context.getValue()?.name}</Text>,
        header: () => HEAD_CELLS.grades,
      }),
      columHelper.accessor("name", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.name,
        maxSize: 100,
      }),
      columHelper.accessor("teacher", {
        cell: (context) => <Text>{context.getValue()?.name}</Text>,
        header: () => HEAD_CELLS.teacher,
        maxSize: 300,
        minSize: 200,
      }),
      columHelper.accessor("specialize", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.specialize,
        minSize: 200,
      }),
      columHelper.accessor("listStudent", {
        cell: (context) => (
          <Box
            className="cursor-pointer flex justify-center"
            onClick={() =>
              handleShowStudent(
                context.row.original?.id,
                context.row.original,
                "show"
              )
            }
          >
            <IconBook />
          </Box>
        ),
        header: () => HEAD_CELLS.listStudent,
        minSize: 150,
      }),
      columHelper.accessor("time_table", {
        cell: (context) => (
          <Box className="cursor-pointer flex justify-center">
            <IconStickyNote />
          </Box>
        ),
        header: () => HEAD_CELLS.time_table,
        minSize: 150,
      }),
      columHelper.accessor("action", {
        cell: (context) => (
          <Box className="flex flex-row justify-center gap-4">
            {
              <Box
                className="cursor-pointer"
                onClick={() => handleSwitch(context.row.original?.id, "switch")}
              >
                {onSwitch && id == context.row.original?.id ? (
                  <IconToggleLeft />
                ) : (
                  <IconToggleRight />
                )}
              </Box>
            }
            <Box
              className="cursor-pointer"
              onClick={() =>
                handleEdit(
                  context.row.original?.id,
                  context.row.original,
                  "edit"
                )
              }
            >
              <IconPencil />
            </Box>
            <Box
              className="cursor-pointer"
              onClick={() => handleRemove(context.row.original?.id, "remove")}
            >
              <IconTrash />
            </Box>
          </Box>
        ),
        header: () => "...",
        minSize: 200,
      }),
    ],
    [onSwitch, onEdit, onRemove, type, id, showStudent]
  );

  return {
    columns,
    type,
    id,
    onEdit,
    setOnEdit,
    onSwitch,
    setOnSwitch,
    onRemove,
    setOnRemove,
    setType,
    dataEd,
    showStudent,
    setShowStudent,
    nameOfClass,
  };
};

const defaultData: IPost[] = [
  {
    id: "class__1",
    index: "1",
    grades: "Khối 11",
    name: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    specialize: "Toán",
    listStudent: "string",
    time_table: "string",
  },
  {
    id: "class__2",
    index: "2",
    grades: "Khối 11",
    name: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    specialize: "Lý",
    listStudent: "string",
    time_table: "string",
  },
  {
    id: "class__3",
    index: "3",
    grades: "Khối 11",
    name: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    specialize: "Hoá",
    listStudent: "string",
    time_table: "string",
  },
  {
    id: "class__4",
    index: "4",
    grades: "Khối 11",
    name: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    specialize: "Văn",
    listStudent: "string",
    time_table: "string",
  },
  {
    id: "class__5",
    index: "5",
    grades: "Khối 11",
    name: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    specialize: "Tiếng Anh",
    listStudent: "string",
    time_table: "string",
  },
];
