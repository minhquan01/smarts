import { IconDirectboxReceive, IconDocumentDownload } from "@admin-ui/assets";
import { useGetList, useUpdate } from "@hola/ui-core";
import { Box, Grid, Text } from "@mantine/core";
import { useEffect, useState } from "react";

import ModalForm from "@admin-ui/components/modal";
import ModalMessage from "@admin-ui/components/modal/ModalMessage";
import RenderInput from "@admin-ui/components/render-input";

interface IClassType {
  openModal: boolean;
  closeModal: () => void;
  title: string;
  type: string;
  id: string;
  refetch?: () => void;
  dataEd: any;
}

const fields = [
  {
    type: "select",
    span: 4,
    label: "Năm học",
    name: "schoolYears",
    require: true,
    defaultValue: "2022-2023",
    data: ["2022-2023", "2023-2024"],
  },
  {
    type: "text",
    span: 4,
    label: "Tên lớp học",
    name: "name",
    require: true,
    defaultValue: "",
  },
  {
    type: "select",
    span: 4,
    label: "Khối",
    name: "grades",
    require: true,
    defaultValue: "10",
    data: [
      {
        value: "1f52901f-9918-410e-97d7-ba7b2dac9a84",
        label: "string",
      },
    ],
  },
  {
    type: "select",
    span: 4,
    label: "GVCN",
    name: "teacher",
    require: true,
    defaultValue: "a6a50755-43ad-4ede-bcb6-5b67cbdcd024 ",
    data: [
      {
        value: "2b6f5ad7-7432-4837-8fbb-d1d58b7bd550",
        label: "Le Manh Truong",
      },
      {
        value: "ab5197ff-dd30-448d-bdd5-5668eb83f1ef",
        label: "Tran Thi Ngoc Anh",
      },
      {
        value: "a6a50755-43ad-4ede-bcb6-5b67cbdcd024",
        label: "Le Tran Hai Bang",
      },
      {
        value: "61bd7aaa-340a-4c38-95f7-0794bb4fe813",
        label: "Ton Ngo Khong",
      },
    ],
  },
  {
    type: "select",
    span: 4,
    label: "Môn chuyên",
    name: "specialize",
    require: false,
    defaultValue: "Toán",
    data: ["Toán", "Lý", "Hoá", "Tiếng Anh", "Văn"],
  },
  {
    type: "text",
    span: 4,
    label: "STT hiển thị",
    name: "index",
    require: false,
    defaultValue: "",
  },

  {
    type: "select",
    span: 4,
    label: "Số ngày/tuần",
    name: "dayperweek",
    require: false,
    defaultValue: "6",
    data: ["4", "5", "6"],
  },
  {
    type: "select",
    span: 4,
    label: "Số buổi/ngày",
    name: "numberoftimesperday",
    require: false,
    defaultValue: "2",
    data: ["1", "2"],
  },
];

function EditClass({
  openModal,
  closeModal,
  title,
  type,
  id,
  dataEd,
}: IClassType) {
  const [errs, setErrs] = useState([]);
  const [values, setValues] = useState([]);
  const [responseMessage, setResponseMessage] = useState("");
  const [created, setCreated] = useState(null);

  const [update, { data, isLoading }] = useUpdate(
    "classrooms",
    {},
    {
      onSuccess(data) {
        console.log(data, "success");
        setCreated(true);
        closeModal();
      },
      onError(err) {
        console.log({ err });
        setResponseMessage(err?.message);
        setCreated(false);
      },
    }
  );
  const { data: dataTeacher, isLoading: isLoadingTeacher } = useGetList(
    "teachers",
    {}
  );
  const { data: dataGrade, isLoading: isLoadingGrade } = useGetList(
    "grades",
    {}
  );
  const { data: dataSchoolYear, isLoading: isLoadingSchoolYear } = useGetList(
    "schoolYears",
    {}
  );
  const [listTeacher, setListTeacher] = useState([]);
  const [listGrade, setListGrade] = useState([]);
  const [listSchoolYear, setListSchoolYear] = useState([]);

  useEffect(() => {
    if (dataTeacher?.length > 0) {
      const arr = dataTeacher.map((item) => {
        return { value: item.id, label: item.name };
      });
      setListTeacher(arr);
    }
    if (dataGrade?.length > 0) {
      const arr = dataGrade.map((item) => {
        return { value: item.id, label: item.name };
      });
      setListGrade(arr);
    }
    if (dataSchoolYear?.length > 0) {
      const arr = dataSchoolYear.map((item) => {
        return { value: item.id, label: item.name };
      });
      setListSchoolYear(arr);
    }
  }, [dataTeacher, dataGrade, dataSchoolYear]);
  console.log(listSchoolYear, "listSchoolYear");

  const handleSubmit = (val) => {
    const arr = fields.map((item) => {
      if (!val[item.name]) return item.name;
    });
    console.log(val, "vallll");
    setValues(val);
    setErrs(arr);
    let countErrs = 0;
    fields.map((data) => {
      data.require && arr.includes(data.name) ? (countErrs += 1) : countErrs;
    });
    if (!countErrs) {
      update("classrooms", {
        id: id,
        data: {
          grades: {
            id: val?.grades,
          },
          name: val?.name,
          order: 0,
          status: false,
          teacher: { id: val?.teacher },
          specialize: val?.specialize,
          schoolYears: { id: val?.schoolYears },
        },
        previousData: dataEd,
      });
    }
  };
  return (
    <>
      <ModalForm
        openModal
        closeModal={closeModal}
        title={title}
        type={type}
        id={id}
        handleSubmit={(val) => handleSubmit(val)}
        resetForm={false}
        loading={isLoading}
        datas={{
          ...dataEd,
          teacher: dataEd.teacher?.id,
          grades: dataEd.grades?.id,
          schoolYears: dataEd.schoolYears?.id,
          dayperweek: 6,
          numberoftimesperday: 2,
        }}
      >
        <Grid>
          {fields.map((_data, index) => (
            <RenderInput
              key={index}
              type={_data.type}
              label={_data.label}
              required={_data.require}
              value={dataEd[_data.name]}
              span={_data.span}
              // data={_data.name == "teacher" ? listTeacher : _data.data}
              data={
                _data.name == "teacher"
                  ? listTeacher
                  : _data.name == "grades"
                  ? listGrade
                  : _data.name == "schoolYears"
                  ? listSchoolYear
                  : _data.data
              }
              name={_data?.name}
              error={
                _data.require && errs.includes(_data.name)
                  ? "Không được trống trường này"
                  : ""
              }
              placeholder={""}
            />
          ))}
        </Grid>
        <Box className="flex items-center gap-4 pt-8">
          <Box className="flex flex-row gap-4 items-center">
            <IconDocumentDownload />
            <Text className="text-[16px] font-semibold">Tải xuống mẫu</Text>
          </Box>
          <Box className="flex flex-row gap-4 items-center">
            <IconDirectboxReceive />
            <Text className="text-[16px] font-semibold">Upload danh sách</Text>
          </Box>
        </Box>
      </ModalForm>
      <ModalMessage
        onOpen={!!responseMessage}
        onCancel={() => setResponseMessage("")}
        message={responseMessage}
        onSubmit={() => setResponseMessage("")}
        type="noti"
      />
    </>
  );
}

export default EditClass;
