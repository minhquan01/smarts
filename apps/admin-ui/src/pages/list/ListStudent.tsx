import { IHeadCell, IStudentProps } from "@admin-ui/types";
import { Box, Text } from "@mantine/core";
import { useEffect, useMemo, useState } from "react";

import CoreTable from "@admin-ui/components/core-table";
import ModalForm from "@admin-ui/components/modal";
import { convertDateToString } from "@admin-ui/constants/convert";
import { useGetList } from "@hola/ui-core";
import { createColumnHelper } from "@tanstack/react-table";

const HEAD_CELLS: IHeadCell<IStudentProps> = {
  code: "Mã học sinh",
  name: "Họ và tên",
  gender: "Giới tính",
  birthday: "Ngày sinh",
  class: "Lớp",
  birthplace: "Nơi sinh",
  address: "Địa chỉ hiện tại",
  ethnicity: "Dân tộc",
  religion: "Tôn giáo",
  status: "Tình trạng",
  action: "...",
};

interface IClassType {
  openModal: boolean;
  closeModal: () => void;
  title: string;
  type: string;
  id: string;
  refetch?: () => void;
}

function ListStudent({ openModal, closeModal, title, type, id }: IClassType) {
  const columnHelper = createColumnHelper<IStudentProps>();

  const {
    data: dataStudent,
    refetch,
    isLoading,
  } = useGetList(`classrooms/${id}/student`);

  const [rowSelection, setRowSelection] = useState<Record<string, boolean>>({
    0: true,
  });
  const handleSubmit = () => {};
  useEffect(() => {
    refetch();
  }, [openModal]);

  const columns = useMemo(() => {
    return [
      // @ts-ignore
      columnHelper.accessor("code", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.code,
      }),
      columnHelper.accessor("name", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.name,
      }),
      columnHelper.accessor("gender", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.gender,
      }),
      columnHelper.accessor("birthday", {
        cell: (context) => (
          <Text>{convertDateToString(context.getValue())}</Text>
        ),
        header: () => HEAD_CELLS.birthday,
      }),
      columnHelper.accessor("birthplace", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.birthplace,
      }),
      columnHelper.accessor("address", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.address,
      }),
      columnHelper.accessor("ethnicity", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.ethnicity,
      }),
      columnHelper.accessor("religion", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.religion,
      }),
      columnHelper.accessor("status", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.status,
      }),
    ];
  }, []);
  return (
    <ModalForm
      openModal
      closeModal={closeModal}
      title={title}
      type={type}
      id={id}
      handleSubmit={(val) => handleSubmit(val)}
      hideBtn={true}
      size="100%"
    >
      {isLoading ? (
        <Box className="flex justify-center items-center">loading..</Box>
      ) : (
        <CoreTable<IStudentProps>
          columns={columns}
          data={dataStudent}
          loading={false}
          bodyHeight={500}
          rowSelection={rowSelection}
          setRowSelection={setRowSelection}
        />
      )}
    </ModalForm>
  );
}

export default ListStudent;
