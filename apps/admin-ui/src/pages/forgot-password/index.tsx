import {
  ConstFormProvider,
  ConstUseForm,
} from "@admin-ui/components/core-form/form.context";
import ComfirmCode from "@admin-ui/containers/forgot-password/comfirm.code";
import ComfirmEmail from "@admin-ui/containers/forgot-password/comfirm.email";
import CreatePassword from "@admin-ui/containers/forgot-password/create.password";
import ResetPassword from "@admin-ui/containers/login/reset.password";
import SignIn from "@admin-ui/containers/login/signin";
import { Tabs } from "@mantine/core";
import { useState } from "react";

const tabs = ["comfirm-email", "comfirm-code", "create"];
const ForgotPassword = () => {
  const [activeTab, setActiveTab] = useState<string>(tabs[0]);
  const form = ConstUseForm({
    initialValues: {},
  });

  console.log({ activeTab });

  const handleNextTab = () => {
    setActiveTab(tabs[tabs.indexOf(activeTab) + 1]);
  };

  const handleSubmit = (values) => {
    if (activeTab === "comfirm-email" || activeTab === "comfirm-code") {
      handleNextTab();
    }
  };
  return (
    <ConstFormProvider form={form}>
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Tabs value={activeTab}>
          <Tabs.Panel value="comfirm-email">
            <ComfirmEmail />
          </Tabs.Panel>
          <Tabs.Panel value="comfirm-code">
            <ComfirmCode />
          </Tabs.Panel>
          <Tabs.Panel value="create">
            <CreatePassword />
          </Tabs.Panel>
        </Tabs>
      </form>
    </ConstFormProvider>
  );
};

export default ForgotPassword;
