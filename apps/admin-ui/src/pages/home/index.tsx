import { Box, Checkbox, Text } from "@mantine/core";
import { SortingState, createColumnHelper } from "@tanstack/react-table";
import { useMemo, useState } from "react";

import { IconArrowDown } from "@admin-ui/assets";
import CoreTable from "@admin-ui/components/core-table";
import OverviewTag from "@admin-ui/components/overview-tag";
import { IHeadCell } from "@admin-ui/types";

const Test = () => {
  const { columns } = useTablePost();
  const [rowSelection, setRowSelection] = useState<Record<string, boolean>>({
    0: true,
  });
  const [sorting, setSorting] = useState<SortingState>([]);

  return (
    <div>
      <Box className="flex items-center justify-between pb-10 gap-6">
        <OverviewTag type="student" total="1000" content="Tổng số học sinh" />
        <OverviewTag type="teacher" total="100" content="Tổng số giáo viên" />
        <OverviewTag type="class" total="50" content="Tổng số lớp học" />
      </Box>
      <Box className="flex items-center justify-between pb-4">
        <Text className="text-xl font-bold">Điểm danh chuyên cần</Text>
        <Box className="p-2 rounded-lg flex items-center gap-4 bg-white">
          <p className="text-sm font-semibold text-textPrimary">23/01/2023</p>
          <Box className="scale-90">
            <IconArrowDown />
          </Box>
        </Box>
      </Box>
      <CoreTable
        columns={columns}
        data={defaultData}
        loading={false}
        bodyHeight={500}
        rowSelection={rowSelection}
        setRowSelection={setRowSelection}
        sorting={sorting}
        setSorting={setSorting}
        // columnPinning={columnPinning}
        // setColumnPinning={setColumnPinning}
      />
    </div>
  );
};

export default Test;

interface IPost {
  grade: string;
  class: string;
  teacher: string;
  totalStudent: string;
  dayOff_Unaccepted: string;
  dayOff_Accepted: string;
  late: string;
  action: string;
  detail: string;
}
const HEAD_CELLS: IHeadCell<IPost> = {
  grade: "Khối lớp",
  class: "Lớp",
  teacher: "GVCN",
  totalStudent: "Tổng số học sinh",
  dayOff_Unaccepted: "Nghỉ không phép",
  dayOff_Accepted: "Nghỉ có phép",
  late: "Đi muộn",
  detail: "Chi tiết",
  action: "Hành động",
};
const columHelper = createColumnHelper<IPost>();

const useTablePost = () => {
  const [sorting, setSorting] = useState<SortingState>([]);
  const [selectOption, setSelectOption] = useState("");
  const columns = useMemo(
    () => [
      {
        id: "select",
        cell: ({ row }) => (
          <Checkbox
            {...{
              checked: row.getIsSelected(),
              disabled: !row.getCanSelect(),
              indeterminate: row.getIsSomeSelected(),
              onChange: row.getToggleSelectedHandler(),
              style: {
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              },
            }}
          />
        ),

        header: () => "Checkbox",
        maxSize: 100,
      },

      columHelper.accessor("grade", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.grade,
      }),
      columHelper.accessor("class", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.class,
        maxSize: 100,
      }),
      columHelper.accessor("teacher", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.teacher,
        maxSize: 300,
        minSize: 200,
      }),
      columHelper.accessor("totalStudent", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.totalStudent,
        minSize: 200,
      }),
      columHelper.accessor("dayOff_Unaccepted", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.dayOff_Unaccepted,
        minSize: 200,
      }),
      columHelper.accessor("dayOff_Accepted", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.dayOff_Accepted,
        minSize: 200,
      }),
      columHelper.accessor("late", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.late,
      }),
      columHelper.accessor("action", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.action,
      }),
      columHelper.accessor("detail", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.detail,
      }),
    ],
    []
  );

  return { columns };
};

const defaultData: IPost[] = [
  {
    grade: "Khối 11",
    class: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    totalStudent: "41",
    dayOff_Unaccepted: "undefined",
    dayOff_Accepted: "3",
    late: "3",
    action: "add",
    detail: "detail",
  },
  {
    grade: "Khối 11",
    class: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    totalStudent: "41",
    dayOff_Unaccepted: "undefined",
    dayOff_Accepted: "3",
    late: "3",
    action: "add",
    detail: "detail",
  },
  {
    grade: "Khối 11",
    class: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    totalStudent: "41",
    dayOff_Unaccepted: "undefined",
    dayOff_Accepted: "3",
    late: "3",
    action: "add",
    detail: "detail",
  },
  {
    grade: "Khối 11",
    class: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    totalStudent: "41",
    dayOff_Unaccepted: "undefined",
    dayOff_Accepted: "3",
    late: "3",
    action: "add",
    detail: "detail",
  },
  {
    grade: "Khối 11",
    class: "11A",
    teacher: "Nguyễn Vân Anh Anh",
    totalStudent: "41",
    dayOff_Unaccepted: "undefined",
    dayOff_Accepted: "3",
    late: "3",
    action: "add",
    detail: "detail",
  },
];
