import "@admin-ui/styles/globals.css";

import { CoreAdminContext, fetchUtils } from "@hola/ui-core";

import type { AppProps } from "next/app";
import Head from "next/head";
// import { useDehydratedState } from 'use-dehydrated-state';
import { MantineProvider } from "@mantine/core";
import { QueryClient } from "@tanstack/react-query";
import simpleRestProvider from "@hola/data-amplication-rest";
// import jsonServerProvider from "ra-data-json-server";
import { useState } from "react";
import Layout from "@admin-ui/components/layouts";
import authProvider from "@admin-ui/constants/auth-provider";

export default function App({ Component, pageProps }: AppProps) {
  const [queryClient] = useState(() => new QueryClient());
  // const dehydratedState = useDehydratedState();
  const fetchJson = (url, options = {}) => {
    // if (!options.headers) {
    //   options.headers = new Headers({ Accept: "application/json" });
    // }

    const user = {
      token: `Bearer ${"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJlMDI1MGNmMy1iNDhkLTRmZTEtYmYyMS1hMjkzZjU1NjIwOWEiLCJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNjk0MTQyODU4LCJleHAiOjE2OTU4NzA4NTh9.u8j4dSDjasjXs1KE6z_m4Vxkne6nrUUC8gl6pYcaBpU"}`,
      authenticated: true,
    };
    return fetchUtils.fetchJson(url, { ...options, user });
  };

  return (
    <>
      <Head>
        <title>Admin - UI</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>

      {/* <QueryClientProvider client={queryClient}> */}
      {/* <Hydrate state={pageProps.dehydratedState}> */}
      <CoreAdminContext
        authProvider={authProvider}
        dataProvider={simpleRestProvider(
          // "https://jsonplaceholder.typicode.com"

          "http://35.181.152.240:3005/api",
          // process.env.NEXT_PUBLIC_API_URL || "http://35.181.152.240:3005/api",
          fetchJson
        )}
      >
        <MantineProvider
          withGlobalStyles
          withNormalizeCSS
          theme={{
            /** Put your mantine theme override here */
            colorScheme: "light",
          }}
        >
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </MantineProvider>
      </CoreAdminContext>
      {/* </Hydrate> */}
      {/* </QueryClientProvider> */}
    </>
  );
}
