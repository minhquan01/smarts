import {
  ConstFormProvider,
  ConstUseForm,
} from "@admin-ui/components/core-form/form.context";
import { Box, Button, Grid } from "@mantine/core";

import RenderInput from "@admin-ui/components/render-input";
import { useState } from "react";

function Report() {
  const form = ConstUseForm({
    initialValues: {},
  });
  const [values, setValues] = useState([]);
  const [errs, setErrs] = useState([]);

  const handleOnSubmit = (values: any) => {
    const arr = fields.map((item) => {
      if (!values[item.name]) return item.name;
    });
    setValues(values);
    setErrs(arr);
  };

  // RenderInput { type, span, label, require, defaultValue, data } fields = [{}]

  const fields = [
    {
      type: "text",
      span: 3,
      label: "School name",
      name: "school_name",
      require: true,
      defaultValue: "THPT Hoang Van Thu",
    },
    {
      type: "select",
      span: 3,
      label: "School type",
      name: "school_type",
      require: true,
      defaultValue: "International School",
      data: ["International School", "cong lap", "dan lap", "giao duong"],
    },
    {
      type: "select",
      span: 3,
      label: "School Level",
      name: "school_level",
      require: true,
      defaultValue: "Primary School",
      data: ["Primary School", "Secondary School", "High School"],
    },
    {
      type: "text",
      span: 3,
      label: "Principle",
      name: "principle",
      require: true,
      defaultValue: "Le Manh Truong",
    },
    {
      type: "select",
      span: 3,
      label: "Province/City",
      name: "city",
      require: true,
      defaultValue: "Ha Noi",
      data: ["Ha Noi", "HCMc", "Da Nang", "Quang Ninh"],
    },
    {
      type: "text",
      span: 3,
      label: "Principles",
      name: "principles",
      require: true,
      defaultValue: "Le Manh Truong",
    },
    {
      type: "select",
      span: 3,
      label: "Province/City",
      name: "cityy",
      require: true,
      defaultValue: "Ha Noi",
      data: ["Ha Noi", "HCMc", "Da Nang", "Quang Ninh"],
    },
  ];

  return (
    <ConstFormProvider form={form}>
      <Box className="flex justify-between items-center pb-10">
        <Box
          className="relative flex justify-center items-center w-32 h-32 bg-cyan-300 rounded-full"
          style={{
            background:
              "var(--gradient-2, linear-gradient(to right, #00B4EC 0%, #FEF834 100%))",
          }}
        >
          <Box className="bg-white w-28 h-28 rounded-full flex items-center justify-center">
            <img
              className="w-28 h-28 rounded-full object-cover"
              src="https://images.unsplash.com/photo-1600984575359-310ae7b6bdf2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1964&q=80"
              alt=""
            />
          </Box>
          <Box className="absolute bg-white w-12 h-12 rounded-full flex items-center justify-center right-0 -bottom-2">
            {/* <InputFileForm /> */}
          </Box>
        </Box>
        <Box>profile</Box>
      </Box>
      <form onSubmit={form.onSubmit(handleOnSubmit)}>
        <Grid>
          {fields.map((_data, index) => (
            <RenderInput
              key={index}
              type={_data.type}
              label={_data.label}
              required={_data.require}
              value={_data.defaultValue}
              span={_data.span}
              data={_data?.data}
              name={_data?.name}
              error={
                errs.includes(_data.name) ? "Không được trống trường này" : ""
              }
            />
          ))}
        </Grid>
        <Box className="pt-6 text-right">
          <Button type="submit" color="cyan" className="min-w-[192px]">
            Cập nhật
          </Button>
        </Box>
      </form>
    </ConstFormProvider>
  );
}

export default Report;
