import {
  ConstFormProvider,
  ConstUseForm,
} from "@admin-ui/components/core-form/form.context";
import RenderInput from "@admin-ui/components/render-input";
import TitleAuth from "@admin-ui/components/ui/text/title.auth";
import ResetPassword from "@admin-ui/containers/login/reset.password";
import SignIn from "@admin-ui/containers/login/signin";
import {
  Box,
  Button,
  Checkbox,
  Grid,
  Group,
  PasswordInput,
  Tabs,
  TextInput,
} from "@mantine/core";
import { useRouter } from "next/router";
import React, { useState } from "react";

const tabs = ["signin", "reset"];
const Login = () => {
  const [activeTab, setActiveTab] = useState<string>(tabs[0]);
  const form = ConstUseForm({
    initialValues: {},
  });

  const handleSubmit = (values) => {
    if (activeTab === "signin") {
      // setActiveTab("reset");
    }
  };
  return (
    <ConstFormProvider form={form}>
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Tabs value={activeTab}>
          <Tabs.Panel value="signin">
            <SignIn />
          </Tabs.Panel>
          <Tabs.Panel value="reset">
            <ResetPassword />
          </Tabs.Panel>
        </Tabs>
      </form>
    </ConstFormProvider>
  );
};

export default Login;
