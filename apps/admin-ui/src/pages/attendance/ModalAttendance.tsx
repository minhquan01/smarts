import {
  InputTextForm,
  SelectInputForm,
} from "@admin-ui/components/core-form/input-core";
import { Box, Text } from "@mantine/core";

import { Avatar } from "@admin-ui/assets";
import { ConstUseForm } from "@admin-ui/components/core-form/form.context";
import InputDateBirthForm from "@admin-ui/components/core-form/input-core/input.date.field";
import ModalCustom from "@admin-ui/components/modal/modal.custom";
import { renderGender } from "@admin-ui/utils";
import dayjs from "dayjs";
import { useEffect } from "react";

interface IAttendanceProps {
  title?: string;
  id: string;
  datas: any;
  type: string;
  onOpened: boolean;
  onClose: () => void;
}
function ModalAttendance({
  id,
  datas,
  type,
  onOpened,
  onClose,
  title,
}: IAttendanceProps) {
  const form = ConstUseForm({
    initialValues: {},
  });
  const handleSubmit = (val) => {
    onClose();
  };

  const convertHour = (inputHour) => {
    const hour = new Date(inputHour).getHours();
    const minutes = new Date(inputHour).getMinutes();

    if (Number.isNaN(hour)) {
      return "Chưa điểm danh";
    }
    return hour + ":" + minutes;
  };
  const funcStatus = (startTime, checkinTime) => {
    const hourC = new Date(checkinTime).getHours();
    const minuteC = new Date(checkinTime).getMinutes();
    var timeArray = startTime.split(":");

    var hour = timeArray[0];
    var minute = timeArray[1];

    if (Number.isNaN(hourC) || Number.isNaN(minuteC)) {
      return "Nghỉ có phép";
    }
    if (hourC > hour || (hourC == hour && minuteC > minute)) {
      return "Đi muộn";
    }
    if (hourC < hour || (hourC == hour && minuteC == minute)) {
      return "Có mặt";
    }
  };

  useEffect(() => {
    form.setValues({
      nameDay: dayjs(datas?.birthday).get("date"),
      nameMonth: dayjs(datas?.birthday).get("month") + 1,
      nameYear: dayjs(datas?.birthday).get("year"),
    });
  }, [datas, onOpened]);

  return (
    <>
      <ModalCustom
        opened={onOpened}
        onClose={onClose}
        title={title}
        // type={type}
        id={id}
        handleSubmit={handleSubmit}
        form={form}
        actionText="Cập nhật"
        size="1128px"
      >
        <Box className="flex justify-between gap-8">
          <Box className="w-[66%]">
            <Box className="flex gap-4 pb-4">
              <InputTextForm
                type="text"
                label="Mã học sinh"
                value={datas.code}
                defaultValue={datas.code}
                className="w-full"
                name=""
                readOnly
              />
              <InputTextForm
                type="text"
                label="Họ và tên"
                value={datas.name}
                defaultValue={datas.name}
                className="w-full"
                name=""
                readOnly
              />
            </Box>
            <Box className="flex gap-4">
              <SelectInputForm
                type="select"
                label="Giới tính"
                value={renderGender(datas.gender)}
                defaultValue={renderGender(datas.gender)}
                className="pointer-events-none opacity-70"
                data={["Nam", "Nữ", "Khác"]}
                name=""
                readOnly
              />
              <InputDateBirthForm
                nameDay={"nameDay"}
                nameYear={"nameYear"}
                nameMonth={"nameMonth"}
                label={"Ngày sinh"}
                value={"02/01/2010"}
                placeholder={"placeholder"}
                defaultValue={"02/01/2010"}
                className="pointer-events-none opacity-70"
                name=""
                readOnly
              />
              <InputTextForm
                type="text"
                label="Lớp"
                value={datas.classrooms?.name}
                defaultValue={datas.classrooms?.name}
                className="pointer-events-none opacity-70"
                name=""
                readOnly
              />
            </Box>
            <Box className="flex gap-4 pb-4">
              <InputTextForm
                type="text"
                label="Ngày điểm danh"
                value={dayjs(new Date()).format("DD/MM/YYYY")}
                defaultValue={dayjs(new Date()).format("DD/MM/YYYY")}
                className="w-full"
                name=""
                readOnly
              />
              <InputTextForm
                type="text"
                label="Buổi điểm danh"
                value={"Sáng"}
                defaultValue={"Sáng"}
                className="w-full"
                name=""
                readOnly
              />
            </Box>
            <Box className="flex gap-4 pb-4">
              <InputTextForm
                type="text"
                label="Giờ vào"
                value={convertHour(datas.attendance[0]?.checkinTime)}
                defaultValue={convertHour(datas.attendance[0]?.checkinTime)}
                className="w-full"
                name=""
                readOnly
              />
              <InputTextForm
                type="text"
                label="Giờ ra"
                value={convertHour(datas.attendance[0]?.checkoutTime)}
                defaultValue={convertHour(datas.attendance[0]?.checkoutTime)}
                className="w-full"
                name=""
                readOnly
              />
            </Box>
            <Box className="flex gap-4 pb-4">
              <SelectInputForm
                type="text"
                label="Trạng thái"
                value={funcStatus("9:00", datas.attendance[0]?.checkinTime)}
                defaultValue={funcStatus(
                  "9:00",
                  datas.attendance[0]?.checkinTime
                )}
                data={["Đi muộn", "Có mặt", "Nghỉ có phép", "Nghỉ không phép"]}
                name="status"
                className="w-full"
              />
              <Box className="w-full"></Box>
            </Box>
          </Box>
          <Box className="w-[33%] py-2">
            <Text className="text-[16px] font-semibold">Hình ảnh</Text>
            <img src={Avatar.src} className="w-full pt-4" />
          </Box>
        </Box>
      </ModalCustom>
    </>
  );
}

export default ModalAttendance;
