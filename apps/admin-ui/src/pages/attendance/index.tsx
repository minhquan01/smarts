import { IconDirectboxReceive, IconDocumentDownload } from "@admin-ui/assets";
import DatePickerCustom, {
  getDateForType,
} from "@admin-ui/components/core-form/input.datepicker";
import { Box, Text } from "@mantine/core";
import { IconMessage2Pin, IconUserCircle } from "@tabler/icons-react";
import { SortingState, createColumnHelper } from "@tanstack/react-table";
import { useEffect, useMemo, useState } from "react";

import InputIconSearch from "@admin-ui/components/core-form/input.iconSearch";
import SelectSearch from "@admin-ui/components/core-form/select.search";
import CoreTable from "@admin-ui/components/core-table";
import { defaultSelectClass } from "@admin-ui/constants";
import { IHeadCell } from "@admin-ui/types";
import { useGetList } from "@hola/ui-core";
import Link from "next/link";
import { useRouter } from "next/router";
import ModalAttendance from "./ModalAttendance";

enum TimePeriods {
  day = "day",
  week = "week",
  month = "month",
  season = "season",
}

function Attendance() {
  const { columns, columnsDay, dataEd, showDetail, setShowDetail, id } =
    useTablePost();
  const [rowSelection, setRowSelection] = useState<Record<string, boolean>>({
    0: true,
  });

  const [timePeriod, setTimePeriod] = useState<TimePeriods>(TimePeriods.day);
  const [today, setToday] = useState(new Date());
  const [rangeW, setRangeW] = useState<[Date | null, Date | null]>([
    null,
    null,
  ]);
  const [rangeM, setRangeM] = useState<[Date | null, Date | null]>([
    null,
    null,
  ]);

  const funcSetDate = () => {
    setRangeW([
      getDateForType("week", today, "first"),
      getDateForType("week", today, "last"),
    ]);
    setRangeM([
      getDateForType("month", today, "first"),
      getDateForType("month", today, "last"),
    ]);
  };
  useEffect(() => {
    funcSetDate();
  }, [timePeriod]);

  const { data, isLoading, refetch } = useGetList(
    "attendances",
    {},
    {
      onSuccess: (res) => {
        // console.log({ res });
      },
      onError: (err) => {
        // console.log({ err });
      },
    }
  );

  return (
    <>
      <Box className="flex justify-between pb-10 gap-4">
        <Box className="flex justify-center items-center gap-5">
          <Box
            className={
              "border-b-[2px] cursor-pointer py-2 hover:opacity-100 hover:border-b-2 hover:border-b-[#00B4EC]" +
              `${
                timePeriod == "day"
                  ? " opacity-100 border-b-[#00B4EC]"
                  : " opacity-40 border-b-[#F0F4F7]"
              }`
            }
            onClick={() => {
              setTimePeriod(TimePeriods.day);
            }}
          >
            <Text className="">Ngày</Text>
          </Box>
          <Box
            className={
              "border-b-[2px] cursor-pointer py-2 hover:opacity-100 hover:border-b-2 hover:border-b-[#00B4EC]" +
              `${
                timePeriod == "week"
                  ? " opacity-100 border-b-[#00B4EC]"
                  : " opacity-40  border-b-[#F0F4F7]"
              }`
            }
            onClick={() => {
              setTimePeriod(TimePeriods.week);
            }}
          >
            <Text>Tuần</Text>
          </Box>
          <Box
            className={
              "border-b-[2px] cursor-pointer  py-2 hover:opacity-100 hover:border-b-2 hover:border-b-[#00B4EC]" +
              `${
                timePeriod == "month"
                  ? " border-b-[#00B4EC]"
                  : " opacity-40 border-b-[#F0F4F7]"
              }`
            }
            onClick={() => {
              setTimePeriod(TimePeriods.month);
            }}
          >
            <Text>Tháng</Text>
          </Box>
          <Box
            className={
              "border-b-[2px] cursor-pointer py-2 hover:opacity-100 hover:border-b-2 hover:border-b-[#00B4EC]" +
              `${
                timePeriod == "season"
                  ? " opacity-100 border-b-[#00B4EC]"
                  : " opacity-40  border-b-[#F0F4F7]"
              }`
            }
            onClick={() => {
              setTimePeriod(TimePeriods.season);
            }}
          >
            <Text>Học kỳ</Text>
          </Box>
        </Box>
        <Box className="flex gap-6 items-center">
          <InputIconSearch
            data={defaultSelectClass}
            className="flex items-center gap-2 font-medium w-36"
            placeholder="Tìm kiếm"
            clearable
          />
          <SelectSearch
            data={defaultSelectClass}
            className="flex items-center gap-2 font-medium w-28"
            placeholder="Lớp"
            clearable
          />
          {timePeriod == "season" && (
            <SelectSearch
              data={defaultSelectClass}
              className="flex items-center gap-2 font-medium w-28"
              placeholder="Học kỳ I"
              clearable
            />
          )}

          {timePeriod == "day" && (
            <Box className="flex gap-6">
              <SelectSearch
                data={defaultSelectClass}
                className="flex items-center gap-2 font-medium w-28"
                placeholder="Hôm nay"
                clearable
              />
              <DatePickerCustom type={timePeriod} value={today} />
            </Box>
          )}
          {timePeriod == "week" && (
            <Box className="flex gap-6">
              <SelectSearch
                data={defaultSelectClass}
                className="flex items-center gap-2 font-medium w-28"
                placeholder="Tuần này"
                clearable
              />
              <DatePickerCustom
                type={timePeriod}
                rangeW={rangeW}
                value={today}
              />
            </Box>
          )}
          {timePeriod == "month" && (
            <Box className="flex gap-6">
              <SelectSearch
                data={defaultSelectClass}
                className="flex items-center gap-2 font-medium w-30"
                placeholder="Tháng này"
                clearable
              />
              <DatePickerCustom
                type={timePeriod}
                rangeM={rangeM}
                value={today}
              />
            </Box>
          )}
        </Box>
      </Box>
      {timePeriod == "day" && (
        <CoreTable
          columns={columnsDay}
          data={data}
          loading={false}
          bodyHeight={500}
          rowSelection={rowSelection}
          setRowSelection={setRowSelection}
          // sorting={sorting}
          // setSorting={setSorting}
          // columnPinning={columnPinning}
          // setColumnPinning={setColumnPinning}
        />
      )}
      {timePeriod == "week" && (
        <CoreTable
          columns={columns}
          data={defaultData}
          loading={false}
          bodyHeight={500}
          rowSelection={rowSelection}
          setRowSelection={setRowSelection}
        />
      )}
      {timePeriod == "month" && (
        <CoreTable
          columns={columns}
          data={defaultData}
          loading={false}
          bodyHeight={500}
          rowSelection={rowSelection}
          setRowSelection={setRowSelection}
        />
      )}
      {timePeriod == "season" && (
        <CoreTable
          columns={columns}
          data={defaultData}
          loading={false}
          bodyHeight={500}
          rowSelection={rowSelection}
          setRowSelection={setRowSelection}
        />
      )}

      <Box className="flex flex-row justify-between items-center pt-20">
        <Box></Box>
        <Box></Box>
        <Box className="flex items-center gap-4">
          <Box className="flex flex-row gap-4 items-center">
            <IconDocumentDownload />
            <Text className="text-[16px] font-semibold">Xuất file</Text>
          </Box>
          <Box className="flex flex-row gap-4 items-center">
            <IconDirectboxReceive />
            <Text className="text-[16px] font-semibold">In file</Text>
          </Box>
        </Box>
      </Box>
      {showDetail && (
        <ModalAttendance
          onOpened={showDetail}
          onClose={() => setShowDetail(false)}
          title="Thông tin điểm danh học sinh"
          type="edit"
          datas={dataEd}
          id={id}
        />
      )}
    </>
  );
}

export default Attendance;

interface IPost {
  studentCode: string;
  fullName: string;
  totalOfLesson: string;
  permitted: string;
  notPermitted: string;
  lated: string;
}
const HEAD_CELLS: IHeadCell<IPost> = {
  studentCode: "Mã học sinh",
  fullName: "Họ và tên",
  totalOfLesson: "TS buổi học",
  permitted: "Nghỉ có phép",
  notPermitted: "Nghỉ không phép",
  lated: "Đi muộn",
  action: "",
};

interface IPostDay {
  id: string;
  code: string;
  name: string;
  startTime: string;
  checkinTime: string;
  checkoutTime: string;
  status: string;
  detail: any;
  action: any;
}
const HEAD_CELLSDAY: IHeadCell<IPostDay> = {
  code: "Mã học sinh",
  name: "Họ và tên",
  startTime: "Giờ vào lớp",
  checkinTime: "Giờ vào",
  checkoutTime: "Giờ ra",
  status: "Tình trạng",
  detail: "Chi tiết",
  action: "Hành động",
};

const columHelper = createColumnHelper<IPost>();
const columHelperDay = createColumnHelper<IPostDay>();

const useTablePost = () => {
  const [sorting, setSorting] = useState<SortingState>([]);
  const [selectOption, setSelectOption] = useState("");
  const [onSwitch, setOnSwitch] = useState<boolean>(false);
  const [onEdit, setOnEdit] = useState<boolean>(false);
  const [onRemove, setOnRemove] = useState<boolean>(false);
  const [type, setType] = useState<string>("");
  const [id, setId] = useState<string>("");
  const [dataEd, setDataEd] = useState({
    studentCode: "string",
    timeStart: "string",
    timeCheckin: "string",
    timeCheckout: "string",
    status: "string",
    fullName: "string",
  });
  const convertHour = (inputHour) => {
    const hour = new Date(inputHour).getHours();
    const minutes = new Date(inputHour).getMinutes();

    if (Number.isNaN(hour)) {
      return "Chưa điểm danh";
    }
    return hour + ":" + minutes;
  };
  const funcStatus = (startTime, checkinTime) => {
    const hourC = new Date(checkinTime).getHours();
    const minuteC = new Date(checkinTime).getMinutes();
    var timeArray = startTime.split(":");

    var hour = timeArray[0];
    var minute = timeArray[1];

    if (Number.isNaN(hourC) || Number.isNaN(minuteC)) {
      return "Nghỉ có phép";
    }
    if (hourC > hour || (hourC == hour && minuteC > minute)) {
      return "Đi muộn";
    }
    if (hourC < hour || (hourC == hour && minuteC == minute)) {
      return "Có mặt";
    }
  };
  const [showDetail, setShowDetail] = useState(false);
  const handleShowDetail = (id, data, type) => {
    setId(id);
    setDataEd(data);
    setShowDetail(true);
  };
  const route = useRouter();

  const columns = useMemo(
    () => [
      columHelper.accessor("studentCode", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.studentCode,
        minSize: 120,
      }),
      columHelper.accessor("fullName", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.fullName,
        maxSize: 300,
        minSize: 200,
      }),
      columHelper.accessor("totalOfLesson", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.totalOfLesson,
        minSize: 200,
      }),
      columHelper.accessor("permitted", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.permitted,
        minSize: 200,
      }),

      columHelper.accessor("notPermitted", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.notPermitted,
        minSize: 200,
      }),
      columHelper.accessor("lated", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.lated,
        minSize: 200,
      }),
    ],
    [onSwitch, onEdit, onRemove, type, id]
  );

  const columnsDay = useMemo(
    () => [
      columHelperDay.accessor("code", {
        cell: (context) => (
          <Link
            href={{
              pathname: "/student/attendance",
              query: {
                id: context.row.original?.id,
              },
            }}
          >
            <Text>{context.getValue()}</Text>
          </Link>
        ),
        header: () => HEAD_CELLSDAY.code,
        minSize: 120,
      }),
      columHelperDay.accessor("name", {
        cell: (context) => (
          <Link
            href={{
              pathname: "/student/attendance",
              query: {
                id: context.row.original?.id,
              },
            }}
          >
            <Text>{context.getValue()}</Text>
          </Link>
        ),
        header: () => HEAD_CELLSDAY.name,
        maxSize: 300,
        minSize: 200,
      }),
      columHelperDay.accessor("startTime", {
        cell: (context) => (
          <Link
            href={{
              pathname: "/student/attendance",
              query: {
                id: context.row.original?.id,
              },
            }}
          >
            {/* <Text>{context.getValue()}</Text> */}
            <Text>09:00</Text>
          </Link>
        ),
        header: () => HEAD_CELLSDAY.startTime,
        minSize: 100,
      }),
      columHelperDay.accessor("checkinTime", {
        cell: (context) => (
          <Link
            href={{
              pathname: "/student/attendance",
              query: {
                id: context.row.original?.id,
              },
            }}
          >
            <Text>
              {convertHour(context.row.original?.attendance[0]?.checkinTime)}
            </Text>
          </Link>
        ),
        header: () => HEAD_CELLSDAY.checkinTime,
        minSize: 100,
      }),

      columHelperDay.accessor("checkoutTime", {
        cell: (context) => (
          <Link
            href={{
              pathname: "/student/attendance",
              query: {
                id: context.row.original?.id,
              },
            }}
          >
            <Text>
              {convertHour(context.row.original?.attendance[0]?.checkoutTime)}
            </Text>
          </Link>
        ),
        header: () => HEAD_CELLSDAY.checkoutTime,
        minSize: 100,
      }),
      columHelperDay.accessor("status", {
        cell: (context) => (
          <Link
            href={{
              pathname: "/student/attendance",
              query: {
                id: context.row.original?.id,
              },
            }}
          >
            <Text>
              {funcStatus(
                "09:00",
                context.row.original?.attendance[0]?.checkinTime
              )}
            </Text>
          </Link>
        ),
        header: () => HEAD_CELLSDAY.status,
        minSize: 100,
      }),
      columHelperDay.accessor("detail", {
        cell: (context) => (
          <Box
            className="cursor-pointer flex justify-center"
            onClick={() => {
              handleShowDetail(
                context.row.original?.id,
                context.row.original,
                "detail"
              );
            }}
          >
            <IconUserCircle />
          </Box>
        ),

        header: () => HEAD_CELLSDAY.detail,
        minSize: 100,
      }),
      columHelperDay.accessor("action", {
        cell: (context) => (
          <Box className="cursor-pointer flex justify-center">
            <IconMessage2Pin />
          </Box>
        ),
        header: () => HEAD_CELLSDAY.action,
        minSize: 100,
      }),
    ],
    [showDetail]
  );

  return {
    columns,
    columnsDay,
    type,
    id,
    onEdit,
    setOnEdit,
    onSwitch,
    setOnSwitch,
    onRemove,
    setOnRemove,
    setType,
    dataEd,
    showDetail,
    setShowDetail,
  };
};

const defaultData: IPost[] = [
  {
    studentCode: "1",
    fullName: "Nguyễn Vân Anh Anh",
    totalOfLesson: "90",
    permitted: "5",
    notPermitted: "1",
    lated: "3",
  },
  {
    studentCode: "2",
    fullName: "Nguyễn Vân Anh Anh",
    totalOfLesson: "90",
    permitted: "5",
    notPermitted: "2",
    lated: "3",
  },
  {
    studentCode: "3",
    fullName: "Nguyễn Vân Anh Anh",
    totalOfLesson: "90",
    permitted: "5",
    notPermitted: "4",
    lated: "3",
  },
  {
    studentCode: "4",
    fullName: "Nguyễn Vân Anh Anh",
    totalOfLesson: "90",
    permitted: "5",
    notPermitted: "0",
    lated: "3",
  },
  {
    studentCode: "5",
    fullName: "Nguyễn Vân Anh Anh",
    totalOfLesson: "90",
    permitted: "5",
    notPermitted: "1",
    lated: "3",
  },
];

const defaultDataDay: IPostDay[] = [
  {
    id: "1",
    studentCode: "1",
    fullName: "Nguyễn Vân Anh Anh",
    timeStart: "08:00",
    timeCheckin: "07:50",
    timeCheckout: "17:00",
    status: "Có mặt",
    detail: "3",
    action: "",
  },
  {
    id: "2",
    studentCode: "2",
    fullName: "Nguyễn Vân Anh Anh",
    timeStart: "08:00",
    timeCheckin: "07:50",
    timeCheckout: "17:00",
    status: "Có mặt",
    detail: "3",
    action: "",
  },
  {
    id: "3",
    studentCode: "3",
    fullName: "Nguyễn Vân Anh Anh",
    timeStart: "08:00",
    timeCheckin: "07:50",
    timeCheckout: "17:00",
    status: "Có mặt",
    detail: "3",
    action: "",
  },
  {
    id: "4",
    studentCode: "4",
    fullName: "Nguyễn Vân Anh Anh",
    timeStart: "08:00",
    timeCheckin: "07:50",
    timeCheckout: "17:00",
    status: "Có mặt",
    detail: "3",
    action: "",
  },
  {
    id: "5",
    studentCode: "5",
    fullName: "Nguyễn Vân Anh Anh",
    timeStart: "08:00",
    timeCheckin: "07:50",
    timeCheckout: "17:00",
    status: "Có mặt",
    detail: "3",
    action: "",
  },
];
