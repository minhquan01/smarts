import { Box, Grid } from "@mantine/core";
import {
  ConstFormProvider,
  ConstUseForm,
} from "@admin-ui/components/core-form/form.context";

import DatePickerCustom from "@admin-ui/components/core-form/input.datepicker";
import { InputTextForm } from "@admin-ui/components/core-form/input-core";
import SelectSearch from "@admin-ui/components/core-form/select.search";
import { defaultSelectClass } from "@admin-ui/constants";
import { useRouter } from "next/router";
import { useState } from "react";

enum TimePeriods {
  day = "day",
  week = "week",
  month = "month",
  season = "season",
}
const aMonth = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
  23, 24, 25, 26, 27, 28, 29, 30,
];
function AttendceByStudentId({ ...props }, ref) {
  const router = useRouter();
  const form = ConstUseForm({
    initialValues: {},
  });

  const [timePeriod, setTimePeriod] = useState<TimePeriods>(TimePeriods.day);

  // Get data by id filter by month

  return (
    <ConstFormProvider form={form}>
      <form>
        <Box className="flex justify-between gap-10">
          <Box className="flex w-[60%] justify-between pb-4 gap-4">
            <InputTextForm
              type="text"
              label="Mã học sinh"
              value={"123"}
              defaultValue={"123"}
              className="pointer-events-none opacity-70 w-[160px]"
              name=""
            />
            <InputTextForm
              type="text"
              label="Họ và tên"
              value={"Le Manh Truong"}
              defaultValue={"Le Manh Truong"}
              className="pointer-events-none opacity-70 w-[160px]"
              name=""
            />
            <InputTextForm
              type="text"
              label="Giới tính"
              value={"Nam"}
              defaultValue={"Nam"}
              className="pointer-events-none opacity-70 w-[120px]"
              name=""
            />
            <InputTextForm
              type="text"
              label="Lớp"
              value={"9A"}
              defaultValue={"9A"}
              className="pointer-events-none opacity-70 w-[120px]"
              name=""
            />
          </Box>

          <Box className="flex w-[40%] justify-between items-center pt-4 gap-8">
            <SelectSearch
              data={defaultSelectClass}
              className="flex items-center gap-2 font-medium w-30"
              placeholder="Tháng này"
              clearable
            />
            <DatePickerCustom type={"month"} />
          </Box>
        </Box>
        <Box className="flex justify-between">
          <Box className="w-[20%]">
            <Box className="w-[60%]">
              <InputTextForm
                type="text"
                label="Số buổi học"
                value={"20"}
                defaultValue={"20"}
                className="pointer-events-none opacity-70 w-[160px] pt-5"
                name=""
              />
              <InputTextForm
                type="text"
                label="Vắng có phép"
                value={"01"}
                defaultValue={"01"}
                className="pointer-events-none opacity-70 w-[160px] pt-5"
                name=""
              />
              <InputTextForm
                type="text"
                label="Vắng không phép"
                value={"0"}
                defaultValue={"0"}
                className="pointer-events-none opacity-70 w-[160px] pt-5"
                name=""
              />
              <InputTextForm
                type="text"
                label="Đi muộn"
                value={"02"}
                defaultValue={"02"}
                className="pointer-events-none opacity-70 w-[160px] pt-5"
                name=""
              />
            </Box>
          </Box>
          <Box className="w-[80%] rounded-[8px] p-4 mt-8">
            <Grid columns={7} className="">
              {aMonth.map((item, index) => (
                <Grid.Col
                  span={1}
                  key={index}
                  className="border-[#F0F4F7] border-2 bg-white rounded-[8px] flex justify-center items-center h-24 cursor-pointer"
                  onClick={() => {}}
                >
                  {item}
                </Grid.Col>
              ))}
            </Grid>
          </Box>
        </Box>
      </form>
    </ConstFormProvider>
  );
}

export default AttendceByStudentId;
