// @ts-nocheck

import {
  IconDirectboxReceive,
  IconDirectboxSend,
  IconDocumentDownload,
  IconRoundPlus,
} from "@admin-ui/assets";
import { defaultSelectStatusStudent } from "@admin-ui/constants";
import { IHeadCell, IStudentProps } from "@admin-ui/types";
import { renderEthnicity, renderGender, renderReligion } from "@admin-ui/utils";
import { useDelete, useGetList } from "@hola/ui-core";
import { Box, Text } from "@mantine/core";
import {
  IconPencil,
  IconToggleLeft,
  IconToggleRight,
  IconTrash,
} from "@tabler/icons-react";
import { useMemo, useState } from "react";
import { useResource } from "@hola/ui-admin";
import ButtonCustom from "@admin-ui/components/button/button.custom";
import SelectSearch from "@admin-ui/components/core-form/select.search";
import CoreTable from "@admin-ui/components/core-table";
import { convertDateToString } from "@admin-ui/constants/convert";
import { renderStatusStudy } from "@admin-ui/utils";
import { createColumnHelper } from "@tanstack/react-table";
import ModalAddStudent from "@admin-ui/containers/student/modal.add";
import ModalEditStudent from "@admin-ui/containers/student/modal.edit";

const HEAD_CELLS: IHeadCell<IStudentProps> = {
  code: "Mã học sinh",
  name: "Họ và tên",
  gender: "Giới tính",
  birthday: "Ngày sinh",
  class: "Lớp",
  birthplace: "Nơi sinh",
  address: "Địa chỉ hiện tại",
  ethnicity: "Dân tộc",
  religion: "Tôn giáo",
  status: "Tình trạng",
  action: "...",
};

const Student = () => {
  const [filters, setFilters] = useState<any>({});
  const columnHelper = createColumnHelper<IStudentProps>();
  const [rowSelection, setRowSelection] = useState<Record<string, boolean>>({
    0: true,
  });
  const {
    list: dataStudent,
    refetch,
    deleteOne,
    setIdSelected,
    idSelected,
    openAddModal,
    setOpenAddModal,
    openEditModal,
    setOpenEditModal
  } = useResource({ resource: "students" });

  // const { data: dataStudent, refetch } = useGetList("students", {}, {});
  // console.log(dataStudent);
  const { data } = useGetList("classrooms");

  const [onSwitch, setOnSwitch] = useState<boolean>(false);
  const [onRemove, setOnRemove] = useState<boolean>(false);
  const [type, setType] = useState<string>("");

  console.log({ dataStudent });
  const handleEdit = (id, type) => {
    setIdSelected(id);
    setOpenEditModal(true);
  };
  const handleRemove = (id, type) => {
    deleteOne("students", {
      id: id,
    });
    refetch;
  };
  const handleSwitch = (id, type) => {
    setOnSwitch(!onSwitch);
    setType(type);
    setIdSelected(id);
  };

  const dataSelectClass = useMemo(() => {
    if (!data) return [];
    const dataMap = data.map((item) => ({
      value: item.id,
      label: item.name,
    }));
    return dataMap;
  }, [data]);

  const columns = useMemo(() => {
    return [
      // @ts-ignore
      columnHelper.accessor("code", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.code,
      }),
      columnHelper.accessor("name", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.name,
      }),
      columnHelper.accessor("gender", {
        cell: (context) => <Text>{renderGender(context.getValue())}</Text>,
        header: () => HEAD_CELLS.gender,
      }),
      columnHelper.accessor("birthday", {
        cell: (context) => (
          <Text>{convertDateToString(context.getValue())}</Text>
        ),
        header: () => HEAD_CELLS.birthday,
      }),
      {
        id: "class",
        cell: (context) => (
          <Text>{context?.row?.original?.classrooms?.name}</Text>
        ),
        header: () => HEAD_CELLS.class,
      },
      columnHelper.accessor("birthplace", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.birthplace,
      }),
      columnHelper.accessor("address", {
        cell: (context) => <Text>{context.getValue()}</Text>,
        header: () => HEAD_CELLS.address,
      }),
      columnHelper.accessor("ethnicity", {
        cell: (context) => <Text>{renderEthnicity(context.getValue())}</Text>,
        header: () => HEAD_CELLS.ethnicity,
      }),
      columnHelper.accessor("religion", {
        cell: (context) => <Text>{renderReligion(context.getValue())}</Text>,
        header: () => HEAD_CELLS.religion,
      }),
      columnHelper.accessor("status", {
        cell: (context) => <Text>{renderStatusStudy(context.getValue())}</Text>,
        header: () => HEAD_CELLS.status,
      }),
      {
        id: "action",
        cell: (context) => (
          <Box className="flex flex-row justify-center gap-4">
            {
              <Box
                className="cursor-pointer"
                onClick={() => handleSwitch(context.row.original?.id, "switch")}
              >
                {onSwitch && idSelected == context.row.original?.id ? (
                  <IconToggleLeft />
                ) : (
                  <IconToggleRight />
                )}
              </Box>
            }
            <Box
              className="cursor-pointer"
              onClick={() => handleEdit(context.row.original?.id, "edit")}
            >
              <IconPencil />
            </Box>
            <Box
              className="cursor-pointer"
              onClick={() => handleRemove(context.row.original?.id, "remove")}
            >
              <IconTrash />
            </Box>
          </Box>
        ),
        header: () => HEAD_CELLS.action,
      },
    ];
  }, []);
  const handleFilterClass = (value) => {
    setFilters({
      ...filters,
      classrooms: { id: value },
    });

    // refetch();
  };

  return (
    <>
      <Box className="flex justify-between items-center w-full pb-10 gap-4">
        <Box className="flex gap-6 items-center">
          <SelectSearch
            data={dataSelectClass}
            className="flex items-center gap-2 font-medium"
            placeholder="Lớp"
            clearable
            onChange={(value) => handleFilterClass(value)}
          />
          <SelectSearch
            data={defaultSelectStatusStudent}
            className="flex items-center gap-2 font-medium"
            placeholder="Tình trạng"
            clearable
          />
        </Box>
        <Box className="flex items-center gap-5">
          <Box className="flex items-center gap-2">
            <IconDirectboxReceive />
            <IconDirectboxSend />
            <IconDocumentDownload />
          </Box>
          <ButtonCustom
            onClick={() => setOpenAddModal(true)}
            leftIcon={<IconRoundPlus />}
          >
            Thêm mới học sinh
          </ButtonCustom>
        </Box>
      </Box>

      <CoreTable<IStudentProps>
        columns={columns}
        data={dataStudent}
        loading={false}
        bodyHeight={500}
        rowSelection={rowSelection}
        setRowSelection={setRowSelection}
      />

      <ModalAddStudent
        opened={openAddModal}
        close={() => setOpenAddModal(false)}
        refetch={refetch}
      />
      <ModalEditStudent
        opened={openEditModal}
        close={() => setOpenEditModal(false)}
        idEdit={idSelected}
        refetch={refetch}
      />
    </>
  );
};

export default Student;
