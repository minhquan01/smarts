import { Box, Button, Modal } from "@mantine/core";
import { ReactNode, useEffect } from "react";
import { ConstFormProvider, ConstUseForm } from "../core-form/form.context";

interface IFormType {
  id: string;
  openModal: boolean;
  title: string;
  type?: string;
  closeModal: () => void;
  iconClose?: boolean;
  children: ReactNode;
  handleSubmit: (val) => void;
  resetForm: boolean;
  loading: boolean;
  datas: {};
  hideBtn: boolean;
  size: string;
}
function ModalForm({
  openModal,
  closeModal,
  title,
  type,
  id,
  iconClose,
  children,
  handleSubmit,
  resetForm,
  loading,
  datas,
  hideBtn,
  size,
}: IFormType) {
  const form = ConstUseForm({
    initialValues: {},
  });
  console.log(datas);

  useEffect(() => {
    if (resetForm) {
      form.values = {};
    }
    if (type == "edit") {
      form.setValues(datas);
    }
  }, [resetForm]);

  const handleOnSubmit = (values: any) => {
    handleSubmit(values);
  };

  return (
    <Modal.Root
      opened={openModal}
      onClose={() => closeModal()}
      centered
      size={size ? size : "xl"}
      title={title}
      closeOnClickOutside={false}
      radius={"20px"}
      padding={"30px"}
      style={{ fontSize: "30px" }}
      styles={{
        inner: {
          zIndex: 9999,
        },
      }}
    >
      <Modal.Overlay
        onClick={() => closeModal()}
        style={{
          zIndex: 9999,
          backgroundColor: "rgba(32, 32, 32, 0.20)",
          backdropFilter: "blur(2px)",
        }}
      />
      <Modal.Content>
        <Modal.Header className="bg-[#F0F4F7]">
          <Modal.Title className="text-[20px] font-bold">{title}</Modal.Title>
          <Modal.CloseButton className="text-white bg-[#292D32] rounded-full hover:text-[#292D32] hover:bg-white" />
        </Modal.Header>
        <Modal.Body className="bg-[#F0F4F7]">
          <ConstFormProvider form={form}>
            <form onSubmit={form.onSubmit(handleOnSubmit)}>
              {children}
              {!hideBtn && (
                <Box className="pt-6 text-right">
                  <Button
                    type="button"
                    className="min-w-[136px] mr-5 text-[#292D32] text-opacity-40 bg-white border-[#292D32] border-opacity-40 hover:text-white hover:bg-[#292D32] hover:bg-opacity-40 hover:border-white"
                    onClick={() => closeModal()}
                    loading={loading}
                  >
                    Huỷ
                  </Button>
                  <Button
                    type="submit"
                    color="cyan"
                    className="min-w-[136px] bg-[#00B4EC] hover:bg-white hover:text-[#00B4EC]"
                    loading={loading}
                  >
                    {type != "edit" ? "Thêm mới" : "Cập nhật"}
                  </Button>
                </Box>
              )}
            </form>
          </ConstFormProvider>
        </Modal.Body>
      </Modal.Content>
    </Modal.Root>
  );
}

export default ModalForm;
