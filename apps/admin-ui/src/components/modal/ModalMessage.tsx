import { Box, Button, Modal } from "@mantine/core";

import { LogoSmartBlack } from "@admin-ui/assets";

interface IData {
  title?: string;
  message?: string;
  type?: string;
  onCancel: () => void;
  onSubmit: () => void;
  onOpen: boolean;
  loading?: boolean;
}
function ModalMessage({
  title,
  message,
  type,
  onCancel,
  onSubmit,
  onOpen,
  loading,
}: IData) {
  return (
    <Modal.Root
      opened={onOpen}
      closeOnClickOutside={false}
      centered
      radius={"8px"}
      style={{ width: "360px" }}
      size={"xs"}
      styles={{
        inner: {
          zIndex: 9999,
        },
      }}
      onClose={() => {}}
    >
      <Modal.Overlay
        style={{
          zIndex: 9999,
          backgroundColor: "rgba(32, 32, 32, 0.20)",
          backdropFilter: "blur(2px)",
        }}
      />
      <Modal.Content>
        <Modal.Header className="bg-[#F0F4F7] flex justify-center">
          <Modal.Title className="text-center">
            {title ? title : <img src={LogoSmartBlack.src} />}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="bg-[#F0F4F7]">
          <Box className="px-4 py-6 bg-white rounded-[12px]">
            <Box className="font-normal text-[14px]">{message}</Box>
            <Box className="pt-6 text-center flex">
              <Button
                type="button"
                className="flex-1 font-semibold mr-5 text-[#292D32] rounded-[8px] text-opacity-40 bg-[#F0F4F7] hover:text-white hover:bg-[#292D32] hover:bg-opacity-40 hover:border-white"
                onClick={() => onCancel()}
                loading={loading}
              >
                Huỷ
              </Button>

              <Button
                onClick={() => (type === "noti" ? onCancel() : onSubmit())}
                type="submit"
                className="flex-1 rounded-[8px] bg-[#00B4EC] hover:bg-white hover:text-[#00B4EC] hover:border-[#00B4EC]"
                loading={loading}
              >
                Xác nhận
              </Button>
            </Box>
          </Box>
        </Modal.Body>
      </Modal.Content>
    </Modal.Root>
  );
}

export default ModalMessage;
