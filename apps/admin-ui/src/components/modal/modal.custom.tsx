import { Box, Button, Modal, ModalProps } from "@mantine/core";
import { FormEvent, ReactNode, useEffect, useState } from "react";
import { ConstFormProvider } from "../core-form/form.context";

import { UseFormReturnType } from "@mantine/form";

interface IFormType extends ModalProps {
  opened: boolean;
  title: string;
  onClose: () => void;
  iconClose?: boolean;
  children: ReactNode;
  actionText?: string;
  cancelText?: string;
  size?: string;
  form: UseFormReturnType<unknown, (values: unknown) => unknown>;
  handleSubmit: (
    values: any | unknown,
    event?: FormEvent<HTMLFormElement>
  ) => void;
}
function ModalCustom({
  opened,
  onClose,
  title,
  iconClose,
  children,
  actionText = "Thêm mới",
  cancelText = "Huỷ",
  size = "xl",
  handleSubmit,
  form,
}: IFormType) {
  const [values, setValues] = useState([]);

  useEffect(() => {}, [values]);

  return (
    <Modal.Root
      opened={opened}
      onClose={() => onClose()}
      centered
      size={size}
      title={title}
      closeOnClickOutside={false}
      radius={"20px"}
      padding={"30px"}
      style={{ fontSize: "30px" }}
      styles={{
        inner: {
          zIndex: 9999,
        },
      }}
    >
      <Modal.Overlay
        onClick={() => onClose()}
        style={{
          zIndex: 9999,
          backgroundColor: "rgba(32, 32, 32, 0.20)",
          backdropFilter: "blur(2px)",
        }}
      />
      <Modal.Content>
        <Modal.Header className="bg-[#F0F4F7]">
          <Modal.Title className="text-[20px] font-bold">{title}</Modal.Title>
          {iconClose && (
            <Modal.CloseButton className="text-white bg-[#292D32] rounded-full hover:text-[#292D32] hover:bg-white" />
          )}
        </Modal.Header>
        <Modal.Body className="bg-[#F0F4F7]">
          <ConstFormProvider form={form}>
            <form onSubmit={form.onSubmit(handleSubmit)}>
              {children}
              <Box className="pt-6 text-right">
                <Button
                  type="button"
                  className="min-w-[136px] mr-5 text-[#292D32] text-opacity-40 bg-white border-[#292D32] border-opacity-40 hover:text-white hover:bg-[#292D32] hover:bg-opacity-40 hover:border-white"
                  onClick={() => onClose()}
                >
                  {cancelText}
                </Button>
                <Button
                  type="submit"
                  color="cyan"
                  className="min-w-[136px] bg-[#00B4EC] hover:bg-white hover:text-[#00B4EC]"
                >
                  {actionText}
                </Button>
              </Box>
            </form>
          </ConstFormProvider>
        </Modal.Body>
      </Modal.Content>
    </Modal.Root>
  );
}

export default ModalCustom;
