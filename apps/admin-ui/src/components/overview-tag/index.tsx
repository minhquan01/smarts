import { IconList, IconMortarBoard, IconUserCheck } from "@admin-ui/assets";
import { Box, Text } from "@mantine/core";

interface IProps {
  type: string;
  content: string;
  total: string;
}
function OverviewTag({ type, content, total }: IProps) {
  return (
    <Box className="flex-1 py-4 rounded-lg flex items-center bg-white">
      <Box className="p-2 mx-6 rounded-lg flex items-center gap-4 bg-bgPri">
        {type == "teacher" && <IconUserCheck />}
        {type == "student" && <IconMortarBoard />}
        {type == "class" && <IconList />}
      </Box>
      <Box className="">
        <Text className="text-[#00B4EC] text-[18px] font-bold">{total}</Text>
        <Text>{content}</Text>
      </Box>
    </Box>
  );
}

export default OverviewTag;
