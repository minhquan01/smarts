import { InputTextForm, SelectInputForm } from "../core-form/input-core";

import { Grid } from "@mantine/core";
import UploadFileImageForm from "../core-form/input-core/file-image.field";
import InputPasswordForm from "../core-form/input-core/input-password.field";
import InputDateBirthForm from "../core-form/input-core/input.date.field";

interface IProps {
  label?: string;
  span: number;
  placeholder?: string;
  type: string;
  required?: boolean;
  error?: string;
  value?: string;
  name?: string;
  nameDay?: string;
  nameMonth?: string;
  nameYear?: string;
  data?: Array<any>;
  typeFor?: "avatar" | "cover";
  disabled?: boolean;
}
function RenderInput({
  label,
  span,
  placeholder,
  name,
  type,
  nameDay,
  nameMonth,
  nameYear,
  disabled,
  required,
  error,
  value,
  data,
  typeFor = "avatar",
}: IProps) {
  console.log({
    name,
    value,
  });
  return (
    <Grid.Col span={span}>
      {type == "text" && (
        <InputTextForm
          name={name}
          withAsterisk={required}
          label={label}
          value={value}
          placeholder={placeholder}
          error={error}
          defaultValue={value}
          disabled={disabled}
        />
      )}
      {type == "select" && (
        <SelectInputForm
          name={name}
          withAsterisk={required}
          label={label}
          value={value}
          placeholder={placeholder}
          data={data}
          error={error}
          defaultValue={value}
        />
      )}
      {type == "upload-image" && (
        <UploadFileImageForm
          name={name}
          withAsterisk={required}
          error={error}
          typeFor={typeFor}
        />
      )}
      {type == "date-birth" && (
        <InputDateBirthForm
          nameDay={nameDay}
          nameYear={nameYear}
          nameMonth={nameMonth}
          label={label}
          value={value}
          placeholder={placeholder}
          defaultValue={value}
        />
      )}
      {type == "password" && (
        <InputPasswordForm
          name={name}
          withAsterisk={required}
          label={label}
          type="password"
          value={value}
          placeholder={placeholder}
          error={error}
          defaultValue={value}
        />
      )}
    </Grid.Col>
  );
}

export default RenderInput;
