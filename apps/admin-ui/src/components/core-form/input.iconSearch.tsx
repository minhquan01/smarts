import { Box, Input, SelectProps, createStyles, rem } from "@mantine/core";
import { SelectHTMLAttributes, forwardRef } from "react";

import { IconSearch } from "@tabler/icons-react";

export interface SelectSearchProps
  extends SelectHTMLAttributes<HTMLSelectElement> {}

const InputIconSearch = forwardRef<
  HTMLSelectElement,
  SelectProps & SelectSearchProps
>(({ className, ...props }, ref) => {
  const { classes } = useStyled();

  return (
    <Box className="flex flex-row justify-between items-center bg-white px-4 rounded-[8px]">
      <Input
        className={`rounded-md font-normal ${classes.select} ${className}`}
        ref={ref}
        {...props}
      />
      <IconSearch className="opacity-40 w-5 h-5" />
    </Box>
  );
});
InputIconSearch.displayName = "SelectSearch";
export default InputIconSearch;

const useStyled = createStyles(() => ({
  select: {
    "& input": {
      borderRadius: rem(8),
      border: 0,
      height: rem(44),
    },
    "& .mantine-Input-rightSection": {
      paddingRight: rem(8),
    },
  },
}));
