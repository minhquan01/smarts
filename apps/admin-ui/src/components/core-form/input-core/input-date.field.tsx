import { forwardRef, InputHTMLAttributes } from "react";

import { createStyles, TextInput, TextInputProps } from "@mantine/core";
import { ConstFormContext } from "../form.context";

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
}

const InputDateForm = forwardRef<HTMLInputElement, TextInputProps & IProps>(
  ({ className, name, ...props }, ref) => {
    const { classes, cx } = useStyled();
    const form = ConstFormContext();

    return (
      // @ts-ignore
      <TextInput
        name={name}
        className={cx(classes.input, className)}
        ref={ref}
        placeholder="DD/MM/YYYY"
        {...props}
        {...form.getInputProps(name)}
        onChange={(e) => {
          const value = formatDateToString(e.target);
          form.setFieldValue(name, value);
          e.target.value = value;
        }}
      />
    );
  }
);
InputDateForm.displayName = "InputDateForm";
export default InputDateForm;

const useStyled = createStyles(() => ({
  input: {
    "& input": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));

function formatDateToString(input: HTMLInputElement) {
  const inputValue = input.value.replace(/\D/g, "");
  let formattedValue = "";

  if (inputValue.length > 0) {
    formattedValue += inputValue.substring(0, 2);
  }

  if (inputValue.length > 2) {
    const month = inputValue.substring(2, 4);
    if (+month === 1) {
      formattedValue += "/" + month;
    } else if (+month > 1 && +month <= 12) {
      formattedValue += "/" + month.padStart(2, "0");
    } else {
      if (inputValue.length <= 4)
        formattedValue += "/" + inputValue.substring(2, 3);
      return formattedValue;
    }
  }

  if (inputValue.length > 4) {
    const year = inputValue.substring(4, 8);
    if (+year === 0) {
      return formattedValue;
    }
    formattedValue += "/" + inputValue.substring(4, 8);
  }

  input.value = formattedValue;
  return formattedValue;
}
