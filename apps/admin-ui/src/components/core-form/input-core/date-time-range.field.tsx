import { ButtonHTMLAttributes, forwardRef, useState } from "react";

import { createStyles } from "@mantine/core";
import { DatePickerInput, DatePickerInputProps } from "@mantine/dates";
import { ConstFormContext } from "../form.context";

interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  name: string;
}

const DateTimeRangeForm = forwardRef<
  HTMLButtonElement,
  DatePickerInputProps<"range"> & IProps
>(({ className, name, ...props }, ref) => {
  const [value, setValue] = useState<[Date | null, Date | null]>([null, null]);
  const { classes, cx } = useStyled();
  const form = ConstFormContext();

  return (
    <DatePickerInput
      name={name}
      className={cx(classes.date, className)}
      // @ts-ignore
      value={value}
      valueFormat="DD/MM/YYYY"
      // @ts-ignore
      onChange={setValue}
      type="range"
      mx="auto"
      placeholder="From - To"
      ref={ref}
      {...props}
      {...form.getInputProps(name)}
    />
  );
});
DateTimeRangeForm.displayName = "DateRangeForm";
export default DateTimeRangeForm;

const useStyled = createStyles(() => ({
  date: {
    "& button.mantine-DatePickerInput-input": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));
