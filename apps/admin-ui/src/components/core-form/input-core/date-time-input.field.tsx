import { forwardRef, InputHTMLAttributes } from "react";

import { createStyles } from "@mantine/core";
import { DateInput, DateInputProps } from "@mantine/dates";
import { ConstFormContext } from "../form.context";

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
}

const DateTimeInputForm = forwardRef<HTMLInputElement, DateInputProps & IProps>(
  ({ className, name, ...props }, ref) => {
    const { classes, cx } = useStyled();
    const form = ConstFormContext();

    return (
      // @ts-ignore
      <DateInput
        name={name}
        className={cx(classes.date, className)}
        valueFormat="DD/MM/YYYY"
        placeholder="DD/MM/YYYY"
        ref={ref}
        mx="auto"
        {...props}
        {...form.getInputProps(name)}
      />
    );
  }
);
DateTimeInputForm.displayName = "DateTimeInputForm";
export default DateTimeInputForm;

const useStyled = createStyles(() => ({
  date: {
    "& input": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));
