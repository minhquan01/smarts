import { NumberInput, TextInputProps, createStyles } from "@mantine/core";
import { InputHTMLAttributes, forwardRef } from "react";

import { ConstFormContext } from "../form.context";

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
}

const InputNumberForm = forwardRef<HTMLInputElement, TextInputProps & IProps>(
  ({ className, name, ...props }, ref) => {
    const { classes, cx } = useStyled();
    const form = ConstFormContext();

    return (
      // @ts-ignore
      <NumberInput
        hideControls
        name={name}
        className={cx(classes.input, className)}
        ref={ref}
        size="md"
        radius="8px"
        placeholder="Nhập"
        {...props}
        {...form.getInputProps(name)}
        error={props.error}
      />
    );
  }
);
InputNumberForm.displayName = "InputNumberForm";
export default InputNumberForm;

const useStyled = createStyles(() => ({
  input: {
    "& input": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));
