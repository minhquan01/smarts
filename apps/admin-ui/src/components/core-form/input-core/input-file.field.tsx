import { ButtonHTMLAttributes, forwardRef } from "react";

import { createStyles, FileInput, FileInputProps, rem } from "@mantine/core";
import { IconUpload } from "@tabler/icons-react";
import { ConstFormContext } from "../form.context";

interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  name: string;
}

const InputFileForm = forwardRef<HTMLButtonElement, FileInputProps & IProps>(
  ({ className, name, ...props }, ref) => {
    const { classes, cx } = useStyled();
    const form = ConstFormContext();

    return (
      <FileInput
        icon={<IconUpload size={rem(14)} />}
        className={cx(classes.file, className)}
        ref={ref}
        placeholder="Upload"
        {...props}
        {...form.getInputProps(name)}
      />
    );
  }
);
InputFileForm.displayName = "InputFileForm";
export default InputFileForm;

const useStyled = createStyles(() => ({
  file: {
    "& button": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));
