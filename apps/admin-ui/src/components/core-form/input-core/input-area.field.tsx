import { forwardRef, TextareaHTMLAttributes } from "react";

import { createStyles, Textarea, TextareaProps } from "@mantine/core";
import { ConstFormContext } from "../form.context";

interface IProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  name: string;
}

const InputTextareaForm = forwardRef<
  HTMLTextAreaElement,
  TextareaProps & IProps
>(({ className, name, ...props }, ref) => {
  const { classes, cx } = useStyled();
  const form = ConstFormContext();

  return (
    // @ts-ignore
    <Textarea
      name={name}
      className={cx(classes.input, className)}
      ref={ref}
      placeholder="Add text"
      autosize
      minRows={3}
      {...props}
      {...form.getInputProps(name)}
    />
  );
});
InputTextareaForm.displayName = "InputTextareaForm";
export default InputTextareaForm;

const useStyled = createStyles(() => ({
  input: {
    "& textarea": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));
