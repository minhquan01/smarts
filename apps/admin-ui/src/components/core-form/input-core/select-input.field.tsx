import { InputHTMLAttributes, forwardRef } from "react";
import { Select, SelectProps, createStyles, rem } from "@mantine/core";

import { ConstFormContext } from "../form.context";
import { IconChevronDown } from "@tabler/icons-react";

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
}

const SelectInputForm = forwardRef<HTMLInputElement, SelectProps & IProps>(
  ({ className, name, ...props }, ref) => {
    const { classes, cx } = useStyled();
    const form = ConstFormContext();
    return (
      // @ts-ignore
      <Select
        name={name}
        className={cx(classes.select, className)}
        rightSection={<IconChevronDown size={rem(14)} />}
        placeholder="Select"
        ref={ref}
        size="md"
        radius="8px"
        {...props}
        {...form.getInputProps(name)}
        error={props.error}
      />
    );
  }
);
SelectInputForm.displayName = "SelectInputForm";
export default SelectInputForm;

const useStyled = createStyles(() => ({
  select: {
    "& input": {
      backgroundColor: "white",
      borderColor: "white",
    },
    "& .mantine-Input-rightSection": {
      paddingRight: rem(8),
    },
  },
}));
