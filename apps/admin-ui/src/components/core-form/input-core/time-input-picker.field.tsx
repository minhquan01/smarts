import { forwardRef, InputHTMLAttributes, useRef } from "react";

import { createStyles, rem } from "@mantine/core";
import { TimeInput, TimeInputProps } from "@mantine/dates";
import { IconClock } from "@tabler/icons-react";
import { ConstFormContext } from "../form.context";

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
}

const TimeInputPickerForm = forwardRef<
  HTMLInputElement,
  TimeInputProps & IProps
>(({ className, name, ...props }, ref) => {
  const { classes, cx } = useStyled();
  const form = ConstFormContext();
  const timeRef = useRef<HTMLInputElement | null>(null);

  return (
    // @ts-ignore
    <TimeInput
      name={name}
      className={cx(classes.date, className)}
      rightSection={<IconClock size={rem(14)} />}
      ref={timeRef}
      onClick={() => timeRef?.current && timeRef.current.showPicker()}
      mx="auto"
      {...props}
      {...form.getInputProps(name)}
    />
  );
});
TimeInputPickerForm.displayName = "TimeInputPickerForm";
export default TimeInputPickerForm;

const useStyled = createStyles(() => ({
  date: {
    "& input": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));
