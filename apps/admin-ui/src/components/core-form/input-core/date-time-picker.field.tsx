import { ButtonHTMLAttributes, forwardRef } from "react";

import { createStyles, rem } from "@mantine/core";
import { DatePickerInputProps, DateTimePicker } from "@mantine/dates";
import { IconCalendar } from "@tabler/icons-react";
import { ConstFormContext } from "../form.context";

interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  name: string;
}

const DateTimePickerForm = forwardRef<
  HTMLButtonElement,
  DatePickerInputProps & IProps
>(({ className, name, ...props }, ref) => {
  const { classes, cx } = useStyled();
  const form = ConstFormContext();

  return (
    // @ts-ignore
    <DateTimePicker
      name={name}
      className={cx(classes.date, className)}
      rightSection={<IconCalendar size={rem(14)} />}
      ref={ref}
      placeholder="Select"
      mx="auto"
      {...props}
      {...form.getInputProps(name)}
    />
  );
});
DateTimePickerForm.displayName = "DateTimePickerForm";
export default DateTimePickerForm;

const useStyled = createStyles(() => ({
  date: {
    "& button.mantine-DateTimePicker-input": {
      backgroundColor: "white",
      borderColor: "white",
    },
    "& button.mantine-DateTimePicker-input[data-invalid]": {
      borderColor: "#fa5252",
    },
  },
}));
