import { ButtonHTMLAttributes, forwardRef } from "react";

import {
  Box,
  createStyles,
  FileInput,
  FileInputProps,
  rem,
} from "@mantine/core";
import { IconUpload } from "@tabler/icons-react";
import { ConstFormContext } from "../form.context";
import { IconCamera, User } from "@admin-ui/assets";
import UploadAvatar from "@admin-ui/components/ui/upload.avatar";
import UploadCover from "@admin-ui/components/ui/upload.cover";

interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  name: string;
  typeFor: "avatar" | "cover";
}

const UploadFileImageForm = forwardRef<
  HTMLButtonElement,
  FileInputProps & IProps
>(({ className, name, typeFor, ...props }, ref) => {
  const { classes, cx } = useStyled();
  const form = ConstFormContext();

  return (
    <Box className="w-full h-full">
      <FileInput
        className="hidden"
        ref={ref}
        id={name}
        accept="image/*"
        {...props}
        {...form.getInputProps(name)}
      />
      {typeFor === "avatar" ? (
        <UploadAvatar name={name} />
      ) : (
        <UploadCover name={name} />
      )}
    </Box>
  );
});
UploadFileImageForm.displayName = "UploadFileImageForm";
export default UploadFileImageForm;

const useStyled = createStyles(() => ({
  file: {
    "& button": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));
