import {
  PasswordInput,
  TextInput,
  TextInputProps,
  createStyles,
} from "@mantine/core";
import { InputHTMLAttributes, forwardRef } from "react";

import { ConstFormContext } from "../form.context";

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
}

const InputPasswordForm = forwardRef<HTMLInputElement, TextInputProps & IProps>(
  ({ className, name, ...props }, ref) => {
    const { classes, cx } = useStyled();
    const form = ConstFormContext();

    return (
      // @ts-ignore
      <PasswordInput
        name={name}
        className={cx(classes.input, className)}
        ref={ref}
        size="md"
        radius="8px"
        placeholder="Nhập"
        {...props}
        {...form.getInputProps(name)}
        error={props.error}
      />
    );
  }
);
InputPasswordForm.displayName = "InputPasswordForm";
export default InputPasswordForm;

const useStyled = createStyles(() => ({
  input: {
    "& input": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));
