import { InputHTMLAttributes, useRef } from "react";

import { Box } from "@mantine/core";
import InputNumberForm from "./input.number";

interface InputDateBirthFormProps
  extends InputHTMLAttributes<HTMLInputElement> {
  nameDay: string;
  nameMonth: string;
  nameYear: string;
  label?: string;
}

function InputDateBirthForm({ ...props }, ref: InputDateBirthFormProps) {
  const { nameDay, nameMonth, nameYear, label } = props;
  const dayRef = useRef(null);
  const monthRef = useRef(null);
  const yearRef = useRef(null);

  const handleInputChange = (e, nextRef, prevRef) => {
    const value = e.target.value;

    if (value.length === e.target.maxLength && nextRef?.current) {
      nextRef.current.focus();
    }
    if (value.length === 0 && prevRef?.current) {
      prevRef.current.focus();
    }
  };

  return (
    <Box className="flex items-center gap-x-3">
      <InputNumberForm
        label={label ?? ""}
        required
        ref={dayRef}
        placeholder="dd"
        // onChange={(e) => handleInputChange(e, monthRef, null)}
        maxLength={2}
        max={31}
        name={nameDay}
        className={props?.className}
      />
      <InputNumberForm
        ref={monthRef}
        label={"  "}
        placeholder="mm"
        // onChange={(e) => handleInputChange(e, yearRef, dayRef)}
        maxLength={2}
        max={12}
        name={nameMonth}
        className={props?.className}
      />
      <InputNumberForm
        ref={yearRef}
        label={"  "}
        placeholder="yyyy"
        // onChange={(e) => handleInputChange(e, null, monthRef)}
        maxLength={4}
        min={1900}
        max={2023}
        name={nameYear}
        className={props?.className}
      />
    </Box>
  );
}

export default InputDateBirthForm;
