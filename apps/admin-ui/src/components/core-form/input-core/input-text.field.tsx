import { InputHTMLAttributes, forwardRef } from "react";
import { TextInput, TextInputProps, createStyles } from "@mantine/core";

import { ConstFormContext } from "../form.context";

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
}

const InputTextForm = forwardRef<HTMLInputElement, TextInputProps & IProps>(
  ({ className, name, ...props }, ref) => {
    const { classes, cx } = useStyled();
    const form = ConstFormContext();

    return (
      // @ts-ignore
      <TextInput
        name={name}
        className={cx(classes.input, className)}
        ref={ref}
        size="md"
        radius="8px"
        placeholder="Nhập"
        {...props}
        {...form.getInputProps(name)}
        error={props.error}
      />
    );
  }
);
InputTextForm.displayName = "InputTextForm";
export default InputTextForm;

const useStyled = createStyles(() => ({
  input: {
    "& input": {
      backgroundColor: "white",
      borderColor: "white",
    },
  },
}));
