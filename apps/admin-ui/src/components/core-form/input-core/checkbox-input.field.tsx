import { forwardRef, InputHTMLAttributes } from "react";

import { Checkbox, CheckboxProps, createStyles } from "@mantine/core";
import { ConstFormContext } from "../form.context";

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
}

const CheckboxInputForm = forwardRef<HTMLInputElement, CheckboxProps & IProps>(
  ({ className, name, ...props }, ref) => {
    const { classes, cx } = useStyled();
    const form = ConstFormContext();

    return (
      <Checkbox
        name={name}
        className={cx(classes.input, className)}
        ref={ref}
        {...props}
        {...form.getInputProps(name, { type: "checkbox" })}
      />
    );
  }
);
CheckboxInputForm.displayName = "CheckboxInputForm";
export default CheckboxInputForm;

const useStyled = createStyles(() => ({
  input: {
    "& .mantine-Checkbox-input:checked": {
      borderColor: "white",
    },
  },
}));
