import { forwardRef, SelectHTMLAttributes } from "react";

import { createStyles, rem, Select, SelectProps } from "@mantine/core";
import { IconChevronDown } from "@tabler/icons-react";
import { IconArrowDown } from "@admin-ui/assets";

export interface SelectSearchProps
  extends SelectHTMLAttributes<HTMLSelectElement> {}

const SelectSearch = forwardRef<
  HTMLSelectElement,
  SelectProps & SelectSearchProps
>(({ className, ...props }, ref) => {
  const { classes } = useStyled();
  const { clearable } = props;

  return (
    <Select
      className={`rounded-md ${classes.select} ${className}`}
      // @ts-ignore
      ref={ref}
      // rightSection={<IconArrowDown />}
      {...props}
      clearable={clearable}
    />
  );
});
SelectSearch.displayName = "SelectSearch";
export default SelectSearch;

const useStyled = createStyles(() => ({
  select: {
    "& input": {
      borderRadius: rem(8),
      border: 0,
      height: rem(44),
    },
    "& .mantine-Input-rightSection": {
      paddingRight: rem(8),
    },
  },
}));
