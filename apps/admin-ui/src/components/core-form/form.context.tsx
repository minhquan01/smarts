import { createFormContext } from "@mantine/form";

export const [ConstFormProvider, ConstFormContext, ConstUseForm] =
  createFormContext<unknown>();
