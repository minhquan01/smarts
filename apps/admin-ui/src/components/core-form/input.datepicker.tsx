import "dayjs/locale/es";

import { Box, SelectProps, createStyles, rem } from "@mantine/core";
import { SelectHTMLAttributes, forwardRef, useEffect, useState } from "react";

import { DatePickerInput } from "@mantine/dates";
import { IconCalendar } from "@tabler/icons-react";
import dayjs from "dayjs";

export function getDay(date: Date) {
  const day = date.getDay();
  return day === 0 ? 6 : day - 1;
}

export function startOfWeek(date: Date) {
  return new Date(
    date.getFullYear(),
    date.getMonth(),
    date.getDate() - getDay(date) - 1
  );
}

export function endOfWeek(date: Date) {
  return dayjs(
    new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate() + (6 - getDay(date))
    )
  )
    .endOf("date")
    .toDate();
}

export function getFirstDayOfMonth(date: Date) {
  return new Date(date.getFullYear(), date.getMonth(), 1);
}
export function getLastDayOfMonth(date: Date) {
  return new Date(date.getFullYear(), date.getMonth() + 1, 0);
}

export function getDateForType(type: any, dateVal, position) {
  if (type == "week" && position == "first") {
    return startOfWeek(dateVal);
  }
  if (type == "week" && position == "last") {
    return endOfWeek(dateVal);
  }
  if (type == "month" && position == "first") {
    return getFirstDayOfMonth(dateVal);
  }
  if (type == "month" && position == "last") {
    return getLastDayOfMonth(dateVal);
  }
}

export function isInTimeRange(date: Date, value: Date | null, type: any) {
  return (
    value &&
    dayjs(date).isBefore(
      type === "week" ? endOfWeek(value) : getLastDayOfMonth(value)
    ) &&
    dayjs(date).isAfter(
      type === "week" ? startOfWeek(value) : getFirstDayOfMonth(value)
    )
  );
}
export interface SelectSearchProps
  extends SelectHTMLAttributes<HTMLSelectElement> {}

const DatePickerCustom = forwardRef<
  HTMLSelectElement,
  SelectProps & SelectSearchProps
>(({ className, type, ...props }, ref) => {
  const { classes } = useStyled();
  const [value, setValue] = useState<Date>(props?.value);
  const [hovered, setHovered] = useState<Date>(props?.value);
  const [rangeVal, setRangeVal] = useState<[Date | null, Date | null]>([
    null,
    null,
  ]);
  useEffect(() => {
    if (type == "week") {
      setRangeVal(props?.rangeW);
    }
    if (type == "month") {
      setRangeVal(props?.rangeM);
    }
  }, [props?.rangeW, type, props?.rangeM]);
  return (
    <Box className="flex items-center bg-white py-1 rounded-[8px]">
      <DatePickerInput
        // className={cn(classes.date)}
        // disabled={isDisable}
        firstDayOfWeek={1}
        maxDate={new Date()}
        variant="unstyled"
        className={`items-center rounded-md font-normal py-1 ${
          type == "day" ? " w-[140px] " : " w-[220px] "
        } ${classes.select} ${className}`}
        icon={<IconCalendar size="1.1rem" stroke={1.5} />}
        valueFormat="DD/MM/YYYY"
        value={type === "day" ? value : rangeVal}
        type={type === "day" ? "default" : "range"}
        mx="auto"
        placeholder={type == "day" ? "Chọn ngày" : "Từ - đến"}
        monthsListFormat="[Tháng] MM"
        yearsListFormat="[Năm] YYYY"
        yearLabelFormat="[Năm] YYYY"
        withCellSpacing={false}
        // @ts-ignore
        getDayProps={
          type === "week"
            ? (date: Date) => {
                const isHovered = isInTimeRange(date, hovered, type);
                const isSelected = isInTimeRange(date, value, type);
                const isInRange = isHovered || isSelected;

                return {
                  onMouseEnter: () => setHovered(date),
                  onMouseLeave: () => setHovered(null),
                  inRange: isInRange,
                  firstInRange: isInRange && date.getDay() === 1,
                  lastInRange: isInRange && date.getDay() === 0,
                  selected: isSelected,
                  onClick: () => {
                    setValue(date);
                    setRangeVal([
                      getDateForType(type, date, "first"),
                      getDateForType(type, date, "last"),
                    ]);
                  },
                };
              }
            : (date: Date) => {
                return {
                  onClick: () => {
                    setValue(date);
                  },
                };
              }
        }
        // @ts-ignore
        getMonthControlProps={
          type === "month"
            ? (date: Date) => {
                return {
                  onClick: () => {
                    setValue(date);
                    setRangeVal([
                      getDateForType(type, date, "first"),
                      getDateForType(type, date, "last"),
                    ]);
                  },
                };
              }
            : undefined
        }
      />
    </Box>
  );
});
DatePickerCustom.displayName = "SelectSearch";
export default DatePickerCustom;

const useStyled = createStyles(() => ({
  select: {
    "& input": {
      borderRadius: rem(8),
      border: 0,
      height: rem(44),
    },
    "& .mantine-Input-rightSection": {
      paddingRight: rem(8),
    },
  },
}));
