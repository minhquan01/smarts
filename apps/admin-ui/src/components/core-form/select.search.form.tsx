import { forwardRef, SelectHTMLAttributes } from "react";

import { createStyles, rem, Select, SelectProps } from "@mantine/core";

export interface SelectSearchProps
  extends SelectHTMLAttributes<HTMLSelectElement> {}

const SelectSearchForm = forwardRef<
  HTMLSelectElement,
  SelectProps & SelectSearchProps
>(({ className, name, ...props }, ref) => {
  const { classes } = useStyled();

  return (
    <Select
      name={name}
      className={`${classes.select} ${className}`}
      // @ts-ignore
      ref={ref}
      clearable={true}
      size="md"
      radius="8px"
      searchable
      placeholder="Nhập"
      filter={(value, item) => {
        return (
          item.label
            ?.toLocaleLowerCase()
            ?.includes(value.toLowerCase().trim()) ||
          item?.value?.includes(value.toLowerCase().trim())
        );
      }}
      {...props}
    />
  );
});
SelectSearchForm.displayName = "SelectSearchForm";
export default SelectSearchForm;

const useStyled = createStyles(() => ({
  select: {
    "& input": {
      backgroundColor: "white",
      borderColor: "white",
    },
    "& .mantine-Input-rightSection": {
      paddingRight: rem(8),
    },
  },
}));
