import { Box } from "@mantine/core";

interface IProps {
  label: string;
  placeholder: string;
  type: string;
  required: boolean;
  error: string;
  value: string;
  setValue: () => {};
}
function Input({
  label,
  placeholder,
  type,
  required,
  error,
  value,
  setValue,
}: IProps) {
  return <Box></Box>;
}

export default Input;
