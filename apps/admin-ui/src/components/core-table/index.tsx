// import type { ColumnDef, RowData } from '@tanstack/react-table';
import {
  Box,
  LoadingOverlay,
  LoadingOverlayProps,
  ScrollArea,
  Table,
  TableProps,
  Text,
  createStyles,
} from "@mantine/core";
import {
  ColumnPinningState,
  ExpandedState,
  RowSelectionState,
  SortingState,
  TableOptions,
  VisibilityState,
  flexRender,
  getCoreRowModel,
  getExpandedRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import { Dispatch, SetStateAction, useState } from "react";

import { IProColumns } from "./type-table";
import { IconLayoutCards } from "@tabler/icons-react";

// declare module '@tanstack/table-core' {
//   interface ColumnMeta<TData extends RowData> {
//     title?: string;
//     info?: string;
//     algin?: AlignSetting;
//     type?: 'text' | 'date' | 'select' | 'img' | 'input';
//     editable?: boolean | ((row: TData) => boolean);
//     hidden?: boolean | ((row: TData) => boolean);
//     required?: boolean;
//     updateData?: (rowIndex: number, columnId: string, value: unknown) => void;
//   }
// }

export interface CoreTableProps<T> extends TableProps {
  loading: boolean;
  data?: T[];
  columns: IProColumns<T>;
  rowSelection?: RowSelectionState;
  setRowSelection?: Dispatch<SetStateAction<RowSelectionState>>;
  expanded?: ExpandedState;
  setExpanded?: Dispatch<SetStateAction<ExpandedState>>;
  columnVisibility?: VisibilityState;
  setColumnVisibility?: Dispatch<SetStateAction<VisibilityState>>;
  columnPinning?: ColumnPinningState;
  setColumnPinning?: Dispatch<SetStateAction<ColumnPinningState>>;
  sorting?: SortingState;
  setSorting?: Dispatch<SetStateAction<SortingState>>;
  bodyHeight?: number;
  isGroup?: boolean;
  isFooter?: boolean;
  loadingOverlayProps?: LoadingOverlayProps;
  tableOptions?: TableOptions<T>;
  onAction?: () => void;
}

export const CoreTable = <T extends object>(props: CoreTableProps<T>) => {
  const {
    loading,
    data = [],
    columns,
    className,
    isFooter,
    isGroup,
    rowSelection = {},
    setRowSelection,
    columnVisibility = {},
    setColumnVisibility,
    expanded = {},
    columnPinning = {},
    setColumnPinning,
    sorting,
    setSorting,
    setExpanded,
    bodyHeight = 0,
    loadingOverlayProps = {},
    tableOptions,
    onAction,
    ...rest
  } = props;

  const [scrolled, setScrolled] = useState<boolean>(false);
  const { classes, cx } = useStyles();

  const { getHeaderGroups, getRowModel, getFooterGroups } = useReactTable({
    data,
    columns,
    state: { rowSelection, expanded, columnVisibility, columnPinning, sorting },
    // @ts-ignore
    getSubRows: (row) => row.subRows,
    enableRowSelection: Boolean(rowSelection),
    enablePinning: Boolean(columnPinning),
    enableExpanding: Boolean(expanded),
    onRowSelectionChange: setRowSelection,
    onExpandedChange: setExpanded,
    onColumnVisibilityChange: setColumnVisibility,
    onColumnPinningChange: setColumnPinning,
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getExpandedRowModel: getExpandedRowModel(),
    ...tableOptions,
  });

  return (
    <ScrollArea
      sx={{ position: "relative" }}
      // style={{
      //   maxHeight: `calc(100vh - ${bodyHeight}px)`,
      // }}
      // className="overflow-hidden"
      // h={bodyHeight}
      // onScrollPositionChange={({ y }) => setScrolled(y !== 0)}
      {...loadingOverlayProps}
    >
      <LoadingOverlay visible={loading} />

      <Table
        className={cx(className, classes.table)}
        horizontalSpacing="md"
        style={{
          height: "500px",
        }}
        verticalSpacing="xs"
        highlightOnHover
        withColumnBorders
        // withBorder
        {...rest}
      >
        <thead
          className={` ${cx({
            [classes.scroll]: scrolled,
          })}`}
        >
          {getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => {
                // const { algin } = header.column.columnDef.meta || {};
                const width = header.getSize();
                const maxWidth = header.column.columnDef.maxSize;
                const minWidth = header.column.columnDef.minSize;

                return (
                  <th
                    key={header.id}
                    style={{
                      width,
                      maxWidth,
                      minWidth,
                      textAlign: "center",
                      color: "#fff",
                    }}
                  >
                    <div
                      {...{
                        className: header.column.getCanSort()
                          ? "cursor-pointer select-none"
                          : "",
                        onClick: header.column.getToggleSortingHandler(),
                      }}
                      className="flex flex-row justify-center"
                    >
                      <Text>
                        {header.isPlaceholder
                          ? null
                          : flexRender(
                              header.column.columnDef.header,
                              header.getContext()
                            )}
                        {{
                          asc: " 🔼",
                          desc: " 🔽",
                        }[header.column.getIsSorted() as string] ?? null}
                      </Text>
                    </div>
                  </th>
                );
              })}
            </tr>
          ))}
        </thead>

        <tbody className="h-full overflow-y-auto">
          {getRowModel().rows.map((row) => {
            return (
              <tr
                key={row.id}
                className={row.getCanExpand() ? "expand" : ""}
                tabIndex={-1}
              >
                {row.getVisibleCells().map((cell) => {
                  const width = cell.column.columnDef.size;
                  const maxWidth = cell.column.columnDef.maxSize;
                  const minWidth = cell.column.columnDef.minSize;

                  return (
                    <td
                      key={cell.id}
                      className="capitalize"
                      style={{
                        width,
                        maxWidth,
                        minWidth,
                        textAlign: "center",
                      }}
                    >
                      <Box>
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )}
                      </Box>
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>

        {isFooter && (
          <tfoot>
            {getFooterGroups().map((footerGroup) => (
              <tr key={footerGroup.id}>
                {footerGroup.headers.map((footer) => {
                  const width = footer.getSize();
                  const maxWidth = footer.column.columnDef.maxSize;
                  const minWidth = footer.column.columnDef.minSize;

                  return (
                    <th
                      key={footer.id}
                      style={{ width, maxWidth, minWidth }}
                      colSpan={footer.colSpan}
                    >
                      <Text>
                        {footer.isPlaceholder
                          ? null
                          : flexRender(
                              footer.column.columnDef.footer,
                              footer.getContext()
                            )}
                      </Text>
                    </th>
                  );
                })}
              </tr>
            ))}
          </tfoot>
        )}
      </Table>
      {!loading && data?.length === 0 && (
        <Box className="mt-[10%] flex flex-col items-center justify-center">
          <IconLayoutCards />
          <Text className="mt-2 text-center text-[14px] font-semibold">
            Empty data
          </Text>
        </Box>
      )}
    </ScrollArea>
  );
};

export default CoreTable;

const useStyles = createStyles((theme) => ({
  table: {
    display: "block",
    overflowX: "auto",
    // overflowY: "auto",
    whiteSpace: "nowrap",
    "& thead": {
      position: "sticky",
      top: 0,
      transition: "box-shadow 150ms ease",
      zIndex: 10,
      width: "auto",
      background:
        "var(--gradient-2, linear-gradient(90deg, #00B4EC 0%, #B5C876 100%))",

      "&::after": {
        content: '""',
        position: "absolute",
        left: 0,
        right: 0,
        bottom: 0,
      },

      "& th:nth-of-type(1)": {
        borderTopLeftRadius: "8px",
      },
      "& th:last-of-type": {
        borderTopRightRadius: "8px",
      },
    },
    "& tbody": {
      background: "#fff",
      "& tr:last-of-type": {
        "& td:last-of-type": {
          borderBottomRightRadius: "8px",
        },
        "& td:first-of-type": {
          borderBottomLeftRadius: "8px",
        },
      },
    },

    "& tfoot": {
      position: "sticky",
      bottom: 0,
      transition: "box-shadow 150ms ease",
      zIndex: 10,

      "&::after": {
        content: '""',
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
      },
    },
  },
  scroll: {
    boxShadow: theme.shadows.sm,
  },
}));
