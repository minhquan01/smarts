import { SelectItem } from '@mantine/core';

import type { ColumnDef } from '@tanstack/react-table';

export type IProColumns<D> = ColumnDef<D, string>[];

export interface IPaginationProps {
  total?: number;
  pageSize: number;
  rowsPerPageOptions?: (string | SelectItem)[];
  setPage?: (page: number, size: number) => void;
}

export type IHeadCell<T> = {
  [key in keyof T]?: string;
} & {
  id?: string;
  actions?: string;
  select?: string;
  [key: string]: unknown;
};
