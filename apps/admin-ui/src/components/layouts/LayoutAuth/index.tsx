import { SmartsLogoAuth } from "@admin-ui/assets";
import { Box } from "@mantine/core";
import React from "react";
import { ReactNode } from "react";

interface LayoutAuthProps {
  children: ReactNode;
}

const LayoutAuth = ({ children }: LayoutAuthProps) => {
  return (
    <div
      style={{
        backgroundImage: "linear-gradient(155deg, #6FDDFF 0%, #FFCF44 160%)",
      }}
      className="w-full min-h-screen flex justify-center"
    >
      <Box className="w-[90%] mx-auto flex flex-col items-center">
        <img className="my-16" src={SmartsLogoAuth.src} alt="" />
        <Box
          miw={744}
          style={{ background: "rgba(255, 255, 255, 0.50)" }}
          className="w-fit rounded-xl p-8 "
        >
          {children}
        </Box>
      </Box>
    </div>
  );
};

export default LayoutAuth;
