import {
  IconBook,
  IconExtend,
  IconGenerality,
  IconList,
  IconMenuBoard,
  IconMortarBoard,
  IconReport,
  IconSetting,
  IconStickyNote,
  IconUserCheck,
  Logo,
  LogoSmart,
} from "@admin-ui/assets";
import React, { useMemo } from "react";

import { Box } from "@mantine/core";
import Link from "next/link";
import { useRouter } from "next/router";
import Person from "../Header/Person";
import Manage from "./Manage";

export const menuSide = [
  {
    name: "Tổng quan",
    icon: <IconGenerality />,
    path: "/home",
  },
  {
    name: "Báo cáo",
    icon: <IconReport />,
    path: "/report",
  },
  {
    name: "Danh sách lớp học",
    icon: <IconList />,
    path: "/list",
  },
  {
    name: "Học sinh",
    icon: <IconMortarBoard />,
    path: "/student",
  },
  {
    name: "Thời khóa biểu",
    icon: <IconMenuBoard />,
    path: "/timetable",
  },
  {
    name: "Chuyên cần",
    icon: <IconStickyNote />,
    path: "/attendance",
  },
  {
    name: "Sổ liên lạc",
    icon: <IconBook />,
    path: "/contact-book",
  },
  {
    name: "Giáo viên",
    icon: <IconUserCheck />,
    path: "/teacher",
  },
];

const manage = [
  {
    title: "Quản lý hệ thống",
    icon: <IconSetting />,
    children: [
      { name: "Thông tin trường học", path: "/home" },
      { name: "Quản lý danh mục", path: "/1" },
      { name: "Quản lý nhóm phân quyền", path: "/2" },
      { name: "Quản lý người dùng", path: "/3" },
    ],
  },
];

interface SideBarProps {
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  isOpenMobile: boolean;
  setIsOpenMobile: React.Dispatch<React.SetStateAction<boolean>>;
}

const SideBar = ({
  isOpenMobile,
  isOpen,
  setIsOpen,
  setIsOpenMobile,
}: SideBarProps) => {
  const router = useRouter();

  const pathSelect = useMemo(() => {
    return router.pathname;
  }, [router.pathname]);
  return (
    <Box
      className={`fixed z-[1000] ${
        isOpenMobile ? "max-md:translate-x-0" : "max-md:-translate-x-[264px]"
      } transition-all duration-500`}
    >
      <Box
        className={`select-none bg-[#F9F9F9] h-screen ${
          isOpen ? "w-[264px] pl-6" : "w-20"
        } pt-7 transition-all duration-150 scroll-sidebar overflow-y-auto overflow-x-hidden`}
      >
        <Box
          onClick={() => setIsOpen((prev) => !prev)}
          className={`cursor-pointer absolute z-[1000] hidden md:block -right-3 top-9 ${
            !isOpen && "scale-[-1]"
          }`}
        >
          <IconExtend />
        </Box>
        <Box className={`${!isOpen && "flex items-center justify-center"} `}>
          <img src={isOpen ? LogoSmart.src : Logo.src} alt="" />
        </Box>
        {/* Mennu Side */}
        <Box
          className={`mt-10 flex-col flex gap-y-4 ${!isOpen && "items-center"}`}
        >
          {menuSide.map((item, idx) => (
            <Link
              className="relative flex items-center gap-x-3 py-2"
              key={idx}
              href={item.path}
            >
              <Box
                className={`${
                  pathSelect === item.path
                    ? "text-textSelect"
                    : "text-textPrimary"
                }`}
              >
                {item.icon}
              </Box>
              <p
                className={`text-base font-semibold ${!isOpen && "hidden"} ${
                  pathSelect === item.path ? "text-select" : "text-textPrimary "
                }`}
              >
                {item.name}
              </p>
              {pathSelect === item.path && isOpen && (
                <Box className="absolute right-0 top-0 bottom-0 my-auto rounded-l-[2px] w-1 h-6 bg-[#FEF834]" />
              )}
            </Link>
          ))}
        </Box>
        {/* Manage */}
        <Box className={`mt-28 space-y-4 ${isOpen && "pr-6"}`}>
          {manage.map((item, idx) => (
            <Manage isOpen={isOpen} data={item} key={idx} />
          ))}
        </Box>
        <Box className="md:hidden max-md:my-12 ">
          <Person />
        </Box>
      </Box>
    </Box>
  );
};

export default SideBar;
