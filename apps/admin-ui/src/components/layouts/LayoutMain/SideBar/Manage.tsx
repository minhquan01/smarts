import { IconArrowUp } from '@admin-ui/assets';
import { Box } from '@mantine/core';
import { useRouter } from 'next/router';
import React from 'react';
import { useState } from 'react';
import { useMemo } from 'react';

interface ManageProps {
  data: {
    title: string;
    icon: React.JSX.Element;
    children: {
      name: string;
      path: string;
    }[];
  };
  isOpen: boolean;
}

const Manage = ({ data, isOpen }: ManageProps) => {
  const router = useRouter();
  const [isExtend, setIsExtend] = useState(false);

  const pathSelect = useMemo(() => {
    return router.pathname;
  }, [router.pathname]);
  return (
    <Box>
      <Box
        onClick={() => setIsExtend((prev) => !prev)}
        className={`cursor-pointer select-none flex items-center w-full  ${
          isOpen ? 'justify-between ' : ' justify-center'
        }`}
      >
        <Box className={`flex items-center ${isOpen ? ' gap-x-3' : ''}`}>
          {data.icon}
          <p
            className={`text-textPrimary font-semibold ${!isOpen && 'hidden'}`}
          >
            {data.title}
          </p>
        </Box>
        <Box
          className={`${isExtend ? 'rotate-180' : 'rotate-0'} ${
            !isOpen && 'hidden'
          } transition-all duration-200`}
        >
          {' '}
          <IconArrowUp />
        </Box>
      </Box>
      <Box
        className={` ${
          isExtend ? 'max-h-[1000px]' : 'max-h-0'
        } transition-all duration-200 overflow-hidden`}
      >
        <Box
          className={`p-3 bg-white rounded-lg flex flex-col gap-3 ${
            isOpen ? 'mt-2 ' : 'w-fit items-center mx-auto mt-4 '
          } `}
        >
          {data.children.map((child, index) => (
            <Box
              className={`flex items-center ${
                isOpen ? 'gap-x-2' : 'justify-center'
              }`}
              key={index}
            >
              <Box
                className={`w-2 h-2 rounded-full ${
                  pathSelect !== child.path
                    ? 'bg-[#292D3280]/50'
                    : 'bg-textSelect'
                }`}
              />
              <p
                className={`text-sm font-medium ${!isOpen && 'hidden'} ${
                  pathSelect === child.path ? 'text-select' : 'text-textPrimary'
                }`}
              >
                {child.name}
              </p>
            </Box>
          ))}
        </Box>
      </Box>
    </Box>
  );
};

export default Manage;
