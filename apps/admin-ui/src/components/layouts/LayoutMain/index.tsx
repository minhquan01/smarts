import { Box } from "@mantine/core";
import { ReactNode, useState } from "react";
import Header from "./Header";
import SideBar from "./SideBar";

interface LayoutMainProps {
  children: ReactNode;
}

const LayoutMain = ({ children }: LayoutMainProps) => {
  const [isOpen, setIsOpen] = useState(true);
  const [isOpenMobile, setIsOpenMobile] = useState(false);
  return (
    <Box className="flex items-start">
      <SideBar
        setIsOpen={setIsOpen}
        isOpen={isOpen}
        setIsOpenMobile={setIsOpenMobile}
        isOpenMobile={isOpenMobile}
      />
      <Box
        className={`${
          isOpen ? "w-[264px]" : "w-20"
        } transition-all h-full duration-150 bg-transparent`}
      />
      {/* <Box
        onClick={() => setIsOpenMobile(false)}
        className={`max-md:bg-black/5 fixed inset-0 z-50 ${
          isOpenMobile ? '' : 'opacity-0 -z-50'
        }`}
      /> */}

      <Box className="min-h-screen flex-1 flex flex-col">
        <Header setIsOpenMobile={setIsOpenMobile} />
        <Box className="flex flex-1">
          <Box className={`bg-[#F0F4F7] min-h-full flex-1`}>
            <Box className="w-mobile md:w-main mx-auto h-full pt-9">
              {children}
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default LayoutMain;
