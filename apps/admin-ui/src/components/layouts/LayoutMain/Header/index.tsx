import { IconArrowDown, IconBell, IconMenu } from "@admin-ui/assets";
import { Box } from "@mantine/core";
import React from "react";
import Person from "./Person";

interface HeaderProps {
  setIsOpenMobile: React.Dispatch<React.SetStateAction<boolean>>;
}

const Header = ({ setIsOpenMobile }: HeaderProps) => {
  return (
    <Box className="select-none h-20 bg-white w-full flex-shrink-0">
      <Box className="w-mobile md:w-main mx-auto flex items-center h-full justify-between">
        <Box>
          <Box
            onClick={() => setIsOpenMobile(true)}
            className="block md:hidden p-1"
          >
            <IconMenu />
          </Box>
        </Box>
        <Box className="flex items-center gap-x-4 md:gap-x-8 max-md:flex-row-reverse">
          <Box className="flex items-center gap-x-3">
            <p className="text-textPrimary font-semibold">Năm học</p>
            <Box className="p-2 rounded-lg flex items-center gap-4 bg-bgPri">
              <p className="text-sm font-semibold text-textPrimary">
                2022-2023
              </p>
              <Box className="scale-90">
                <IconArrowDown />
              </Box>
            </Box>
          </Box>
          <Box className="flex items-center gap-x-4">
            <Box className="relative">
              <IconBell />
              <Box className="absolute -top-0.5 text-[11px] font-medium text-white tracking-[0.5px] right-0 w-3 h-3 rounded-full flex items-center justify-center bg-[#FF3B30]">
                3
              </Box>
            </Box>
            <Box className="hidden md:block">
              <Person />
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Header;
