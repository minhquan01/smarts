import { IconArrowDown } from "@admin-ui/assets";
import { Box } from "@mantine/core";
import React from "react";

const Person = () => {
  return (
    <Box className="flex items-center gap-x-3">
      <Box
        style={{
          background:
            "var(--gradient-2, linear-gradient(180deg, #00B4EC 0%, #B5C876 100%))",
        }}
        className="w-9 h-9 rounded-[10px] flex items-center justify-center"
      >
        <Box className="bg-white w-[32px] h-[32px] rounded-lg flex items-center justify-center">
          <img
            className="w-[28px] h-[28px] rounded-md object-cover"
            src="https://images.unsplash.com/photo-1600984575359-310ae7b6bdf2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1964&q=80"
            alt=""
          />
        </Box>
      </Box>
      <p className="text-textPrimary font-medium">Nguyễn Vân Anh</p>
      <IconArrowDown />
    </Box>
  );
};

export default Person;
