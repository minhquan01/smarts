import { useRouter } from "next/router";
import React, { ReactNode } from "react";
import LayoutAuth from "./LayoutAuth";
import LayoutMain from "./LayoutMain";

interface LayoutProps {
  children: ReactNode;
}

const authPath = ["/login", "/register", "/forgot-password"];

const Layout = ({ children }: LayoutProps) => {
  const router = useRouter();

  return (
    <>
      {authPath.includes(router.pathname) ? (
        <LayoutAuth>{children}</LayoutAuth>
      ) : (
        <LayoutMain>{children}</LayoutMain>
      )}
    </>
  );
};

export default Layout;
