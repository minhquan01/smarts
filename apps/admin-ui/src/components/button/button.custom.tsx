import { ButtonHTMLAttributes, forwardRef } from "react";
import { Button, ButtonProps, createStyles, rem } from "@mantine/core";

export interface ButtonCustomProps
  extends ButtonHTMLAttributes<HTMLButtonElement> {
  path?: "button" | "search" | "icon" | "border" | "hover" | "primary";
}

const ButtonCustom = forwardRef<
  HTMLButtonElement,
  ButtonCustomProps & ButtonProps
>(({ className, path = "primary", ...props }, ref) => {
  const { classes } = useStyled();
  return (
    <Button
      className={`${classes.button} ${classes[path]} ${className}`}
      ref={ref}
      {...props}
    />
  );
});
ButtonCustom.displayName = "ButtonCustom";
export default ButtonCustom;

const useStyled = createStyles((theme) => ({
  button: {
    boxShadow: "0px 4px 20px rgba(0,0,0, 0.05)",
    borderRadius: rem(8),
    "&:focus, &:focus-visible": {
      outline: 0,
    },
    "& .mantine-Button-label": {
      fontSize: rem(16),
      fontWeight: 500,
    },
  },
  primary: {
    backgroundColor: "#00B4EC !important",
    padding: "0 36px",
    height: rem(40),
    "&:hover": {
      backgroundColor: "#fff !important",
      color: "#00B4EC !important",
    },
    "& .mantine-Button-label": {
      fontSize: rem(18),
      fontWeight: 600,
    },
  },
  search: {
    backgroundColor: "#fff !important",
    color: "#292D32",
    padding: "12px 16px",
    height: "fit-content",
    "&:hover": {
      backgroundColor: "#d0ebffa6 !important",
      color: "#0F45FF",
    },
    "&:disabled": {
      backgroundColor: "#0F45FF",
      color: "#FFFFFF",
      opacity: 0.2,
    },
  },
  border: {
    border: "2px solid #0F45FF",
    color: "#0F45FF",
    backgroundColor: "#FFFFFF",
    "&:hover": {
      backgroundColor: "#d0ebffa6",
    },
  },
  icon: {
    backgroundColor: "#FFFFFF",
    "&:hover": {
      backgroundColor: "#d0ebffa6",
    },
  },
  hover: {
    border: "2px solid #0F45FF",
    color: "#0F45FF",
    backgroundColor: "#FFFFFF",
    "&:hover": {
      background: "#0F45FF",
      color: "#FFFFFF",
    },
  },
}));
