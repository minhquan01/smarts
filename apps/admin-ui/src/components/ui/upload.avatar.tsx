import { IconCamera, User } from "@admin-ui/assets";
import React from "react";

interface UploadAvatarProps {
  name: string;
}

const UploadAvatar = ({ name }: UploadAvatarProps) => {
  return (
    <label htmlFor={name} className="cursor-pointer">
      <div
        className="w-40 relative h-40 rounded-full flex items-center justify-center"
        style={{
          background: "linear-gradient(90deg, #00B4EC 0%, #FEF834 100%)",
        }}
      >
        <div className="w-36 h-36 rounded-full bg-white flex items-center justify-center">
          <img src={User.src} alt="" />
        </div>
        <div className="absolute bg-white bottom-1 right-1 w-[50px] h-[50px] rounded-full flex items-center justify-center">
          <div className="bg-[#F0F4F7] w-11 h-11 rounded-full flex items-center justify-center">
            <IconCamera />
          </div>
        </div>
      </div>
    </label>
  );
};

export default UploadAvatar;
