import { IconGallery } from "@admin-ui/assets";
import React from "react";

interface UploadCoverProps {
  name: string;
}

const UploadCover = ({ name }: UploadCoverProps) => {
  return (
    <label htmlFor={name} className="cursor-pointer w-full h-full">
      <div className="bg-white rounded-xl text-[#949698] w-full h-full flex items-center justify-center gap-4">
        <IconGallery />
        <p className="text-xl font-semibold">Thêm ảnh bìa</p>
      </div>
    </label>
  );
};

export default UploadCover;
