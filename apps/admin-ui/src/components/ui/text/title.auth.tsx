import React from "react";

interface TitleAuthProps {
  title: string;
  desc?: string;
}

const TitleAuth = ({ title, desc }: TitleAuthProps) => {
  return (
    <div className="text-[#292D32] space-y-2 mb-6">
      <h1 className=" font-bold text-2xl sm:text-[28px] ">{title}</h1>
      {desc && <p className="opacity-50">{desc}</p>}
    </div>
  );
};

export default TitleAuth;
