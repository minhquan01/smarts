/** @type {import('next').NextConfig} */
const withTM = require("next-transpile-modules")([
  "@hola/ui-core",
  "@hola/ui-admin",
]);
const nextConfig = {
  reactStrictMode: true,
};

module.exports = withTM(nextConfig);
