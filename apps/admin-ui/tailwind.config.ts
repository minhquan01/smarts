import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/containers/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        textPrimary: "#292D32",
        textSelect: "#00B4EC",
        bgPri: "#F0F4F7",
      },
      width: {
        main: "1128px",
        mobile: "90%",
      },
    },
  },
  plugins: [],
};
export default config;
