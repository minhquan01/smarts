import {
  GetListParams,
  useCreate,
  useDelete,
  useGetList,
  useGetOne,
  useUpdate,
} from "@hola/ui-core";
import { useState } from "react";
interface ResourceProps {
  resource: string;
}

export const useResource = (
  props: ResourceProps
): {
  params: Partial<GetListParams>;
  setParames: React.Dispatch<React.SetStateAction<Partial<GetListParams>>>;
  list: any[];
  // detail: any;
  refetch: () => void;
  deleteOne: (resource: string, params: any) => void;
  create: (resource: string, params: any) => void;
  update: (resource: string, params: any) => void;
  openAddModal: boolean;
  setOpenAddModal: React.Dispatch<React.SetStateAction<boolean>>;
  openEditModal: boolean;
  setOpenEditModal: React.Dispatch<React.SetStateAction<boolean>>;
  idSelected: string | undefined | null;
  setIdSelected: React.Dispatch<
    React.SetStateAction<string | undefined | null>
  >;
} => {
  const [params, setParames] = useState<Partial<GetListParams>>({});
  const { data: list = [], refetch } = useGetList(props.resource, params, {});
  const [deleteOne] = useDelete(props.resource);
  const [create] = useCreate(props.resource);
  const [update] = useUpdate(props.resource);
  const [openAddModal, setOpenAddModal] = useState(false);
  const [openEditModal, setOpenEditModal] = useState(false);
  const [idSelected, setIdSelected] = useState<string | null | undefined>(null);

  // const { data: detail, refetch: refetchEdit } = useGetOne(
  //   props.resource,
  //   {
  //     id: idSelected,
  //   },
  //   {fe}
  // );

  return {
    params,
    setParames,
    list: list,
    // detail,
    refetch,
    deleteOne,
    create,
    update,
    openAddModal,
    setOpenAddModal,
    openEditModal,
    setOpenEditModal,
    idSelected,
    setIdSelected,
  };
};
