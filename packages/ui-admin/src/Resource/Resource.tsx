import { useCreate, useDelete, useGetList, useUpdate } from "@hola/ui-core";
interface ResourceProps {
  resource: string;
  children: React.ReactNode;
  list: React.ReactNode;
  create: React.ReactNode;
  update: React.ReactNode;
  delete: React.ReactNode;
  filter: React.ReactNode;
}

export const Resource = (props: ResourceProps) => {
  const { data: dataStudent, refetch } = useGetList(props.resource, {}, {});
  const [deleteOne] = useDelete(props.resource);
  const [create] = useCreate(props.resource);
  const [update] = useUpdate(props.resource);

  return <>{props.children}</>;
};
