import { BaseFC } from "@hola/types";
import { AuthProvider } from "./contexts";

export const MainProvider = ({ children }: BaseFC) => {
  return <AuthProvider>{children}</AuthProvider>;
};
