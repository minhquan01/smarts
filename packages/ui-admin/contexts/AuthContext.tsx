import { BaseFC, IAuthContext } from "@hola/types";
import { createContext, useContext } from "react";

export const AuthContext = createContext<IAuthContext>({
  token: "",
});

export const useAuthContext = () => {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error("useCartContext must be used within CartProvider");
  }

  return context;
};

export const AuthProvider = ({ children }: BaseFC) => {
  return (
    <AuthContext.Provider value={{ token: "test" }}>
      {children}
    </AuthContext.Provider>
  );
};
