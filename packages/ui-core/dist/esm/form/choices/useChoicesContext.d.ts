import { RaRecord } from '../../types';
import { ChoicesContextValue } from './ChoicesContext';
export declare const useChoicesContext: <ChoicesType extends RaRecord<import("../../types").Identifier> = RaRecord<import("../../types").Identifier>>(options?: Partial<ChoicesContextValue> & {
    choices?: ChoicesType[];
}) => ChoicesContextValue;
//# sourceMappingURL=useChoicesContext.d.ts.map