import * as React from 'react';
import { ValidationErrorMessage } from './validate';
export interface ValidationErrorProps {
    error: ValidationErrorMessage;
}
declare const ValidationError: (props: ValidationErrorProps) => React.JSX.Element;
export default ValidationError;
//# sourceMappingURL=ValidationError.d.ts.map