import * as React from 'react';
import { isValidElement } from 'react';
import { Route, Routes } from 'react-router-dom';
import { isValidElementType } from 'react-is';
import { ResourceContextProvider } from './ResourceContextProvider';
export var Resource = function (props) {
    var create = props.create, edit = props.edit, list = props.list, name = props.name, show = props.show;
    return (React.createElement(ResourceContextProvider, { value: name },
        React.createElement(Routes, null,
            create && (React.createElement(Route, { path: "create/*", element: getElement(create) })),
            show && React.createElement(Route, { path: ":id/show/*", element: getElement(show) }),
            edit && React.createElement(Route, { path: ":id/*", element: getElement(edit) }),
            list && React.createElement(Route, { path: "/*", element: getElement(list) }),
            props.children)));
};
var getElement = function (ElementOrComponent) {
    if (isValidElement(ElementOrComponent)) {
        return ElementOrComponent;
    }
    if (isValidElementType(ElementOrComponent)) {
        // @ts-ignore
        return React.createElement(ElementOrComponent, null);
    }
    return null;
};
Resource.raName = 'Resource';
Resource.registerResource = function (_a) {
    var create = _a.create, edit = _a.edit, icon = _a.icon, list = _a.list, name = _a.name, options = _a.options, show = _a.show, recordRepresentation = _a.recordRepresentation, hasCreate = _a.hasCreate, hasEdit = _a.hasEdit, hasShow = _a.hasShow;
    return ({
        name: name,
        options: options,
        hasList: !!list,
        hasCreate: !!create || !!hasCreate,
        hasEdit: !!edit || !!hasEdit,
        hasShow: !!show || !!hasShow,
        icon: icon,
        recordRepresentation: recordRepresentation,
    });
};
//# sourceMappingURL=Resource.js.map