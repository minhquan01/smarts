import * as React from "react";
import { useMemo } from "react";
import { QueryClientProvider, QueryClient } from "react-query";
import { AuthContext, convertLegacyAuthProvider } from "../auth";
import { DataProviderContext, convertLegacyDataProvider, defaultDataProvider, } from "../dataProvider";
import { memoryStore } from "../store";
var defaultStore = memoryStore();
export var CoreAdminContext = function (props) {
    var authProvider = props.authProvider, basename = props.basename, _a = props.dataProvider, dataProvider = _a === void 0 ? defaultDataProvider : _a, i18nProvider = props.i18nProvider, _b = props.store, store = _b === void 0 ? defaultStore : _b, children = props.children, history = props.history, queryClient = props.queryClient, loginPage = props.loginPage;
    if (!dataProvider) {
        throw new Error("Missing dataProvider prop.\nReact-admin requires a valid dataProvider function to work.");
    }
    var finalQueryClient = useMemo(function () { return queryClient || new QueryClient(); }, [queryClient]);
    console.log({ authProvider: authProvider });
    var finalAuthProvider = useMemo(function () {
        return authProvider instanceof Function
            ? convertLegacyAuthProvider(authProvider)
            : authProvider;
    }, [authProvider]);
    console.log({ finalAuthProvider: finalAuthProvider });
    var finalDataProvider = useMemo(function () {
        return dataProvider instanceof Function
            ? convertLegacyDataProvider(dataProvider)
            : dataProvider;
    }, [dataProvider]);
    return (React.createElement(AuthContext.Provider, { value: finalAuthProvider },
        React.createElement(DataProviderContext.Provider, { value: finalDataProvider },
            React.createElement(QueryClientProvider, { client: finalQueryClient }, children))));
};
//# sourceMappingURL=CoreAdminContext.js.map