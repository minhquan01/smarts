import * as React from 'react';
import { ReactNode } from 'react';
import { CreateControllerProps } from './useCreateController';
import { Identifier, RaRecord } from '../../types';
/**
 * Call useCreateController and put the value in a CreateContext
 *
 * Base class for <Create> components, without UI.
 *
 * Accepts any props accepted by useCreateController:
 * - id: The record identifier
 * - resource: The resource
 *
 * @example // Custom edit layout
 *
 * const PostCreate = props => (
 *     <CreateBase {...props}>
 *         <Grid container>
 *             <Grid item xs={8}>
 *                 <SimpleForm>
 *                     ...
 *                 </SimpleForm>
 *             </Grid>
 *             <Grid item xs={4}>
 *                 Create instructions...
 *             </Grid>
 *         </Grid>
 *         <div>
 *             Post related links...
 *         </div>
 *     </CreateBase>
 * );
 */
export declare const CreateBase: <RecordType extends Omit<RaRecord<Identifier>, "id"> = any, ResultRecordType extends RaRecord<Identifier> = RecordType & {
    id: Identifier;
}>({ children, ...props }: CreateControllerProps<RecordType, Error, ResultRecordType> & {
    children: ReactNode;
}) => React.JSX.Element;
//# sourceMappingURL=CreateBase.d.ts.map