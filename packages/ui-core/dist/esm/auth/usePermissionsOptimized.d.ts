/**
 * @deprecated use usePermissions instead
 *
 * @see usePermissions
 */
declare const usePermissionsOptimized: (params?: {}) => {
    permissions: any;
    isLoading: boolean;
    error: any;
    refetch: <TPageData>(options?: import("react-query").RefetchOptions & import("react-query").RefetchQueryFilters<TPageData>) => Promise<import("react-query").QueryObserverResult<any, any>>;
};
export default usePermissionsOptimized;
//# sourceMappingURL=usePermissionsOptimized.d.ts.map