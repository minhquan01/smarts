import * as React from 'react';
export declare const LabelPrefixContextProvider: ({ prefix, concatenate, children, }: {
    prefix: any;
    concatenate?: boolean;
    children: any;
}) => React.JSX.Element;
//# sourceMappingURL=LabelPrefixContextProvider.d.ts.map