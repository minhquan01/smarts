import { RaRecord } from '../../types';
/**
 * Get the list of selected items for a resource, and callbacks to change the selection
 *
 * @param resource The resource name, e.g. 'posts'
 *
 * @returns {Object} Destructure as [selectedIds, { select, toggle, clearSelection }].
 */
export declare const useRecordSelection: <RecordType extends RaRecord<import("../../types").Identifier> = any>(resource: string) => [RecordType["id"][], {
    select: (ids: RecordType["id"][]) => void;
    unselect: (ids: RecordType["id"][]) => void;
    toggle: (id: RecordType["id"]) => void;
    clearSelection: () => void;
}];
//# sourceMappingURL=useRecordSelection.d.ts.map