/// <reference types="react" />
export declare const TranslatableContext: import("react").Context<TranslatableContextValue>;
export interface TranslatableContextValue {
    getLabel: GetTranslatableLabel;
    getSource: GetTranslatableSource;
    locales: string[];
    selectedLocale: string;
    selectLocale: SelectTranslatableLocale;
}
export type GetTranslatableSource = (field: string, locale?: string) => string;
export type GetTranslatableLabel = (field: string, label?: string) => string;
export type SelectTranslatableLocale = (locale: string) => void;
//# sourceMappingURL=TranslatableContext.d.ts.map