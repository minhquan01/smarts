"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
// export * from "./src/sum";
var fs = __importStar(require("fs-extra"));
var glob = __importStar(require("glob"));
var remove_decorator_import_comment_1 = require("./remove-decorator-import-comment");
var sourceDirectory = "../../apps/server/src";
var destinationDirectory = "./common/";
// create destination folder if it doesn't exist
if (!fs.existsSync(destinationDirectory)) {
    fs.removeSync(destinationDirectory);
    fs.mkdirSync(destinationDirectory);
}
var allFolders = fs.readdirSync(sourceDirectory).filter(function (file) {
    return fs.statSync("".concat(sourceDirectory, "/").concat(file)).isDirectory();
});
// in each folder, find all files that match the pattern *.ts and copy them to the destination folder
allFolders.forEach(function (folder) {
    var files = glob.sync("".concat(sourceDirectory, "/").concat(folder, "/base/*.ts"));
    files.forEach(function (file) {
        var fileName = file.split("/").pop() || "";
        // if filename match *.ts, copy it to the destination folder
        if (fileName.match(/^[^.]*\.ts$/)) {
            if (!fs.existsSync("".concat(destinationDirectory, "/").concat(folder))) {
                fs.mkdirSync("".concat(destinationDirectory, "/").concat(folder));
            }
            var content = fs.readFileSync(file, "utf8");
            var newContent = (0, remove_decorator_import_comment_1.modifyContent)(content);
            // // create file with the same name in the destination folder
            fs.writeFileSync("".concat(destinationDirectory, "/").concat(folder, "/").concat(fileName), content);
            fs.writeFileSync("".concat(destinationDirectory, "/").concat(folder, "/").concat(fileName, "x"), newContent);
        }
        else {
            console.log("Skipping ".concat(file));
        }
        // create desctination folder if it doesn't exist
        // fs.copyFileSync(file, `${destinationDirectory}/${folder}/${fileName}`);
    });
});
