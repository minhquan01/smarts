import * as fs from "fs";

// const filePath = "./DeleteClassInfoTimetableArgs.ts"; // Replace with the actual file path

// fs.readFile(filePath, "utf8", (err, data) => {
//   if (err) {
//     console.error("Error reading the file:", err);
//     process.exit(1);
//   }

//   // Remove decorator lines
//   const modifiedContent = data
//     .replace(
//       /import(\s+(\*|{).*?(\s+as\s+\w+)?\s+from\s+['"])(?![.])[^'"]+['"];?(\n|\r\n)?/g,
//       ""
//     )
//     .replace(/@ApiProperty\(\{[\s\S]*?}\)(\n|\r\n)/g, "")
//     .replace(/@.*?(\n|\r\n)/g, "")
//     .replace(/\/\/.*(\n|\r\n)?/g, "")
//     .replace(/\/\*[\s\S]*?\*\//g, "");
//   return modifiedContent;
// });

export const modifyContent = (content: string) => {
  let newContent = content
    .replace(
      /import(\s+(\*|{).*?(\s+as\s+\w+)?\s+from\s+['"])(?![.])[^'"]+['"];?(\n|\r\n)?/g,
      ""
    )

    // .replace(/@ApiProperty\(\{[\s\S]*?}\)(\n|\r\n)/g, "")
    .replace(/@Field\(\([\s\S]*?\)\,\s*\{[\s\S]*?\}\)\n/g, "")
    // .replace(/@.*?(\n|\r\n)/g, "")
    .replace(/\/\/.*(\n|\r\n)?/g, "")
    .replace(/\/\*[\s\S]*?\*\//g, "");

  return newContent;
  // return newContent.replace(/@.*?(\n|\r\n)/g, "");
};
