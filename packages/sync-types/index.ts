// export * from "./src/sum";
import * as fs from "fs-extra";
import * as glob from "glob";
import { modifyContent } from "./remove-decorator-import-comment";

const sourceDirectory = "../../apps/server/src";
const destinationDirectory = "./common/";

// create destination folder if it doesn't exist
if (!fs.existsSync(destinationDirectory)) {
  fs.removeSync(destinationDirectory);
  fs.mkdirSync(destinationDirectory);
}

const allFolders = fs.readdirSync(sourceDirectory).filter((file) => {
  return fs.statSync(`${sourceDirectory}/${file}`).isDirectory();
});

// in each folder, find all files that match the pattern *.ts and copy them to the destination folder
allFolders.forEach((folder) => {
  const files = glob.sync(`${sourceDirectory}/${folder}/base/*.ts`);
  files.forEach((file) => {
    const fileName = file.split("/").pop() || "";
    // if filename match *.ts, copy it to the destination folder
    if (fileName.match(/^[^.]*\.ts$/)) {
      if (!fs.existsSync(`${destinationDirectory}/${folder}`)) {
        fs.mkdirSync(`${destinationDirectory}/${folder}`);
      }
      const content = fs.readFileSync(file, "utf8");
      const newContent = modifyContent(content);

      // // create file with the same name in the destination folder
      fs.writeFileSync(
        `${destinationDirectory}/${folder}/${fileName}`,
        content
      );
      fs.writeFileSync(
        `${destinationDirectory}/${folder}/${fileName}x`,
        newContent
      );
    } else {
      console.log(`Skipping ${file}`);
    }

    // create desctination folder if it doesn't exist
    // fs.copyFileSync(file, `${destinationDirectory}/${folder}/${fileName}`);
  });
});
