import { ReactNode } from "react";

export interface BaseFC {
  children: ReactNode;
}
