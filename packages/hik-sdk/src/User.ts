import { Base, BaseConfig } from './Base';
import { Hik } from './Hik';
import {
    CreateUserInfoResponse,
    EmployeeNoList,
    DeleteUserResponse,
    UserInfo,
    UserInfoSearchCond,
    UserInfoSearchResponse,
} from './interfaces/users';
import { v4 as uuidv4 } from 'uuid';

export class User extends Base {
    constructor(config: BaseConfig) {
        super(config);
    }

    public async CreateUser(userInfo: UserInfo): Promise<CreateUserInfoResponse> {
        const resp = await this.request({
            method: 'POST',
            url: '/ISAPI/AccessControl/UserInfo/Record',
            data: {
                UserInfo: userInfo,
            },
        });
        return resp.data;
    }

    public async SearchUsers(condition: UserInfoSearchCond): Promise<UserInfoSearchResponse> {
        const resp = await this.request({
            method: 'POST',
            url: '/ISAPI/AccessControl/UserInfo/Search',
            data: {
                UserInfoSearchCond: {
                    searchID: uuidv4(),
                    ...condition,
                },
            },
        });
        return resp.data;
    }

    public async DeleteUsers(ids: EmployeeNoList[]): Promise<DeleteUserResponse> {
        const resp = await this.request({
            method: 'PUT',
            url: '/ISAPI/AccessControl/UserInfo/Delete',
            data: {
                UserInfoDelCond: {
                    EmployeeNoList: ids,
                },
            },
        });
        return resp.data;
    }
}
