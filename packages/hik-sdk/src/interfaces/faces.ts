export interface FaceDataRecord {
    faceLibType: string;
    FDID: string;
    FPID: string;
    PicFeaturePoints: Array<any>;
    faceUrl: string;
}

export interface FaceDataRecordResponse {
    statusCode: number;
    statusString: string;
    subStatusCode: string;
    FPID: string;
}

export interface FaceDataSearch {
    searchResultPosition: number;
    maxResults: number;
    faceLibType: string;
    FDID: string;
    FPID: string;
}

export interface FaceDataSearchResponse {
    statusCode: number;
    statusString: string;
    subStatusCode: string;
    responseStatusStrg: string;
    numOfMatches: number;
    totalMatches: number;
    MatchList: MatchList[];
}

export interface MatchList {
    FPID: string;
    faceURL: string;
    modelData: string;
}

export interface FaceDataDelete {
    faceLibType: string;
    FDID: string;
    FPID: string;
    deleteFP: boolean;
}
