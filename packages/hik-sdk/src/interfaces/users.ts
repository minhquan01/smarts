export interface UserInfoSearchCond {
    searchID?: string;
    maxResults: 20;
    searchResultPosition: 0;
    EmployeeNoList: [
        {
            employeeNo: string;
        },
    ];
}
export interface UserInfoSearchResponse {
    UserInfoSearch: UserInfoSearch;
}

export interface UserInfoSearch {
    searchID: string;
    /** search status: "OK"-searching completed, "NO MATCH"-no matched results, "MORE"-searching for more results */
    responseStatusStrg: string;
    numOfMatches: number;
    totalMatches: number;
    UserInfo: UserInfo[];
}

export interface UserInfo {
    employeeNo: string;
    name: string;
    userType: 'normal' | 'visitor' | 'blacklist';
    closeDelayEnabled?: boolean;
    Valid: Valid;
    belongGroup?: string;
    password?: string;
    doorRight?: string;
    RightPlan?: RightPlan[];
    maxOpenDoorTime?: number;
    openDoorTime?: number;
    roomNumber?: number;
    floorNumber?: number;
    localUIRight?: boolean;
    gender?: string;
    numOfCard?: number;
    numOfFP?: number;
    numOfFace?: number;
    PersonInfoExtends?: PersonInfoExtend[];
}

export interface Valid {
    enable: boolean;
    beginTime: Date;
    endTime: Date;
    timeType?: 'local' | 'UTC';
}

export interface RightPlan {
    doorNo: number;
    planTemplateNo: string;
}

export interface PersonInfoExtend {
    value: string;
}

export interface CreateUserInfoResponse {
    statusCode: number;
    statusString: string;
    subStatusCode: string;
}
export interface EmployeeNoList {
    employeeNo: string;
}

export interface DeleteUserResponse {
    requestURL: string;
    statusCode: number;
    statusString: string;
    subStatusCode: string;
    errorCode: number;
    errorMsg: string;
}
