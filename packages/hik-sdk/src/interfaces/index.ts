export interface TimeStatus {
    mode: 'NTP' | 'manual';
    time: Date;
    timeZoneOffset: number;
}

export interface ResponseData {
    statusCode: number;
    statusString: string;
    subStatusCode: string;
}

export * from './acs';
export * from './faces';
export * from './users';
