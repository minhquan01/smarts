export interface AcsEvent {
    searchID: string;
    totalMatches: number;
    responseStatusStrg: string;
    numOfMatches: number;
    InfoList: InfoList[];
}

export interface AcsEventCond {
    searchID?: string;
    searchResultPosition: number;
    maxResults: number;
    major: number;
    minor: number;
    startTime: string;
    endTime: string;
}
export interface InfoList {
    major: number;
    minor: number;
    time: string;
    type: number;
    serialNo: number;
    currentVerifyMode: string;
    mask: string;
    remoteHostAddr?: string;
    cardType?: number;
    name?: string;
    cardReaderNo?: number;
    doorNo?: number;
    employeeNoString?: string;
    userType?: string;
    attendanceStatus?: string;
    label?: string;
}

export interface AcsEventResponse {
    requestURL?: string;
    statusCode?: number;
    statusString?: string;
    subStatusCode?: string;
    errorCode?: number;
    errorMsg?: string;
    AcsEvent?: AcsEvent;
}
