import { Access } from './Access';
import { Base, BaseConfig } from './Base';
import { Face } from './Face';
import { System } from './System';
import { User } from './User';

export class Hik {
    public User: User;
    public Face: Face;
    public Access: Access;
    public System: System;
    constructor(config: BaseConfig) {
        this.User = new User(config);
        this.Face = new Face(config);
        this.Access = new Access(config);
        this.System = new System(config);
    }
}
