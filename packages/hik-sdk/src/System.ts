import { Base, BaseConfig } from './Base';
import { AcsEventCond } from './interfaces/acs';
import { parseStringPromise } from 'xml2js';
export class System extends Base {
    constructor(config: BaseConfig) {
        super(config);
    }

    public async DeviceInfo(): Promise<any> {
        const resp = await this.request({
            url: '/ISAPI/System/deviceinfo',
            method: 'GET',
        });
        return parseStringPromise(resp.data, {
            explicitArray: false,
            mergeAttrs: true,
            trim: true,
            explicitRoot: false,
            normalize: true,
        });
    }
}
