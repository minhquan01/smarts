import { Base, BaseConfig } from './Base';
import { ResponseData } from './interfaces';
import {
    FaceDataDelete,
    FaceDataRecord,
    FaceDataRecordResponse,
    FaceDataSearch,
    FaceDataSearchResponse,
} from './interfaces/faces';

export class Face extends Base {
    constructor(config: BaseConfig) {
        super(config);
    }

    public async FaceDataRecord(data: FaceDataRecord): Promise<FaceDataRecordResponse> {
        const resp = await this.request({
            url: '/ISAPI/Intelligent/FDLib/FaceDataRecord',
            method: 'POST',
            data: data,
        });
        return resp.data;
    }

    public async FaceDataSearch(data: FaceDataSearch): Promise<FaceDataSearchResponse> {
        const resp = await this.request({
            url: '/ISAPI/Intelligent/FDLib/FDSearch',
            method: 'POST',
            data: data,
        });
        return resp.data;
    }

    public async FaceDataDelete(data: FaceDataDelete): Promise<ResponseData> {
        const resp = await this.request({
            url: '/ISAPI/Intelligent/FDLib/FDSetUp',
            method: 'PUT',
            data: data,
        });
        return resp.data;
    }
}
