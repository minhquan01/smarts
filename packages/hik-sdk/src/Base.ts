import axios, {
    AxiosInstance,
    AxiosRequestConfig,
    AxiosRequestHeaders,
    AxiosResponse,
} from 'axios';
import { TimeStatus } from './interfaces';
import * as crypto from 'crypto';
import * as url from 'url';

export interface BaseConfig {
    ip: string;
    port?: number;
    user: string;
    password: string;
    headers?: AxiosRequestHeaders;
}

export abstract class Base {
    protected readonly ip: string = '192.168.1.64';
    protected readonly port: number = 80;
    protected readonly user: string = 'admin';
    protected readonly password: string;
    protected axios: AxiosInstance;
    protected headers: any;
    private count: number = 0;

    protected get url(): string {
        return `http://${this.ip}:${this.port}`;
    }

    protected constructor(config: BaseConfig) {
        this.ip = config.ip;
        this.port = config.port || 80;
        this.user = config.user;
        this.password = config.password;
        this.headers = config.headers;
        this.axios = axios.create({
            baseURL: this.url,
            timeout: 10000,
            headers: config.headers,
        });
    }

    public async request(opts: AxiosRequestConfig): Promise<AxiosResponse> {
        try {
            return await this.axios.request(opts);
        } catch (resp1: any) {
            if (
                resp1.response === undefined ||
                resp1.response.status !== 401 ||
                !resp1.response.headers['www-authenticate']?.includes('nonce')
            ) {
                throw resp1;
            }
            const authDetails = resp1.response.headers['www-authenticate']
                .split(',')
                .map((v: string) => v.split('='));
            ++this.count;
            const nonceCount = ('00000000' + this.count).slice(-8);
            const cnonce = crypto.randomBytes(4).toString('hex');
            const realm = authDetails
                .find((el: any) => el[0].toLowerCase().indexOf('realm') > -1)[1]
                .replace(/"/g, '')
                .trim();
            const nonce = authDetails
                .find((el: any) => el[0].toLowerCase().indexOf('nonce') > -1)
                .join('=')
                .replace('nonce=', '')
                .replace(/"/g, '')
                .trim();
            const ha1 = crypto
                .createHash('md5')
                .update(`${this.user}:${realm}:${this.password}`)
                .digest('hex');
            const path = url.parse(opts.url!).pathname;
            const ha2 = crypto
                .createHash('md5')
                .update(`${opts.method ?? 'GET'}:${path}`)
                .digest('hex');
            const response = crypto
                .createHash('md5')
                .update(`${ha1}:${nonce}:${nonceCount}:${cnonce}:auth:${ha2}`)
                .digest('hex');
            const authorization = `Digest username="${this.user}", realm="${realm}", nonce="${nonce}", uri="${opts.url}", algorithm="MD5", qop=auth, nc=${nonceCount}, cnonce="${cnonce}", response="${response}"`;
            if (opts.headers) {
                opts.headers['authorization'] = authorization;
            } else {
                opts.headers = { authorization };
            }
            opts.params = { ...opts.params, format: 'json' };
            return this.axios.request(opts);
        }
    }
}
