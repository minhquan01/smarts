import { Base, BaseConfig } from './Base';
import { AcsEvent, AcsEventCond, AcsEventResponse } from './interfaces/acs';
import { v4 as uuidv4 } from 'uuid';

export class Access extends Base {
    constructor(config: BaseConfig) {
        super(config);
    }

    public async AccessEvent(cond: AcsEventCond): Promise<AcsEventResponse> {
        const resp = await this.request({
            url: '/ISAPI/AccessControl/AcsEvent',
            method: 'POST',
            data: {
                AcsEventCond: {
                    searchID: uuidv4(),
                    ...cond,
                },
            },
        });

        return resp.data;
    }
}
