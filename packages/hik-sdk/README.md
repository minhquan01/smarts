
# Hik SDK

Installation:

```js
import { Hik } from "hik-sdk";

const hik = new Hik({
  ip: "192.168.31.34",
  port: 80,
  user: "admin",
  password: "vvvVVV123",
  headers: {
    Accept: "*/*",
  },
});
```

Examples:

Save User FaceData

```js
const data = await hik.Face.FaceDataRecord({
    faceLibType: "blackFD",
    FDID: "1",
    FPID: "5",
    faceUrl: "http://localhost:8080/images/0000000002.jpg",
    PicFeaturePoints: [],
});
```

Get Access Log


```js
const data = await hik.Access.AccessEvent({
    searchID: "226a3a36-6e31-4ed8-9250-36a1e1574724",
    searchResultPosition: 0,
    maxResults: 40,
    major: 5,
    minor: 38,
    startTime: "2023-07-26T00:00:00+07:00",
    endTime: "2023-07-26T23:59:59+07:00",
  });
```

Hik Pagination:

```js
  searchResultPosition: /*required, integer, the start position of the search result in the result list. When there are multiple records and you cannot get all search results at a time, you can search for the records after the specified position next time*/

  maxResults: /*required, integer, maximum number of search results. If maxResults exceeds the range returned by the device capability, the device will return the maximum number of search results according to the device capability and will not return error message*/

```